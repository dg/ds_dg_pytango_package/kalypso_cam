#-------------------------------------------------
#
# Project created by QtCreator 2017-10-12T16:52:13
#
#-------------------------------------------------

QT       += core gui
QT       += serialport


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = KeithleySMUCommander
TEMPLATE = app
DEFINES += QT_DEPRECATED_WARNINGS


SOURCES += main.cpp\
        mainwindow.cpp \
    keithley2410.cpp \
    messlabor.cpp
 glwidget.cpp

HEADERS  += mainwindow.h \
    keithley2410.h \
    messlabor.h
 glwidget.h

FORMS    += mainwindow.ui

QMAKE_CXXFLAGS += "-std=c++11"




