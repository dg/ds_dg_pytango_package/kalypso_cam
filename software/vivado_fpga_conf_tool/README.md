Simple script to program Hi-Flex via Vivado

The script must be used as follows:
fpga_conf_vivado.sh -f FILENAME -g (opt.)

If FILENAME is a .bit file, the script programs the FPGA.
If the flag "-g" is included, the script generates the .mcs file and then
programs the FLASH memory.

