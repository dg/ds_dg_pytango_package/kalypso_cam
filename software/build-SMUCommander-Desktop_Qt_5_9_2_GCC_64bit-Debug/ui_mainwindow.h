/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QComboBox *Ports;
    QPushButton *SearchCOM;
    QGroupBox *groupBox;
    QComboBox *Compliance_Unit;
    QLabel *label_6;
    QDoubleSpinBox *IV_step;
    QLabel *label_2;
    QLabel *label;
    QLabel *label_7;
    QSpinBox *IV_meanbase;
    QLabel *label_4;
    QDoubleSpinBox *Compliance;
    QPushButton *Connect;
    QLabel *label_3;
    QComboBox *IV_imax_unit;
    QPushButton *IV_button;
    QPushButton *Set_Compl;
    QDoubleSpinBox *IV_end;
    QLabel *label_5;
    QPushButton *Close;
    QDoubleSpinBox *IV_start;
    QSpinBox *IV_delay;
    QDoubleSpinBox *IV_imax;
    QPushButton *test;
    QGroupBox *groupBox_2;
    QPushButton *Connect_TB;
    QPushButton *Close_TB;
    QCheckBox *checkBox;
    QLabel *label_9;
    QSpinBox *SensorChannel;
    QLCDNumber *lcdtemp;
    QLabel *label_10;
    QProgressBar *progressBar;
    QPushButton *Measure;
    QLineEdit *Cal_sensors;
    QDoubleSpinBox *Cal_temp_is;
    QLineEdit *Cal_filename;
    QLabel *label_8;
    QLabel *label_11;
    QLabel *label_12;
    QPushButton *Cal_addpoint;
    QPushButton *automeasureIVonTemp;
    QDoubleSpinBox *doubleSpinBox;
    QLabel *label_13;
    QLabel *label_14;
    QLineEdit *lineEdit;
    QPushButton *pushButton;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(723, 463);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        Ports = new QComboBox(centralWidget);
        Ports->setObjectName(QStringLiteral("Ports"));
        Ports->setGeometry(QRect(100, 20, 72, 22));
        Ports->setEditable(true);
        SearchCOM = new QPushButton(centralWidget);
        SearchCOM->setObjectName(QStringLiteral("SearchCOM"));
        SearchCOM->setGeometry(QRect(10, 20, 80, 21));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(10, 50, 321, 291));
        Compliance_Unit = new QComboBox(groupBox);
        Compliance_Unit->setObjectName(QStringLiteral("Compliance_Unit"));
        Compliance_Unit->setGeometry(QRect(220, 240, 72, 22));
        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(40, 240, 101, 20));
        IV_step = new QDoubleSpinBox(groupBox);
        IV_step->setObjectName(QStringLiteral("IV_step"));
        IV_step->setGeometry(QRect(150, 120, 62, 22));
        IV_step->setDecimals(3);
        IV_step->setMaximum(10);
        IV_step->setSingleStep(0.01);
        IV_step->setValue(0.05);
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(70, 90, 71, 16));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(70, 60, 71, 16));
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(40, 210, 101, 20));
        IV_meanbase = new QSpinBox(groupBox);
        IV_meanbase->setObjectName(QStringLiteral("IV_meanbase"));
        IV_meanbase->setGeometry(QRect(150, 180, 61, 22));
        IV_meanbase->setMinimum(1);
        IV_meanbase->setMaximum(10000);
        IV_meanbase->setValue(10);
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(70, 150, 71, 16));
        Compliance = new QDoubleSpinBox(groupBox);
        Compliance->setObjectName(QStringLiteral("Compliance"));
        Compliance->setGeometry(QRect(150, 240, 62, 22));
        Compliance->setMaximum(999);
        Compliance->setValue(500);
        Connect = new QPushButton(groupBox);
        Connect->setObjectName(QStringLiteral("Connect"));
        Connect->setGeometry(QRect(10, 30, 80, 21));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(70, 120, 71, 16));
        IV_imax_unit = new QComboBox(groupBox);
        IV_imax_unit->setObjectName(QStringLiteral("IV_imax_unit"));
        IV_imax_unit->setGeometry(QRect(220, 150, 72, 22));
        IV_button = new QPushButton(groupBox);
        IV_button->setObjectName(QStringLiteral("IV_button"));
        IV_button->setGeometry(QRect(190, 30, 80, 21));
        Set_Compl = new QPushButton(groupBox);
        Set_Compl->setObjectName(QStringLiteral("Set_Compl"));
        Set_Compl->setGeometry(QRect(-80, 240, 81, 21));
        IV_end = new QDoubleSpinBox(groupBox);
        IV_end->setObjectName(QStringLiteral("IV_end"));
        IV_end->setGeometry(QRect(150, 90, 62, 22));
        IV_end->setDecimals(3);
        IV_end->setMinimum(-1100);
        IV_end->setMaximum(1100);
        IV_end->setValue(5);
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(70, 180, 71, 16));
        Close = new QPushButton(groupBox);
        Close->setObjectName(QStringLiteral("Close"));
        Close->setGeometry(QRect(100, 30, 80, 21));
        IV_start = new QDoubleSpinBox(groupBox);
        IV_start->setObjectName(QStringLiteral("IV_start"));
        IV_start->setGeometry(QRect(150, 60, 62, 22));
        IV_start->setDecimals(3);
        IV_start->setMinimum(-1100);
        IV_start->setMaximum(1100);
        IV_start->setValue(0);
        IV_delay = new QSpinBox(groupBox);
        IV_delay->setObjectName(QStringLiteral("IV_delay"));
        IV_delay->setGeometry(QRect(150, 210, 61, 22));
        IV_delay->setMaximum(10000);
        IV_delay->setValue(3000);
        IV_imax = new QDoubleSpinBox(groupBox);
        IV_imax->setObjectName(QStringLiteral("IV_imax"));
        IV_imax->setGeometry(QRect(150, 150, 62, 22));
        IV_imax->setMaximum(999);
        IV_imax->setValue(200);
        test = new QPushButton(groupBox);
        test->setObjectName(QStringLiteral("test"));
        test->setGeometry(QRect(280, 30, 31, 21));
        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(360, 50, 321, 321));
        Connect_TB = new QPushButton(groupBox_2);
        Connect_TB->setObjectName(QStringLiteral("Connect_TB"));
        Connect_TB->setGeometry(QRect(10, 30, 80, 21));
        Close_TB = new QPushButton(groupBox_2);
        Close_TB->setObjectName(QStringLiteral("Close_TB"));
        Close_TB->setGeometry(QRect(100, 30, 80, 21));
        checkBox = new QCheckBox(groupBox_2);
        checkBox->setObjectName(QStringLiteral("checkBox"));
        checkBox->setGeometry(QRect(180, 90, 111, 19));
        label_9 = new QLabel(groupBox_2);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(20, 70, 81, 16));
        SensorChannel = new QSpinBox(groupBox_2);
        SensorChannel->setObjectName(QStringLiteral("SensorChannel"));
        SensorChannel->setGeometry(QRect(110, 70, 43, 22));
        SensorChannel->setMaximum(15);
        lcdtemp = new QLCDNumber(groupBox_2);
        lcdtemp->setObjectName(QStringLiteral("lcdtemp"));
        lcdtemp->setGeometry(QRect(80, 130, 171, 51));
        lcdtemp->setAutoFillBackground(false);
        lcdtemp->setSmallDecimalPoint(false);
        lcdtemp->setDigitCount(5);
        lcdtemp->setSegmentStyle(QLCDNumber::Flat);
        lcdtemp->setProperty("value", QVariant(-99.9));
        label_10 = new QLabel(groupBox_2);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(260, 130, 51, 51));
        QFont font;
        font.setPointSize(32);
        label_10->setFont(font);
        label_10->setLayoutDirection(Qt::LeftToRight);
        progressBar = new QProgressBar(groupBox_2);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setGeometry(QRect(10, 110, 61, 111));
        progressBar->setMinimum(100);
        progressBar->setMaximum(350);
        progressBar->setValue(180);
        progressBar->setTextVisible(false);
        progressBar->setOrientation(Qt::Vertical);
        Measure = new QPushButton(groupBox_2);
        Measure->setObjectName(QStringLiteral("Measure"));
        Measure->setGeometry(QRect(160, 70, 80, 21));
        Cal_sensors = new QLineEdit(groupBox_2);
        Cal_sensors->setObjectName(QStringLiteral("Cal_sensors"));
        Cal_sensors->setGeometry(QRect(85, 240, 113, 21));
        Cal_temp_is = new QDoubleSpinBox(groupBox_2);
        Cal_temp_is->setObjectName(QStringLiteral("Cal_temp_is"));
        Cal_temp_is->setGeometry(QRect(210, 240, 62, 22));
        Cal_temp_is->setMinimum(-60);
        Cal_temp_is->setMaximum(200);
        Cal_temp_is->setSingleStep(0.1);
        Cal_filename = new QLineEdit(groupBox_2);
        Cal_filename->setObjectName(QStringLiteral("Cal_filename"));
        Cal_filename->setGeometry(QRect(85, 270, 113, 21));
        label_8 = new QLabel(groupBox_2);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(10, 240, 71, 16));
        label_11 = new QLabel(groupBox_2);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(10, 270, 71, 16));
        label_12 = new QLabel(groupBox_2);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(280, 240, 31, 21));
        QFont font1;
        font1.setPointSize(15);
        label_12->setFont(font1);
        label_12->setLayoutDirection(Qt::LeftToRight);
        Cal_addpoint = new QPushButton(groupBox_2);
        Cal_addpoint->setObjectName(QStringLiteral("Cal_addpoint"));
        Cal_addpoint->setGeometry(QRect(210, 270, 80, 21));
        automeasureIVonTemp = new QPushButton(groupBox_2);
        automeasureIVonTemp->setObjectName(QStringLiteral("automeasureIVonTemp"));
        automeasureIVonTemp->setGeometry(QRect(100, 200, 81, 21));
        doubleSpinBox = new QDoubleSpinBox(groupBox_2);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setGeometry(QRect(235, 200, 62, 22));
        doubleSpinBox->setMinimum(-60);
        doubleSpinBox->setMaximum(200);
        doubleSpinBox->setSingleStep(0.1);
        label_13 = new QLabel(groupBox_2);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(190, 200, 51, 16));
        label_14 = new QLabel(groupBox_2);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setGeometry(QRect(305, 204, 16, 16));
        lineEdit = new QLineEdit(groupBox_2);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(190, 20, 113, 20));
        pushButton = new QPushButton(groupBox_2);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(230, 50, 75, 23));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 723, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        Compliance_Unit->setCurrentIndex(1);
        IV_imax_unit->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        SearchCOM->setText(QApplication::translate("MainWindow", "Search", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("MainWindow", "Keithley 2410", Q_NULLPTR));
        Compliance_Unit->clear();
        Compliance_Unit->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "nA", Q_NULLPTR)
         << QApplication::translate("MainWindow", "\302\265A", Q_NULLPTR)
         << QApplication::translate("MainWindow", "mA", Q_NULLPTR)
         << QApplication::translate("MainWindow", "A", Q_NULLPTR)
        );
        label_6->setText(QApplication::translate("MainWindow", "Current Compliance", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "End Voltage", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "Start Voltage", Q_NULLPTR));
        label_7->setText(QApplication::translate("MainWindow", "Measurement Delay", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "Max Current", Q_NULLPTR));
        Connect->setText(QApplication::translate("MainWindow", "Connect", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "Voltage Step", Q_NULLPTR));
        IV_imax_unit->clear();
        IV_imax_unit->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "nA", Q_NULLPTR)
         << QApplication::translate("MainWindow", "\302\265A", Q_NULLPTR)
         << QApplication::translate("MainWindow", "mA", Q_NULLPTR)
         << QApplication::translate("MainWindow", "A", Q_NULLPTR)
        );
        IV_button->setText(QApplication::translate("MainWindow", "IVCurve", Q_NULLPTR));
        Set_Compl->setText(QApplication::translate("MainWindow", "Set Compliance", Q_NULLPTR));
        label_5->setText(QApplication::translate("MainWindow", "Mean Base", Q_NULLPTR));
        Close->setText(QApplication::translate("MainWindow", "Close", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        IV_start->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        test->setText(QApplication::translate("MainWindow", "test", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Temperature Board", Q_NULLPTR));
        Connect_TB->setText(QApplication::translate("MainWindow", "Connect", Q_NULLPTR));
        Close_TB->setText(QApplication::translate("MainWindow", "Close", Q_NULLPTR));
        checkBox->setText(QApplication::translate("MainWindow", "Auto-Measure", Q_NULLPTR));
        label_9->setText(QApplication::translate("MainWindow", "Sensor Number:", Q_NULLPTR));
        label_10->setText(QApplication::translate("MainWindow", "\302\260C", Q_NULLPTR));
        Measure->setText(QApplication::translate("MainWindow", "Measure", Q_NULLPTR));
        Cal_filename->setText(QApplication::translate("MainWindow", "Calibration.dat", Q_NULLPTR));
        label_8->setText(QApplication::translate("MainWindow", "Used Sensors:", Q_NULLPTR));
        label_11->setText(QApplication::translate("MainWindow", "File Name:", Q_NULLPTR));
        label_12->setText(QApplication::translate("MainWindow", "\302\260C", Q_NULLPTR));
        Cal_addpoint->setText(QApplication::translate("MainWindow", "Add Point", Q_NULLPTR));
        automeasureIVonTemp->setText(QApplication::translate("MainWindow", "IV on Temp", Q_NULLPTR));
        label_13->setText(QApplication::translate("MainWindow", "Delta T", Q_NULLPTR));
        label_14->setText(QApplication::translate("MainWindow", "K", Q_NULLPTR));
        pushButton->setText(QApplication::translate("MainWindow", "PushButton", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
