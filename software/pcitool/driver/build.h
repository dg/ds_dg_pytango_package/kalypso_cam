#include "pcilib/build.h"
#define PCIDRIVER_BUILD "Built at 2021/04/22 11:46:35 by soleil"
#define PCIDRIVER_REVISION "Revision r" PCILIB_REVISION " from " PCILIB_REVISION_BRANCH " by " PCILIB_REVISION_AUTHOR " at /home/soleil/software/pcitool/driver, last modification from 2021/03/19 16:56:14"
#define PCIDRIVER_CHANGES PCILIB_REVISION_MODIFICATIONS

