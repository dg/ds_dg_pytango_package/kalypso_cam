#ifndef _FASTWRITER_PRIVATE_H
#define _FASTWRITER_PRIVATE_H

#define FASTWRITER_SYNCIO_ALIGN 512
#define FASTWRITER_DEFAULT_BUFFER_SIZE 134217728 /* 128 MB */
#define FASTWRITER_RESERVE_MEMORY 536870912 /* 512 MB */

#include <pthread.h>
#include "fastwriter.h"

typedef struct {
    fastwriter_flags_t flags;
    size_t buffer_size;
} fastwriter_parameters_t;

typedef enum {
    FASTWRITER_WRITE_FLAGS_DEFAULT = 0,
    FASTWRITER_WRITE_FLAG_FORCE = 1		/**< Force writting all passed data */
} fastwriter_write_flags_t;

typedef struct {
    int (*open)(fastwriter_t *ctx, const char *name, fastwriter_flags_t flags);
    void (*close)(fastwriter_t *ctx);
    int (*write)(fastwriter_t *ctx, fastwriter_write_flags_t flags, size_t size, void *data, size_t *written);
} fastwriter_api_t;

struct fastwrtier_s {
    fastwriter_api_t *api;		/**< Writer API */
    void *ctx;				/**< Writer Context */
    
    fastwriter_flags_t flags;

    int clean_locks;
    volatile int err;		        /**< indicates error reported by the writter backend */
    volatile int run_flag;
    pthread_t wthread;
    pthread_cond_t data_cond;		/**< broadcasted when new data arrives */
    pthread_mutex_t data_cond_mutex;
    pthread_cond_t space_cond;		/**< broadcasted when some space is freed */
    pthread_mutex_t space_cond_mutex;

    void *buffer;
    size_t size;		/**< size of the ring buffer in bytes */
    size_t max_usage;		/**< maximal number of bytes used in ring buffer */
    volatile size_t tail;	/**< first unwritten data in the ring buffer */
    volatile size_t head;	/**< last commited data in the ring buffer */
    size_t pos;			/**< last pushed data in the ring buffer */

    size_t written;		/**< number of bytes written */
    size_t commited;		/**< number of bytes commited */
    size_t chunked;		/**< number of bytes chunked */

    fastwriter_parameters_t params;
};

#endif /* _FASTWRITER_PRIVATE_H */
