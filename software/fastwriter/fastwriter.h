#ifndef _FASTWRITER_H
#define _FASTWRITER_H

typedef struct fastwrtier_s fastwriter_t;

typedef enum {
    FASTWRITER_FLAGS_DEFAULT = 0,
    FASTWRITER_FLAGS_BLOCK = 1,		/**< by default the error will be returned if there is no space in the buffer to accomodate the data */
    FASTWRITER_FLAGS_OVERWRITE = 2	/**< overwrite the data currently in the storage */
} fastwriter_flags_t;

typedef struct {
    size_t buffer_size;			/**< buffer size in bytes */
    size_t buffer_used;			/**< amount of data currently in the buffer */
    size_t buffer_max;			/**< maximal amount of data in the buffer */
    size_t commited;			/**< total commited data for current file */
    size_t written;			/**< total written data for currrent file */
} fastwriter_stats_t;

#define FASTWRITER_BUFFER_DEFAULT	0
#define FASTWRITER_BUFFER_MAX		((size_t)-1)

/*
 * @fs - defines which writter implementation will be actually used. One can
 * pass just a file name, then a type of partition will be autodetected.
 * Otherwise, it is possible to pass the name of storage device. In this 
 * case either RingFS will be used or data will be pushed to the RAW device.
 */
fastwriter_t *fastwriter_init(const char *fs, fastwriter_flags_t flags);
void fastwriter_destroy(fastwriter_t *ctx);

int fastwriter_set_buffer_size(fastwriter_t *ctx, size_t buffer_size);
int fastwriter_get_stats(fastwriter_t *ctx, fastwriter_stats_t *stats);

int fastwriter_open(fastwriter_t *ctx, const char *name, fastwriter_flags_t flags);
int fastwriter_close(fastwriter_t *ctx);

int fastwriter_push(fastwriter_t *ctx, size_t size, const void *buf);
int fastwriter_commit(fastwriter_t *ctx);
int fastwriter_cancel(fastwriter_t *ctx);

int fastwriter_push_data(fastwriter_t *ctx, size_t size, const void *buf);

#endif /* _FASTWRITER_H */
