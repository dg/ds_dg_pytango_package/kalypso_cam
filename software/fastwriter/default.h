#ifndef _FASTWRITER_DEFAULT_H
#define _FASTWRITER_DEFAULT_H

#include "private.h"

int fastwriter_default_open(fastwriter_t *ctx, const char *name, fastwriter_flags_t flags);
void fastwriter_default_close(fastwriter_t *ctx);
int fastwriter_default_write(fastwriter_t *ctx, fastwriter_write_flags_t flags, size_t size, void *data, size_t *written);

#ifdef _FASTWRITER_DEFAULT_C
fastwriter_api_t fastwriter_default_api = {
    fastwriter_default_open,
    fastwriter_default_close,
    fastwriter_default_write
};
#else /* _FASTWRITER_DEFAULT_C */
extern fastwriter_api_t fastwriter_default_api;
#endif /* _FASTWRITER_DEFAULT_C */

#endif /* _FASTWRITER_DEFAULT_H */
