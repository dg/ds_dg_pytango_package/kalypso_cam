#ifndef _PCITOOL_SYSINFO_H
#define _PCITOOL_SYSINFO_H

size_t fastwriter_get_free_memory();
int fastwriter_get_file_fs(const char *fname, size_t size, char *fs);

#endif /* _PCITOOL_SYSINFO_H */
