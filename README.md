
<a name="readme-top"></a>



<!-- PROJECT LOGO -->
<br />
<div align="center">
  

  <h3 align="center">Kalypso Cam Tango Device</h3>

</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Device</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#license">License</a></li>
  </ol>
</details>


<!-- GETTING STARTED -->
## Getting Started

this repository contains the files needed to run the DG_PY_KALYPSO device. this device was created to extract the commands provided in the GUI and make them accessible in TANGO control system. this device can be greatly improved, but is fully satisfactory for the needs we have at SOLEIL.

### Prerequisites

You must have installed Tango and PyTango locally on the kalypso PC and have a valid tango environment (TANGO_DB,...).
https://tango-controls.readthedocs.io/en/latest/installation/index.html
* tango
``` sh
sudo apt-get update
sudo apt install tango-starter tango-test liblog4j1.2-java 
> sudo apt install --assume-yes wget\
   wget -c https://people.debian.org/~picca/libtango-java_XX_version.deb\
   sudo dpkg -i ./libtango-java_XX_version.deb
sudo apt install python3-tango  
sudo apt-install python3-h5py 
```

you have to declare device in your tango environnement
with properties correctly set
https://gitlab.synchrotron-soleil.fr/dg/ds_dg_pytango_package/kalypso_cam/-/wikis/home

### Installation

Just copy project in local folder and start it
example:
```sh
/home/kalypso/device/kalypso_cam/./DG_PY_Kalypso.py <instance_name> -V4
```








<!-- LICENSE -->
## License

Distributed under the GNUV3 License. See `LICENSE.MD` for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

