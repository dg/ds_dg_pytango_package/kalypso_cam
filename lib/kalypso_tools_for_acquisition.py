import numpy as np
import matplotlib.pyplot as plt
import os
import os.path
import json
import subprocess
import time
from datetime import datetime
import tango
from .Keithley2400_v01 import keithley2400
import h5py
from contextlib import contextmanager

@contextmanager
def run_command(command,cwd,obj, append=True, heartbeat=False):
    outp=[]
    if append:
        obj.append_log_stdout('----  Command: '+', '.join(command))
    process = subprocess.Popen(command,cwd=cwd, stdout=subprocess.PIPE,stderr=subprocess.STDOUT,shell=True)
    while True:
        if obj.kill:
            obj.append_log_stdout('---!!! //// Aborted by USER \\\\\\\\ !!!---')
            process.terminate()
            process.kill()
        output = process.stdout.readline()
        if output == b'' and process.poll() is not None:
            break
        if output:
            outp.append(str(output.strip()))
            if append:
                obj.append_log_stdout(str(output.strip()))
            if heartbeat:
                obj.heartbeat_log_stdout()

    rc = process.poll()
    return (rc,outp)

def start_system_w_512_pixels():
    print('##########################################################')
    print('       Start Kalypso system with 512 pixels               ')
    print('##########################################################')

    # Load PCILIB environment
    os.environ['PCILIB_MODEL'] = 'ipedma'

    # Get into directory for 512 pixels
    os.chdir('/home/soleil/kalypso/device/DG-PY-KALYPSO/shell_512')
    #https://www.endpointdev.com/blog/2015/01/getting-realtime-output-using-python/
    # Start sequence
    print('GLOBAL RESET ... ')
    #subprocess.Popen(["./reset_ex_rf.sh"], shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT).wait()
    subprocess.Popen(["./reset_ex_rf.sh"], shell=True).wait()
    time.sleep(0.5)

    print('PLL INIT/CONF for KARA 62.5 MHz ')
    subprocess.Popen(["./pll_62_5.sh"], shell=True).wait()
    time.sleep(0.1)

    print('AXI_MASTER on FPGA INIT ...')
    subprocess.Popen(["./AXI_conf.sh"], shell=True).wait()
    time.sleep(0.1)

    print('AXI_Reconfiguration for SubClass 2, frame per Multi-frame = 16')
    subprocess.Popen(["./AXI_ReConf.sh"], shell=True).wait()
    time.sleep(0.1)

    print('ADCs INIT/CONF SubClass 2, frame per Multi-frame = 16')
    subprocess.Popen(["./adc_config.sh"], shell=True).wait()
    time.sleep(0.1)

    print('SOFT RESET ...')
    subprocess.Popen(["./soft_reset.sh"], shell=True).wait()
    time.sleep(0.1)

    print('SEND TX_TRIG both ADCs ...')
    subprocess.Popen(["./tx_trig_adcs.sh"], shell=True).wait()
    time.sleep(0.1)

    print('Gotthard ON ... ')
    subprocess.Popen(["./G_ON.sh"], shell=True).wait()
    time.sleep(0.1)

    print('Number of acquisitions to acquire (Slow Trig.)= 1')
    subprocess.Popen(["pci -w 0x9024 0x1"], shell=True).wait()
    time.sleep(0.1)
    subprocess.Popen(["pci -w 0x9024 0x0"], shell=True).wait()  # for internal slow Triger #
    time.sleep(0.1)

    print('Number of acquisitions to skip (Slow Trig.)= 0')
    subprocess.Popen(["pci -w 0x902C 0x0"], shell=True).wait()
    time.sleep(0.1)

    print('Number of frame to acquire (Fast Trig.)= 1024')
    subprocess.Popen(["pci -w 0x9020 0x400"], shell=True).wait()
    time.sleep(0.1)

    print('Number of frame to skip (Fast Trig.)... = 1')
    subprocess.Popen(["pci -w 0x9028 0x1"], shell=True).wait()
    time.sleep(0.1)
    subprocess.Popen(["pci -w 0x9028 0x0"], shell=True).wait()
    time.sleep(0.1)

    print('Doing something ????')
    subprocess.Popen(["pci -w 0x910C 0"], shell=True).wait()
    time.sleep(0.1)
    subprocess.Popen(["./adc_offset_at_0_delay.sh"], shell=True).wait()
    time.sleep(0.1)
    subprocess.Popen(["./offset_soleil.sh"], shell=True).wait()
    time.sleep(0.1)

    print('Doing something ????')
    #	subprocess.Popen(["pci -w 0x910C e"], shell=True).wait()
    #	time.sleep(0.1)
    subprocess.Popen(["pci -w 0x910C 33"], shell=True).wait()  # with 904c 3009
    time.sleep(0.1)
    #	subprocess.Popen(["pci -w 0x910C 32"], shell=True).wait()   # with 904c 009
    #	time.sleep(0.1)

    # Get back to directory for scripts
    os.chdir('/home/soleil/kalypso/device/DG-PY-KALYPSO/')

    print('>>>>>>>>>> System started')

    return



def restart_system_w_512_pixels(obj):
    obj.clear_log_stdout()
    obj.append_log_stdout('##########################################################')
    obj.append_log_stdout('       Start Kalypso system with 512 pixels               ')
    obj.append_log_stdout('##########################################################')

    # Load PCILIB environment
    os.environ['PCILIB_MODEL'] = 'ipedma'

    # Get into directory for 512 pixels
    cwd='/home/soleil/kalypso/device/DG-PY-KALYPSO/shell_512'
    os.chdir('/home/soleil/kalypso/device/DG-PY-KALYPSO/shell_512')
    #https://www.endpointdev.com/blog/2015/01/getting-realtime-output-using-python/
    # Start sequence
    obj.append_log_stdout('GLOBAL RESET ... ')
    run_command(["./reset_ex_rf.sh"],cwd,obj)
    time.sleep(0.5)

    obj.append_log_stdout('PLL INIT/CONF for KARA 62.5 MHz ')
    run_command(["./pll_62_5.sh"], cwd,obj)
    time.sleep(0.1)

    obj.append_log_stdout('AXI_MASTER on FPGA INIT ...')
    run_command(["./AXI_conf.sh"],cwd,obj)
    time.sleep(0.1)

    obj.append_log_stdout('AXI_Reconfiguration for SubClass 2, frame per Multi-frame = 16')
    run_command(["./AXI_ReConf.sh"],cwd,obj)
    time.sleep(0.1)

    obj.append_log_stdout('ADCs INIT/CONF SubClass 2, frame per Multi-frame = 16')
    run_command(["./adc_config.sh"],cwd,obj)
    time.sleep(0.1)

    obj.append_log_stdout('SOFT RESET ...')
    run_command(["./soft_reset.sh"],cwd,obj)
    time.sleep(0.1)

    obj.append_log_stdout('SEND TX_TRIG both ADCs ...')
    run_command(["./tx_trig_adcs.sh"],cwd,obj)
    time.sleep(0.1)

    obj.append_log_stdout('Gotthard ON ... ')
    run_command(["./G_ON.sh"],cwd,obj)
    time.sleep(0.1)

    obj.append_log_stdout('Number of acquisitions to acquire (Slow Trig.)= 1')
    run_command(["pci -w 0x9024 0x1"],cwd,obj)
    time.sleep(0.1)
    run_command(["pci -w 0x9024 0x0"],cwd,obj)  # for internal slow Triger #
    time.sleep(0.1)

    obj.append_log_stdout('Number of acquisitions to skip (Slow Trig.)= 0')
    run_command(["pci -w 0x902C 0x0"],cwd,obj)
    time.sleep(0.1)

    obj.append_log_stdout('Number of frame to acquire (Fast Trig.)= 1024')
    run_command(["pci -w 0x9020 0x400"],cwd,obj)
    time.sleep(0.1)

    obj.append_log_stdout('Number of frame to skip (Fast Trig.)... = 1')
    run_command(["pci -w 0x9028 0x1"],cwd,obj)
    time.sleep(0.1)
    run_command(["pci -w 0x9028 0x0"],cwd,obj)
    time.sleep(0.1)

    obj.append_log_stdout('Doing something ????')
    run_command(["pci -w 0x910C 0"],cwd,obj)
    time.sleep(0.1)
    run_command(["./adc_offset_at_0_delay.sh"],cwd,obj)
    time.sleep(0.1)
    run_command(["./offset_soleil.sh"],cwd,obj)
    time.sleep(0.1)

    obj.append_log_stdout('Doing something ????')
    #	subprocess.Popen(["pci -w 0x910C e"], shell=True).wait()
    #	time.sleep(0.1)
    run_command(["pci -w 0x910C 33"],cwd,obj)  # with 904c 3009
    time.sleep(0.1)
    #	subprocess.Popen(["pci -w 0x910C 32"], shell=True).wait()   # with 904c 009
    #	time.sleep(0.1)

    # Get back to directory for scripts
    os.chdir('/home/soleil/kalypso/device/DG-PY-KALYPSO/')

    obj.append_log_stdout('>>>>>>>>>> System started')

    return


def start_system_w_1024_pixels():
    print('##########################################################')
    print('       Start Kalypso system with 1024 pixels              ')
    print('##########################################################')

    # Load PCILIB environment
    os.environ['PCILIB_MODEL'] = 'ipedma'

    # Get into directory for 512 pixels
    os.chdir('/home/soleil/kalypso/soleil_GUI/KALYPSO2_5_GUI_1024/')

    # Start sequence
    print('GLOBAL RESET ... ')
    subprocess.Popen(["./reset_ex_rf.sh"], shell=True).wait()
    time.sleep(0.5)

    print('PLL INIT/CONF for KARA 62.5 MHz ')
    subprocess.Popen(["./pll_62_5.sh"], shell=True).wait()
    time.sleep(0.1)

    print('AXI_MASTER on FPGA INIT ...')
    subprocess.Popen(["./AXI_conf.sh"], shell=True).wait()
    time.sleep(0.1)

    print('AXI_Reconfiguration for SubClass 2, frame per Multi-frame = 16')
    subprocess.Popen(["./AXI_ReConf.sh"], shell=True).wait()
    time.sleep(0.1)

    print('ADCs INIT/CONF SubClass 2, frame per Multi-frame = 16')
    subprocess.Popen(["./adc_config.sh"], shell=True).wait()
    time.sleep(0.1)

    print('SOFT RESET ...')
    subprocess.Popen(["./soft_reset.sh"], shell=True).wait()
    time.sleep(0.1)

    print('SEND TX_TRIG both ADCs ...')
    subprocess.Popen(["./tx_trig_adcs.sh"], shell=True).wait()
    time.sleep(0.5)
    subprocess.Popen(["./tx_trig_adcs.sh"], shell=True).wait()
    time.sleep(0.5)
    print('WHY twice ???? try to remove one...')

    print('Gotthard ON ... ')
    subprocess.Popen(["./G_ON.sh"], shell=True).wait()
    time.sleep(0.1)

    print('Number of acquisitions to acquire (Slow Trig.)= 1')
    subprocess.Popen(["pci -w 0x9024 0x1"], shell=True).wait()
    time.sleep(0.1)
    subprocess.Popen(["pci -w 0x9024 0x0"], shell=True).wait()  # for internal slow Triger #
    time.sleep(0.1)

    print('Number of acquisitions to skip (Slow Trig.)= 0')
    subprocess.Popen(["pci -w 0x902C 0x0"], shell=True).wait()
    time.sleep(0.1)

    print('Number of frame to acquire (Fast Trig.)= 1024')
    subprocess.Popen(["pci -w 0x9020 0x400"], shell=True).wait()
    time.sleep(0.1)

    print('Number of frame to skip (Fast Trig.)... = 1')
    subprocess.Popen(["pci -w 0x9028 0x1"], shell=True).wait()
    time.sleep(0.1)
    subprocess.Popen(["pci -w 0x9028 0x0"], shell=True).wait()
    time.sleep(0.1)

    print('Doing something ????')
    subprocess.Popen(["pci -w 0x910C 0"], shell=True).wait()
    time.sleep(0.1)
    subprocess.Popen(["./adc_offset_at_0_delay.sh"], shell=True).wait()
    time.sleep(0.1)
    subprocess.Popen(["./offset_soleil.sh"], shell=True).wait()
    time.sleep(0.1)

    print('Doing something ????')
    subprocess.Popen(["pci -w 64 1fffff"], shell=True).wait()
    time.sleep(0.1)
    subprocess.Popen(["pci -w 0x910C 2d"], shell=True).wait()
    time.sleep(0.1)

    # Get back to directory for scripts
    os.chdir('/home/soleil/kalypso/device/DG-PY-KALYPSO')

    print('>>>>>>>>>> System started')

    return



def set_acquisition_parameters(opt_nb_pixels, exp_time, nb_frames, opt_fast_trigger, delay, opt_slow_trigger,
                               nb_acq_w_slow_trigger, nb_skip_slow_trigger,obj):
    #######################################################
    # Basic loadings...
    #######################################################
    # Load PCILIB environment
    os.environ['PCILIB_MODEL'] = 'ipedma'
    obj.clear_log_stdout()
    # Get into directory proper 512/1024 pixels directory
    if opt_nb_pixels == 512:
        cwd = '/home/soleil/kalypso/device/DG-PY-KALYPSO/shell_512'
        os.chdir('/home/soleil/kalypso/device/DG-PY-KALYPSO/shell_512')
    elif opt_nb_pixels == 1024:
        cwd = '/home/soleil/kalypso/soleil_GUI/KALYPSO2_5_GUI_1024/'
        os.chdir('/home/soleil/kalypso/soleil_GUI/KALYPSO2_5_GUI_1024/')

    # Initialize output file
    output_file_temp = 'temp.bin'
    cmd = 'rm ' + output_file_temp
    try:
        run_command([cmd], cwd,obj)
        time.sleep(0.1)
    except:
        obj.append_log_stdout('No file to be deleted...')

    #######################################################
    # Acquisition parameters
    #######################################################
    # Exposure time
    obj.append_log_stdout(str('GOTT: Exposure time (ns) = '+ str(exp_time)))
    exp_time_counts = np.int(exp_time / 8.)  # Convert ns to multiple of 8 ns
    exp_time_counts_hex = hex(exp_time_counts)
    cmd = "pci -w 0x9000 " + str(exp_time_counts_hex)
    run_command([cmd],cwd,obj)
    time.sleep(0.1)

    # Gain
    obj.append_log_stdout('Set gain: TO BE DONE !!!!!.........')

    #######################################################
    # Fast trigger parameters
    #######################################################
    # Fast trigger mode
    obj.append_log_stdout('Setting fast trigger to = '+ str(opt_fast_trigger))
    opt_fast_trigger_hex = hex(opt_fast_trigger)
    obj.append_log_stdout('Setting fast trigger hex to = '+ str(opt_fast_trigger_hex))
    cmd = "pci -w 9044 " + str(opt_fast_trigger_hex)
    obj.append_log_stdout('cmd = '+ cmd)
    run_command([cmd],cwd,obj)

    # Delay w.r. Fast Trigger
    obj.append_log_stdout('GOTT: Delay w.r. Fast trigger (ns) = '+ str(delay))
    cmd = "pci -w 0x9004 " + str(hex(delay))
    run_command([cmd],cwd,obj)
    time.sleep(0.1)

    # Number of frames to record with Fast Trigger
    obj.append_log_stdout('Number of frames to acquire (Fast Trig.) = '+ str(nb_frames))
    if nb_frames == 100:
        cmd = "pci -w 0x9020 0x64"
    elif nb_frames == 256:
        cmd = "pci -w 0x9020 0x100"
    elif nb_frames == 512:
        cmd = "pci -w 0x9020 0x200"
    elif nb_frames == 1024:
        cmd = "pci -w 0x9020 0x400"
    elif nb_frames == 2048:
        cmd = "pci -w 0x9020 0x800"
    else:
        obj.append_log_stdout('Default setting: 100 frames')
    # print('********************* No such nb of frames encoded !!!************************')
    # sys.exit()
    run_command([cmd],cwd,obj)
    time.sleep(0.1)

    # Number of shots to skip from Fast Trigger
    obj.append_log_stdout('Number of frame to skip (Fast Trig.)... = ')
    # subprocess.Popen(["pci -w 0x9028 0x1"], shell=True).wait()
    run_command(["pci -w 0x9028 0x0"],cwd,obj)
    time.sleep(0.1)

    #######################################################
    # Slow trigger parameters
    #######################################################
    # Slow Trigger option
    obj.append_log_stdout('Using Slow Trigger option = '+str(opt_slow_trigger))

    # Number of acq. using Slow Trigger
    obj.append_log_stdout('Number of acquisitions to acquire (ext. Slow Trig.) = '+ str(nb_acq_w_slow_trigger))
    run_command(["pci -w 0x9024 0x1"],cwd,obj)  # To initialize the slow trigger (mandatory)
    if opt_slow_trigger == 0:
        cmd = "pci -w 0x9024 0x0"
        run_command([cmd],cwd,obj)  # Using internal Slow Trigger
    elif opt_slow_trigger == 1:
        cmd = "pci -w 9024 " + str(nb_acq_w_slow_trigger)
        run_command([cmd],cwd,obj)  # Using external Slow Trigger
    time.sleep(1.0)

    # Number of shots to skip from Slow Trigger
    obj.append_log_stdout('Number of Slow Triggers to skip (ext. Slow Trig.) = '+ str(nb_skip_slow_trigger))
    cmd = "pci -w 0x902C 0x" + str(nb_skip_slow_trigger)
    run_command([cmd],cwd,obj)
    time.sleep(0.1)

    #######################################################
    # Basic endings....
    #######################################################
    # Get back to directory for scripts
    os.chdir('/home/soleil/kalypso/device/DG-PY-KALYPSO')

    obj.append_log_stdout('>>>>>>>>>> Parameters set')

    return


def record_data(dirname, fileroot, opt_slow_trigger, nb_acq_w_slow_trigger, dct_data_obl,obj):
    # Load PCILIB environment
    os.environ['PCILIB_MODEL'] = 'ipedma'

    # Get into directory for 512 pixels
    cwd='/home/soleil/kalypso/device/DG-PY-KALYPSO/shell_512'
    os.chdir('/home/soleil/kalypso/device/DG-PY-KALYPSO/shell_512')

    #add sep if it's missing
    if not dirname.endswith(os.path.sep):
        dirname += os.path.sep
    #obj.error_stream('separator')
    # Set ADD Reset DDR
    obj.append_log_stdout('ADD Reset DDR')
    run_command(["pci -w 0x9040 0x10210F50"],cwd,obj)
    time.sleep(1.001)
    run_command(["pci -w 0x9040 0x10210F00"],cwd,obj)
    time.sleep(1.001)

    # Prepare output = list of recorded files
    output_file_list = []
    #obj.error_stream('1')
    if opt_slow_trigger == 0:
        #obj.error_stream('opt_slow_0')
        # Start data acquisition
        obj.append_log_stdout('Start Normal acquisition + ENABLE Readout to DDR + HEADER')
        run_command(["pci -w 0x9040 0x1021FF00"],cwd,obj)
        time.sleep(0.001)
        dateTimeObj = datetime.now()

        # Start data reading
        obj.append_log_stdout('Start reading camera data...')
        output_file_temp = 'temp.bin'
        waiting_time = 1000000  # (micro seconds) Time max to wait for data reading
        cmd = "pci -r dma0 --multipacket -o " + output_file_temp + " --timeout=" + str(waiting_time)
        run_command([cmd],cwd,obj)
        obj.append_log_stdout('>>>>>>>>>> Done')

        # IDLE
        run_command(["pci -w 0x9040 0x10210F00"],cwd,obj)
        time.sleep(0.001)

        # Save data into output file
        obj.append_log_stdout('Start saving camera data...')
        timeStampStr = dateTimeObj.strftime("%d-%m-%Y_%H-%M-%S")
        output_file_root = fileroot + '_' + timeStampStr
        output_file = output_file_root + '.bin'
        cmd = "mv " + output_file_temp + " " + dirname + output_file
        run_command([cmd],cwd,obj)
        obj.append_log_stdout('>>>>>>>>>> Done')

        # Get camera settings
        dct_data = {}
        obj.append_log_stdout('Getting camera settings....')
        dct_data = get_camera_settings(dct_data)
        obj.append_log_stdout('>>>>>>>>>> Done')

        # Reset the Slow Trigger option
        run_command(["pci -w 0x9024 0x0"],cwd,obj)  # KEEP THIS LINE !!!

        # Get optical beamline settings
        for k in dct_data_obl.keys():
            dct_data[k] = dct_data_obl[k]

        # Get accelerator settings
        obj.append_log_stdout('Getting machine settings....')
        dct_data = get_machine_settings(opt_slow_trigger, dct_data)
        obj.append_log_stdout('>>>>>>>>>> Done')

        # Save all settings into into .h5 file
        obj.append_log_stdout('Saving all settings into .h5 file....')
        obj.append_log_stdout('Settings to be saved   :   '+ str(dct_data))
        with h5py.File(dirname + output_file_root + '.h5', "w") as fic:
            for k in dct_data.keys():
                # print('k',k)
                # print('dct k',dct[k])
                fic.create_dataset(k, data=dct_data[k])
        obj.append_log_stdout('>>>>>>>>>> Settings recorded')

        # Store file name
        output_file_list.append(output_file_root)

    elif opt_slow_trigger == 1:
        #obj.error_stream('opt_slow_1')
        # Start Normal acquisition + ENABLE Readout to DDR + HEADER
        obj.append_log_stdout('Start Normal acquisition + ENABLE Readout to DDR + HEADER')
        run_command(["pci -w 0x9040 0x1021FF00"],cwd,obj)
        time.sleep(1.001)

        # Dump acquisition info before acquiring
        cmd = "pci -r 9000 -s 100"
        run_command([cmd],cwd,obj)
        #obj.error_stream('opt_slow_1_1')
        # Read data to empty memory
        obj.append_log_stdout('Reading data to empty memory .....')
        output_file_temp = 'temp_i.bin'
        waiting_time = 1000  # (micro seconds) Time max to wait for data reading
        cmd = "pci -r dma0 --multipacket -o " + output_file_temp + " --timeout=" + str(waiting_time)
        run_command([cmd],cwd,obj)
        cmd = "rm " + output_file_temp
        run_command([cmd],cwd,obj)
        #obj.error_stream('opt_slow_1_2')
        # Start looping on nb_acq_w_slow_trigger
        for i in range(nb_acq_w_slow_trigger + 1):
            #obj.error_stream('opt_slow_1_boucle')
            obj.append_log_stdout('Acquiring data set ' + str(i) + ' / ' + str(nb_acq_w_slow_trigger))
            # Read reference state of waiting trigger
            cmd = "(pci -r 9034 | sed 's/.*[0-9]:  //';)"
            res=run_command([cmd],cwd,obj,append=False)
            acq_num = res.gen[1][0]
            obj.append_log_stdout('acq_num  :'+ str(acq_num))  # ATTENTION sortie en hexadecimal !


            # Read temp state of waiting trigger
            cmd = "(pci -r 9034 | sed 's/.*[0-9]:  //';)"
            res= run_command([cmd],cwd,obj,append=False)
            temp = res.gen[1]
            temp = str(temp[0]).split(':')
            acq_num_temp = temp[0]
            obj.append_log_stdout('acq_num_temp  :'+ str(acq_num_temp))  # ATTENTION sortie en hexadecimal !

            obj.append_log_stdout('Waiting for trigger......')
            while acq_num_temp == acq_num:
                #obj.error_stream('opt_slow_1_while'+str(acq_num_temp)+'/'+str(acq_num))
                cmd = "(pci -r 9034 | sed 's/.*[0-9]:  //';)"
                res= run_command([cmd],cwd,obj,append=False,heartbeat=True)
                temp = res.gen[1]
                temp = str(temp[0]).split(':')
                acq_num_temp = temp[0]
            obj.append_log_stdout('Trigger arrived !!!!........')
            dateTimeObj = datetime.now()

            # Dump acquisition info before acquiring
            cmd = "pci -r 9000 -s 100"
            run_command([cmd],cwd,obj)

            # Start data reading
            obj.append_log_stdout('*DMA: Reading data...')
            # output_file_temp = 'temp_' + str(i) + '.bin'
            output_file_temp = 'temp_i.bin'  # TO BE CHECKED !!!!!
            waiting_time = 1000000  # (micro seconds) Time max to wait for data reading
            cmd = "pci -r dma0 --multipacket -o " + output_file_temp + " --timeout=" + str(waiting_time)
            run_command([cmd],cwd,obj)

            # Waiting time
            time.sleep(0.3)  # to be adjusted dependint on slow trigger rep rate. Can be reduced to 0 !

            # ADD Reset DDR
            obj.append_log_stdout('ADD Reset DDR')
            run_command(["pci -w 0x9040 0x10210F50"],cwd,obj)
            time.sleep(0.001)

            # IN ACQUISITION
            obj.append_log_stdout('IN ACQUISITION')
            run_command(["pci -w 0x9040 0x1021FF00"],cwd,obj)
            time.sleep(0.001)

            # Save data into output file
            timeStampStr = dateTimeObj.strftime("%d-%m-%Y_%H-%M-%S")
            # output_file = fileroot + '_' + str(i) + '_' + timeStampStr + '.bin'
            output_file_root = fileroot + '_' + str(i) + '_' + timeStampStr
            output_file = output_file_root + '.bin'
            cmd = "mv " + output_file_temp + " " + dirname + output_file
            run_command([cmd],cwd,obj)

            # Get camera settings
            dct_data = {}
            obj.append_log_stdout('Getting camera settings....')
            dct_data = get_camera_settings(dct_data)
            obj.append_log_stdout('>>>>>>>>>> Done')

            # Get optical beamline settings
            for k in dct_data_obl.keys():
                dct_data[k] = dct_data_obl[k]

            # Get accelerator settings
            obj.append_log_stdout('Getting machine settings....')
            dct_data = get_machine_settings(opt_slow_trigger, dct_data)
            obj.append_log_stdout('>>>>>>>>>> Done')

            # Save all settings into into .h5 file
            obj.append_log_stdout('Saving all settings into .h5 file....')
            obj.append_log_stdout('Settings to be saved   :   '+ str(dct_data))
            with h5py.File(dirname + output_file_root + '.h5', "w") as fic:
                for k in dct_data.keys():
                    # print('k',k)
                    # print('dct k',dct[k])
                    fic.create_dataset(k, data=dct_data[k])
            obj.append_log_stdout('>>>>>>>>>> Settings recorded')

            # Store file name
            output_file_list.append(output_file_root)
        #obj.error_stream('2')
        # Reset acquisition parameters
        obj.append_log_stdout('Acquisition done!')
        run_command(["pci -w 9024 0"],cwd,obj)
        time.sleep(1.1)
        run_command(["pci -w 902C 0"],cwd,obj)
        time.sleep(1.1)

        # Reset system
        obj.append_log_stdout('KALYPSO ready')
        run_command(["pci -w 0x9040 0x10210F00"],cwd,obj)
        time.sleep(1.001)

        obj.append_log_stdout('>>>>>>>>>> Data recorded')
    #obj.error_stream('3')
    # Get back to directory for scripts
    os.chdir('/home/soleil/kalypso/device/DG-PY-KALYPSO')
    #obj.error_stream('5')

    return output_file_list


def switch_on_power_supply(current_max,dev):
    print('Switching on power supply.....')

    print('Setting communication with power supply Keithley2400....')
    # dev = tango.DeviceProxy('Labo/DG/gpib') # Load device for gpib commnication on Reseau LABO
    #dev = tango.DeviceProxy('ans-c01/dg/gpib')  # Load device for gpib commnication on RCM
    #dev.init()  # Initialize GPIB device
    time.sleep(2)
    k2400 = keithley2400(dev)  # Create Sourcemeter class object
    print('deviceproxy OK')
    try:
        print('Reset...')
        k2400.restoreGPIB()  # Reset SourceMeter into default GPIB conditions
        print('Reset OK')
    except Exception as ex:
        print(ex)

    res = k2400.queryConfigure()  # Get mode
    print('...................done !')

    # Define starting voltage
    voltage_start = 1.0  # (V)
    print('Starting voltage defined to : ', voltage_start)

    # Define maximum current
    print('Maximum current set to : ', current_max)

    # Apply starting voltage + max current
    print('Applying starting voltage + max current......')
    k2400.apply_voltage(voltage=voltage_start, compliance_current=current_max)
    time.sleep(2)
    print('...................done !')

    # Enable source
    print('Enable source !!!')
    k2400.enable_source()
    time.sleep(3)

    # Get current state
    state_temp = k2400.get_source_state()
    print('State : ', state_temp)

    # Measure voltage + current
    meas_volt = k2400.measure_voltage()
    print('Voltage measured  : ', meas_volt, ' V')
    meas_curr = k2400.measure_current()
    print('Current measured  :', meas_curr, ' A')

    return


def switch_off_power_supply(current_max,dev):
    print('Switching off power supply.....')

    print('Setting communication with power supply Keithley2400....')
    # dev = tango.DeviceProxy('Labo/DG/gpib') # Load device for gpib commnication
    #dev = tango.DeviceProxy('ans-c01/dg/gpib')  # Load device for gpib commnication on RCM
    #dev.init()  # Initialize GPIB device
    time.sleep(2)
    k2400 = keithley2400(dev)  # Create Sourcemeter class object
    print('...................done !')

    # Measure initial voltage
    meas_volt = k2400.measure_voltage()
    voltage_start = np.float(meas_volt[1:])
    print('Initial voltage  = ', voltage_start, ' V')

    # Ramping down parameters
    voltage_step = 1.0
    voltage_temp = voltage_start + 0.

    # Start ramping down
    print('Start ramping down !!!!.................................')
    while voltage_temp > 1.:
        # print('Applying voltage temp.....', voltage_temp, '(V)')
        k2400.apply_voltage(voltage=voltage_temp, compliance_current=current_max)
        time.sleep(2)
        meas_volt = k2400.measure_voltage()
        voltage_temp = np.float(meas_volt[1:])
        print('Voltage temp : ', voltage_temp, ' V')
        meas_curr = k2400.measure_current()
        # print('Current temp :', meas_curr, ' A')
        voltage_temp = voltage_temp - voltage_step
    print('...................done !')

    # Measure final voltage
    meas_volt = k2400.measure_voltage()
    voltage_end = np.float(meas_volt[1:])
    print('Final voltage  = ', voltage_end, ' V')

    # Disable source
    print('Disable source.....')
    k2400.disable_source()
    time.sleep(2)
    print('...................done !')


def set_voltage_power_supply(voltage_value, current_max):
    print('Setting voltage of power supply.......')

    print('Setting communication with power supply Keithley2400....')
    # dev = tango.DeviceProxy('Labo/DG/gpib') # Load device for gpib commnication
    dev = tango.DeviceProxy('ans-c01/dg/gpib')  # Load device for gpib commnication on RCM
    dev.init()  # Initialize GPIB device
    time.sleep(2)
    k2400 = keithley2400(dev)  # Create Sourcemeter class object
    print('...................done !')

    # Measure initial voltage
    meas_volt = k2400.measure_voltage()
    voltage_start = np.float(meas_volt[1:])
    print('Initial voltage  = ', voltage_start, ' V')

    # Ramping parameters
    voltage_step_1 = 1.0
    voltage_step_2 = 5.0
    voltage_margin = 2.
    voltage_temp = voltage_start + 0.

    if voltage_temp < voltage_value:
        # Start fast ramping up
        print('Start fast ramping up !!!!.................................')
        while voltage_temp < voltage_value - voltage_margin:
            # print('Applying voltage temp.....', voltage_temp, '(V)')
            k2400.apply_voltage(voltage=voltage_temp, compliance_current=current_max)
            time.sleep(2)
            meas_volt = k2400.measure_voltage()
            voltage_temp = np.float(meas_volt[1:])
            print('Voltage temp : ', voltage_temp, ' V')
            meas_curr = k2400.measure_current()
            # print('Current temp :', meas_curr, ' A')
            voltage_temp = voltage_temp + voltage_step_2
        print('...................done !')

        # Measure intermediate voltage
        meas_volt = k2400.measure_voltage()
        voltage_temp = np.float(meas_volt[1:])

        # Start slow ramping up
        print('Start slow ramping up !!!!.................................')
        while voltage_temp < voltage_value:
            # print('Applying voltage temp.....', voltage_temp, '(V)')
            k2400.apply_voltage(voltage=voltage_temp, compliance_current=current_max)
            time.sleep(2)
            meas_volt = k2400.measure_voltage()
            voltage_temp = np.float(meas_volt[1:])
            print('Voltage temp : ', voltage_temp, ' V')
            meas_curr = k2400.measure_current()
            # print('Current temp :', meas_curr, ' A')
            voltage_temp = voltage_temp + voltage_step_1
        print('...................done !')

    elif voltage_temp > voltage_value:
        # Start fast ramping down
        print('Start fast ramping down !!!!.................................')
        while voltage_temp > voltage_value + voltage_margin:
            # print('Applying voltage temp.....', voltage_temp, '(V)')
            k2400.apply_voltage(voltage=voltage_temp, compliance_current=current_max)
            time.sleep(2)
            meas_volt = k2400.measure_voltage()
            voltage_temp = np.float(meas_volt[1:])
            print('Voltage temp : ', voltage_temp, ' V')
            meas_curr = k2400.measure_current()
            # print('Current temp :', meas_curr, ' A')
            voltage_temp = voltage_temp - voltage_step_2
        print('...................done !')

        # Measure intermediate voltage
        meas_volt = k2400.measure_voltage()
        voltage_temp = np.float(meas_volt[1:])

        # Start slow ramping up
        print('Start slow ramping up !!!!.................................')
        while voltage_temp > voltage_value:
            # print('Applying voltage temp.....', voltage_temp, '(V)')
            k2400.apply_voltage(voltage=voltage_temp, compliance_current=current_max)
            time.sleep(2)
            meas_volt = k2400.measure_voltage()
            voltage_temp = np.float(meas_volt[1:])
            print('Voltage temp : ', voltage_temp, ' V')
            meas_curr = k2400.measure_current()
            # print('Current temp :', meas_curr, ' A')
            voltage_temp = voltage_temp - voltage_step_1
        print('...................done !')

    # Measure final voltage
    meas_volt = k2400.measure_voltage()
    voltage_end = np.float(meas_volt[1:])
    print('Final voltage  = ', voltage_end, ' V')

    return

#########################################################################################
#########################################################################################
# Functions for read status led
#########################################################################################
#########################################################################################

def led_update():
    status_led = read_register("STATUS_1", hex=True)
    status_led = read_register("STATUS_1", hex=True)
    status_led = int(status_led, 16)
    status_led = '{:032b}'.format(status_led)
    b_status_led=list(bytearray(12))
    text_status_led=['']*12
    # print(status_led)
    if (status_led == '0000000000000000000000000000000000000000'):
        b_status_led=list(bytearray(12))
    else:
        if (status_led[4] != '0'):
            b_status_led[11] = True
            text_status_led[11]=u'\u26A0' + 'Jesd link error, Re-initialize'
        else:
            b_status_led[11] = False
            text_status_led[11]='JESD BOTTOM ERROR OK'
        if (status_led[5] != '0'):
            b_status_led[10] = True
            text_status_led[10]=u'\u26A0' + 'Jesd link error, Re-initialize'
        else:
            b_status_led[10] = False
            text_status_led[10]='JESD TOP ERROR OK'
        if (status_led[6] != '1'):
            b_status_led[9] = True
            text_status_led[9]=u'\u26A0' + 'Jesd readout error, Re-initialize'
        else:
            b_status_led[9] = False
            text_status_led[9]='JESD BOTTOM OK'
        if (status_led[8] != '1'):
            b_status_led[8] = True
            text_status_led[8]=u'\u26A0' + 'Jesd readout error, Re-initialize'
        else:
            b_status_led[8] = False
            text_status_led[8]='JESD TOP OK'
        if (status_led[9] != '1'):
            b_status_led[7] = True
            text_status_led[7]=u'\u26A0' + 'Jesd not in sync, Re-initialize'
        else:
            b_status_led[7] = False
            text_status_led[7]='ADC BOTTOM OK'
        if (status_led[10] != '1'):
            b_status_led[6] = True
            text_status_led[6]=u'\u26A0' + 'Jesd not in sync, Re-initialize'
        else:
            b_status_led[6] = False
            text_status_led[6]='ADC TOP OK'
        if (status_led[11] != '0'):
            b_status_led[5] = True
            text_status_led[5]=u'\u26A0' + 'PLL on-board kalypso error'
        else:
            b_status_led[5] = False
            text_status_led[5]='PLL on-board kalypso OK'
        if (status_led[12] != '0'):
            b_status_led[4] = True
            text_status_led[4]=u'\u26A0' + 'PLL Infrastrucure FPGA'
        else:
            b_status_led[4] = False
            text_status_led[4]='PLL Infrastrucure FPGA OK'
        if (status_led[13] != '0'):
            b_status_led[3] = True
            text_status_led[3]=u'\u26A0' + 'Pll Gotthard BOTTOM error'
        else:
            b_status_led[3] = False
            text_status_led[3]='PLL Gotthard BOTTOM OK'
        if (status_led[14] != '0'):
            b_status_led[2] = True
            text_status_led[2]=u'\u26A0' + 'Pll Gotthard TOP error'
        else:
            b_status_led[2] = False
            text_status_led[2]='PLL Gotthard TOP OK'
        if (status_led[26] != '1'):
            b_status_led[1] = True
            text_status_led[1]=u'\u26A0' + 'PCIe express link lost or could not be established'
        else:
            b_status_led[1] = False
            text_status_led[1]='PCIe link Ok'
        if (status_led[27] != '1'):
            b_status_led[0] = True
            text_status_led[0]=u'\u26A0' + 'DDR not ready'
        else:
            b_status_led[0] = False
            text_status_led[0]='DDR ready'
    return (b_status_led,text_status_led)

def read_register(reg_key, hex=False):
    ## Output from 'pci -r' function in terminal is a string with (rubbish)
    ## + (8 chars for values, starting from [11])
    with open('/home/soleil/kalypso/device/DG-PY-KALYPSO/shell_512/kalypso_registers.json') as registers_file:
        registers = json.load(registers_file)
    pcitool_string = subprocess.check_output(['pci', '-r', registers[reg_key]["address"]])
    if hex == True:
        return pcitool_string[11:19]
    else:
        value = int(pcitool_string[11:19], 16) & int(registers[reg_key]["mask"], 16)
        return value

#########################################################################################
#########################################################################################
# Functions for reading camera settings
#########################################################################################
#########################################################################################

def get_camera_settings(dct_data):
    with open('kalypso_registers.json') as registers_file:
        registers = json.load(registers_file)
    # print('registers = ', registers)
    with open('kalypso_log_conf.json') as log_conf_file:
        log_conf = json.load(log_conf_file)

    # print('log conf = ', log_conf)

    def read_register(reg_key, hex=False):
        ## Output from 'pci -r' function in terminal is a string with (rubbish)
        ## + (8 chars for values, starting from [11])
        pcitool_string = subprocess.check_output(['pci', '-r', registers[reg_key]["address"]])
        if hex == True:
            return pcitool_string[11:19]
        else:
            value = int(pcitool_string[11:19], 16) & int(registers[reg_key]["mask"], 16)
        return value

    for key in log_conf["log_entries"]:
        dct_data[str(registers[key]["name"])] = read_register(key, hex=False)

    return dct_data


#########################################################################################
#########################################################################################
# Functions for tango database reading
#########################################################################################
#########################################################################################

def get_dic_attributes(opt_slow_trigger):
    ''' List attributes to be recorded while recording kalypso data
    - opt_slow_trigger = 0 if wo Slow Trigger ; =1 if with Slow Trigger
    - return: dict of attributes to be recorded
    '''
    dic_attributes = {'current': [['ans/dg/dcct-ctrl'], ['current']],
                      'delay_SY': [['d03-ir-c00/sy/local.1'], ['spare4TimeDelay']],
                      'delay_extraction': [['ans/sy/central'], ['ExtractionDelayTable']],
                      # quart1 =0 ; quart2 =39 ; quart3 =26 ; quart4 =13
                      'mrsv_H_slit': [['ans-c01/dg/mrsv-m.fente'], ['position']],
                      'shutter_kalypso': [['ans-c01/dg/kalypso-shutter'], ['up']],
                      'emittanceH_phc1': [['ans-c02/dg/phc-emit'], ['EmittanceH']],
                      'emittanceV_phc1': [['ans-c02/dg/phc-emit'], ['EmittanceV']],
                      'emittanceH_phc3': [['ans-c16/dg/phc-emit'], ['EmittanceH']],
                      'emittanceV_phc3': [['ans-c16/dg/phc-emit'], ['EmittanceV']],
                      'SrcPointSigmaH_phc1': [['ans-c02/dg/phc-emit'], ['SrcPointSigmaH']],
                      'SrcPointSigmaV_phc1': [['ans-c02/dg/phc-emit'], ['SrcPointSigmaV']],
                      'SrcPointSigmaH_phc3': [['ans-c16/dg/phc-emit'], ['SrcPointSigmaH']],
                      'SrcPointSigmaV_phc3': [['ans-c16/dg/phc-emit'], ['SrcPointSigmaV']]}
    if opt_slow_trigger == 0:
        dic_attributes['BPM13_x'] = [['ans-c01/dg/bpm.3'], ['XPosSA']]
        dic_attributes['BPM13_z'] = [['ans-c01/dg/bpm.3'], ['ZPosSA']]
        dic_attributes['BPM14_x'] = [['ans-c01/dg/bpm.4'], ['XPosSA']]
        dic_attributes['BPM14_z'] = [['ans-c01/dg/bpm.4'], ['ZPosSA']]
    elif opt_slow_trigger == 1:
        dic_attributes['BPM13_x'] = [['ans-c01/dg/bpm.3'], ['XPosDD']]
        dic_attributes['BPM13_z'] = [['ans-c01/dg/bpm.3'], ['ZPosDD']]
        dic_attributes['BPM14_x'] = [['ans-c01/dg/bpm.4'], ['XPosDD']]
        dic_attributes['BPM14_z'] = [['ans-c01/dg/bpm.4'], ['ZPosDD']]
    return dic_attributes


def read_attr(device_name, attr_name):
    ''' Read attribute of a device
    Returns attribute value = un tableau avec valeur attribut et couleur state
    '''
    dev = tango.DeviceProxy(device_name)
    attrData = dev.read_attributes([attr_name])
    attrVal = attrData[0].value
    return attrVal


def get_machine_settings(opt_slow_trigger, dct_data):
    dic_attributes = get_dic_attributes(opt_slow_trigger)
    # print('dic_attributes', dic_attributes)
    nb_attributes = len(dic_attributes)
    for i in range(nb_attributes):
        field_name = list(dic_attributes)[i]
        # print('field_name', field_name)
        device_name = dic_attributes[field_name][0][0]
        # print('device name', device_name)
        attr_name = dic_attributes[field_name][1][0]
        attrVal = read_attr(device_name, attr_name)
        # print('val', attrVal)
        # dct_data[field_name].append(attrVal)
        dct_data[field_name] = attrVal
    return dct_data
