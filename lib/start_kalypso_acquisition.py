import numpy as np
import matplotlib.pyplot as plt
import os
import os.path
import subprocess
import time
import strip_data

from kalypso_tools_for_acquisition import *
from kalypso_tools_for_analysis import *


####################################################################
# Define loop for acquisition
####################################################################
opt_loop = 0

if opt_loop == 0:
	n_var = 1
	variable_name = ''
	print('No loop')

elif opt_loop == 1:
	var_min = 15
	var_max = 75
	n_var = 4
	variable = np.linspace(var_min, var_max, n_var)
	print(variable)
	variable_name = 'delay'
	print('Looping on : '+ variable_name)	

for i_loop in range(n_var):
	print('Start looping......')
	print('         .... recording step : ' + str(i_loop))
	####################################################################
	# Define optical Beamline Parameters (OBL)
	####################################################################
	dct_data_obl = {}
	dct_data_obl['pixel_size'] = 25. # Kalypso detector pixel size (micron)
	dct_data_obl['beam'] = 'Z' # X or Z Beam diemnsion imaged on Kalypso camera
	dct_data_obl['BP_filter'] = 'CL600nm_BW80nm' # BandPass filter for spectral selection
	dct_data_obl['Polarization'] = 'H' # Polarization of SR on Kalypso
	dct_data_obl['dist_FirstHole_L1'] = 210.+120.+130.+30.+330. # (mm) Dist. from first hole on optical table to first cylindric lens L1
	dct_data_obl['dist_L1_L2'] = 230. # Distance from L1 to second cylindric lens L2
	dct_data_obl['dist_L2_cam'] = 115. # Distance from L2 to Kalypso detector
	print('Optical beam line data', dct_data_obl)


	####################################################################
	# Define acquisition parameters
	####################################################################
	# Starting parameters
	opt_nb_pixels = 512 # 512 or 1024
	opt_start = 0

	# Acquisition parameters
	exp_time = 295 # 240 ; 472 ; 708 ; 944  # (ns) To be converted into multiples of 8 ns
	# MEMO
	# 1 turn = 1180 ns
	# 1 quart = 295 ns
	# 2 quarts = 590 ns
	# 3 quarts = 886 ns
	# 4 quarts = 1180 ns 
	nb_frames = 2048 # 100 / 256 / 512 / 1024 / 2048

	# Fast trigger parameters
	delay = np.int(280./8.) #np.int(variable[i_loop]) # 40 # (-) must be > 0. To be converted into ns using delay X 8 ns. Single bunch = 127 
	print('Delay = ', delay)
	# MEMO
	# 127 = Delay for single bunch / isolated bunch in hybrid (middle of Q4)
	# 108 = 127 - (295/2)/8 = Delay to start recording at begining of Q4
	# 146 = 127 + (295/2)/8 = Delay to start recording at begining of Q1
	# 183 = 146. + 295./8. = Delay to start recording at begining of Q2
	# 220 = 146. + 295./8.*2. = Delay to start recording at begining of Q3
	# 257 = 146. + 295./8.*3. = Delay to start recording at begining of Q4
	# 147 * 8 = 1180 ns
	opt_fast_trigger = 0 # 0 = external # else (int) = divider, where frame rate will be = 126 MHz / divider 
	# MEMO
	#>>> 126./148 = 0.8513513513513513 = 851 kHz
	#>>> 126./150 = 0.84 = 840 kHz


	# Slow trigger parameters
	opt_slow_trigger = 1 # 0 = internal ; 1 = external Slow Trigger
	nb_acq_w_slow_trigger = 10
	nb_skip_slow_trigger = 0
	
	####################################################################
	# Start Kalypso system
	####################################################################
	if opt_start ==1:
		if opt_nb_pixels == 512:
			start_system_w_512_pixels()
	
		elif opt_nb_pixels == 1024:
			start_system_w_1024_pixels()

	else:
		print('Kalypso system not restarted !')

	####################################################################
	# Configure Kalypso for acquisition
	####################################################################
	print('Setting acquisition parameters.....')
	set_acquisition_parameters(opt_nb_pixels, exp_time, nb_frames, opt_fast_trigger, delay, opt_slow_trigger, nb_acq_w_slow_trigger, nb_skip_slow_trigger)

	####################################################################
	# Start data acquisition
	####################################################################	
	fileroot = 'data_'+str(opt_nb_pixels) + 'pix_' + str(dct_data_obl['beam']) + '_' + variable_name + 'n' + str(i_loop)
	if opt_nb_pixels == 512:
		dirname = '/home/soleil/kalypso/data/'
		filename_bckg = '' #'data_512pix_DT8ns_samples1024_U1V_delay127_bckg.bin'
		output_file_root = record_data(dirname, fileroot, opt_slow_trigger, nb_acq_w_slow_trigger, dct_data_obl)
		opt_bckg = 0
		plot_data_raw(dirname, output_file_root[0], opt_nb_pixels, opt_bckg)
		#data_analysis = plot_data_w_analysis(dirname, output_file_root[0], opt_nb_pixels, opt_bckg, opt_show=1)
	
	elif opt_nb_pixels == 1024:
		dirname = '/home/soleil/kalypso/data/'
		print('TO BE DONE !!!')







