import tango
import numpy as np
import time


class keithley2400(object):
    # Constructeur
    def __init__(self, dev):
        self.dev = tango.DeviceProxy(dev)

    def id(self):
        """ Requests and returns the identification of the instrument. """
        return self.ask("*IDN?").strip()

    def version(self):
        """ Requests and returns the version of the instrument. """
        return self.ask(":SYST:VERS?").strip()

    def restoreGPIB(self):
        """ Request a reset SourceMeter return into the default GPIB conditions"""
        self.write("*RST")

    def status(self):
        """ ? """
        return self.ask("status:queue?;")

    def ask(self, command):
        """ Writes the command to the instrument through the adapter
        and returns the read response.

        :param command: command string to be sent to the instrument
        """
        return self.dev.command_inout('WriteRead', command)

    def write(self, command):
        """ Writes the command to the instrument through the adapter.

        :param command: command string to be sent to the instrument
        """
        self.dev.command_inout('Write', command)

    def read(self):
        """ Reads from the instrument through the adapter and returns the
        response.
        """
        return self.dev.command_inout('Read')

    def enable_source(self):
        """ Enables the source of current or voltage depending on the
        configuration of the instrument. """
        self.write(":OUTP ON")

    def disable_source(self):
        """ Disables the source of current or voltage depending on the
        configuration of the instrument. """
        self.write(":OUTP OFF")

    def get_source_state(self):
        """ Requests and returns the state of the output. True:ON; False:OFF  """
        return bool(int(self.ask(":OUTP?").strip()))

    def auto_range_source(self):
        """ Configures the source to use an automatic range.
        """
        if self.source_mode == 'current':
            self.write(":SOUR:CURR:RANG:AUTO 1")
        else:
            self.write(":SOUR:VOLT:RANG:AUTO 1")

    def compliance_voltage(self, compliance_voltage=0.1):
        self.write(":SENS:VOLT:PROT %E" % compliance_voltage)

    def compliance_current(self, compliance_current):
        self.write(":SENS:CURR:PROT %E" % compliance_current)

    def read_compliance_current(self):
        self.ask(":SENS:CURR:PROT?")

    def read_compliance_voltage(self):
        self.ask(":SENS:VOLT:PROT?")

    def apply_current(self, current=0, current_range=None, compliance_voltage=0.1):
        """ Configures the instrument to apply a source current, and
        uses an auto range unless a current range is specified.
        The compliance voltage is also set.

        :param compliance_voltage: A float in the correct range for a
                                   :attr:`~.Keithley2400.compliance_voltage`
        :param current_range: A :attr:`~.Keithley2400.current_range` value or None
        """

        self.source_mode = 'current'
        if current_range is None:
            self.auto_range_source()
        else:
            self.source_current_range = current_range
        self.compliance_voltage(compliance_voltage)
        # self.check_errors()

    def apply_voltage(self, voltage=0, voltage_range=None, compliance_current=0.0001):
        """ Configures the instrument to apply a source voltage, and
        uses an auto range unless a voltage range is specified.
        The compliance current is also set.

        :param compliance_current: A float in the correct range for a
                                   :attr:`~.Keithley2400.compliance_current`
        :param voltage_range: A :attr:`~.Keithley2400.voltage_range` value or None
        """
        self.write(':SOUR:FUNC VOLT')  # mode tension
        self.write(':SOUR:VOLT:MODE FIXED')  # source fixe
        self.source_mode = 'voltage'
        if voltage_range is None:
            self.auto_range_source()
        else:
            self.source_voltage_range = voltage_range
        self.compliance_current(compliance_current)
        self.set_voltage(voltage)
        # self.check_errors()

    def set_voltage(self, voltage):

        self.write(":SOUR:VOLT:LEV %g" % voltage)

    def set_current(self, current):
        self.write(":SOUR:CURR:LEV %g" % current)

    def queryConfigure(self):
        res = self.ask(":CONF?").strip()
        return res

    def ConfigureMeasCurrent(self):
        self.write(':CONF:CURR')
        # print(self.queryConfigure())

    def ConfigureMeasVoltage(self):
        self.write(':CONF:VOLT')
        # print(self.queryConfigure())

    def measure_voltage(self):
        self.write(':SENS:FUNC "VOLT"')  # select voltage measure
        self.write(':SENS:VOLT:RANG:AUTO ON')
        self.write(":SENS:VOLT:NPLC 1;:FORM:ELEM VOLT;")
        res = self.queryConfigure()
        return self.ask(":MEAS:VOLT?").strip()

    def measure_current(self):
        self.write(':SENS:FUNC "CURR"')
        self.write(':SENS:CURR:RANG:AUTO ON')
        self.write(':FORM:ELEM CURR;')
        res = self.queryConfigure()
        self.ConfigureMeasCurrent()
        cur = None
        try:
            cur = self.ask(":MEAS:CURR?").strip()
        except Exception as ex:
            print('Failed first current read')
            self.dev.init()
            time.sleep(3)
            cur = self.read().strip()  # read the data stuck after ask time-Out

        return cur

    def beep(self, frequency, duration):
        """ Sounds a system beep.

        :param frequency: A frequency in Hz between 65 Hz and 2 MHz
        :param duration: A time in seconds between 0 and 7.9 seconds
        """
        self.write(":SYST:BEEP %g, %g" % (frequency, duration))
        time.sleep(duration)


