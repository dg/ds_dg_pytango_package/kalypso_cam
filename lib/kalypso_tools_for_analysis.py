import numpy as np
import matplotlib.pyplot as plt
import os
import os.path
import subprocess
import time
from .strip_data import strip_data
import h5py
from scipy.optimize import curve_fit
from scipy.special import wofz

def get_image(filename, NUMBER_PIXELS):
	"""
	- filename : filepath + filename .bin
	- NUMBER_PIXELS = 512 or 1024
	- send back the shaped image 
	"""
	# Load data
	data, nb_acq  = strip_data(filename, NUMBER_PIXELS, 0)
	data = np.reshape(data, (-1,nb_acq,NUMBER_PIXELS))
	image = np.squeeze(data)
	return image

def plot_data_raw(dirname, filename, NUMBER_PIXELS, opt_bckg, filename_bckg='None'):
	"""
	- dirname : directory for data location
	- filename : filename root without extension (nore .bin nore .h5 etc...)
	- NUMBER_PIXELS = 512 or 1024
	- opt_bckg = 0 or 1 to take into account a bckg file
	- filename_bckg = file to use for background
	- plot image
	"""

	# Load image data
	#########################################
	image = get_image(os.path.join(dirname , filename + '.bin'), NUMBER_PIXELS)
	nb_acq, nb_pix = np.shape(image)

	# Scale data
	#########################################
	y_scale = np.arange(0, NUMBER_PIXELS)
	x_scale = np.arange(0, nb_acq)

	# Load data for background
	#########################################
	if opt_bckg == 1:
		image_bckg = get_image(dirname + filename_bckg, NUMBER_PIXELS)
	elif opt_bckg == 0:
		image_bckg = 0.	

	# Remove background
	#########################################
	image_f = image - image_bckg
	
	# Display image
	#########################################
	f1 = plt.figure(1, figsize=[10,5])
	#
	ax11 = f1.add_subplot(111)
	im11 = ax11.pcolormesh(x_scale, y_scale, np.transpose(image_f), cmap = 'inferno')
	ax11.set_xlabel('Time ()')
	ax11.set_ylabel('Pixels')
	f1.colorbar(im11, ax=ax11)
	ax11.set_title(filename)
	#
	plt.show()
	
	return

def plot_data_w_analysis(filename, NUMBER_PIXELS, opt_bckg, filename_bckg='None', opt_show=1):
	"""
	- filename : filename root without extension (nore .bin nore .h5 etc...)
	- NUMBER_PIXELS = 512 or 1024
	- opt_bckg = 0 or 1 to take into account a bckg file
	- filename_bckg = file to use for background
	- plot image
	- analyze image frame by frame
	- return data_analysis = results of analysis
	"""

	#########################################
	# Load all settings
	#########################################
	dct_data = {}
	hf = h5py.File(filename + '.h5', 'r')
	for k in hf.keys():
		#print('k', k)
		dct_data[k] = np.array(hf.get(k))
	hf.close()
	pixel_size = dct_data['pixel_size'] # (micron)
	print('pixel size', pixel_size)
	beam = dct_data['beam']
	print('beam = ', beam)
	print('beam = ', str(beam))
	if dct_data['Number of slow-triggers to acquire'] == 0:
		opt_slow_trigger = 0
	else:
		opt_slow_trigger = 1
	print('opt_slow_trigger', opt_slow_trigger)
	print('delay_SY = ', dct_data['delay_SY'])

	#########################################
	# Load camera data
	#########################################
	# Load image data
	image = get_image(filename + '.bin', NUMBER_PIXELS)
	nb_acq, nb_pix = np.shape(image)

	# Scale data
	y_scale = np.arange(0, NUMBER_PIXELS)
	x_scale = np.arange(0, nb_acq)

	# Load image data for background
	if opt_bckg == 1:
		image_bckg = get_image(dirname + filename_bckg + '.bin', NUMBER_PIXELS)
	elif opt_bckg == 0:
		image_bckg = 0.	

	# Remove background
	image_f = image - image_bckg
	
	#########################################
	# Figure for image
	#########################################
	f1 = plt.figure(1, figsize=[10,5])
	#
	ax11 = f1.add_subplot(111)
	im11 = ax11.pcolormesh(x_scale, y_scale, np.transpose(image_f), cmap = 'inferno')
	ax11.set_xlabel('Time ()')
	ax11.set_ylabel(beam + ' (pixels)')
	f1.colorbar(im11, ax=ax11)
	ax11.set_title(filename)

	#########################################
	# Figure for vertical profiles
	#########################################
	f2 = plt.figure(2, figsize=[10,5])
	ax21 = f2.add_subplot(111)

	#########################################
	# Figure for frame by frame analysis
	#########################################
	f3 = plt.figure(3, figsize=[10,8])
	ax31 = f3.add_subplot(311)
	ax32 = f3.add_subplot(312)
	ax33 = f3.add_subplot(313)

	#########################################
	# Extract horizontal intensity profile
	#########################################
	int_tot = np.sum(np.sum(image_f, axis = 1), axis=0)
	hprof_tot = np.sum(image_f, axis = 1)
	hprof_tot = hprof_tot - np.mean(hprof_tot)
	ax31.plot(hprof_tot, '-b', label = 'Kalypso')
	ax31.set_xlim([x_scale[0], x_scale[-1]])
	ax31.set_ylabel('Tot. intensity (counts)')
	ax31.grid(True)
	ax31.legend()
	ax31.set_title(filename)

	#########################################
	# Analyze frame by frame
	#########################################
	fwhm = np.zeros((nb_acq))
	fwhm_fit_V = np.zeros((nb_acq))
	pos = np.zeros((nb_acq))
	pos_fit_V = np.zeros((nb_acq))
	pos_fit_V2 = np.zeros((nb_acq))
	noise_val_i = np.zeros((nb_acq))
	noise_val_f = np.zeros((nb_acq))
	max_val = np.zeros((nb_acq))
	for j in range(nb_acq):
		# Extract centroid of beam
		y_max_loc = np.argmax(image_f[j,:])
		pos[j] = y_max_loc 
		# Extract beam profile
		vprof = image_f[j,:]
		noise_val_i[j] = vprof[0]
		noise_val_f[j] = vprof[-1]
		max_val[j] = np.max(vprof)
		vprof = vprof - np.min(vprof)
		vprof = vprof / np.max(vprof)
		fwhm[j] = get_prof_fwhm(x_scale, vprof)
		# Fit with Voigt
		vprof_guess = voigt_func(y_scale, 200., 10., 10.)
		popt, pcov = curve_fit(voigt_func, y_scale, vprof, p0=[y_max_loc, 10., 10.])
		#popt = [250., 10., 0.]
		vprof_fit_V = voigt_func(y_scale, *popt)
		if noise_val_i[j]  > 819:
			pos_fit_V[j] = popt[0]
			pos_fit_V2[j] = np.nan
		else:
			pos_fit_V[j] = np.nan
			pos_fit_V2[j] = popt[0]
		f_G = 2*popt[1]
		f_L = 2*popt[2]
		fwhm_fit_V[j] = 0.5346*f_L + np.sqrt(0.2166*f_L**2 + f_G**2)
		# Plot profiles
		if j>0: #j>300 and j<400:
			ax21.plot(vprof, '-b', linewidth = 0.1)
			if noise_val_i[j]  > 819:
				ax21.plot(vprof_fit_V, '-g', linewidth = 0.1)
			else:
				ax21.plot(vprof_fit_V, '-m', linewidth = 0.1)
	pos = pos #- np.mean(pos)
	#pos_fit_G = pos_fit_G - np.mean(pos_fit_G)
	pos_fit_V = pos_fit_V #- np.mean(pos_fit_V)
	fwhm = fwhm #- np.mean(fwhm[0:100])
	#fwhm_fit_G = fwhm_fit_G - np.mean(fwhm_fit_G[0:100])
	fwhm_fit_V = fwhm_fit_V #- np.mean(fwhm_fit_V[0:100])
	vprof_tot = np.sum(image_f, axis = 0)
	vprof_tot = vprof_tot - np.min(vprof_tot)
	vprof_tot = vprof_tot / np.max(vprof_tot)

	#########################################
	# Complete figures
	#########################################
	# Vertical profiles
	#ax21.plot(vprof_tot, '-k', linewidth = 3)
	ax21.set_xlabel(r'$'+beam+'$ (pixels)')
	ax21.set_ylabel('Intensity (counts)')
	ax21.grid(True)
	ax21.set_title(filename)
	# Position along camera axis
	ax32.plot(pos, '-b', label = 'Kalypso')
	ax32.plot(pos_fit_V, '-g', label = 'Kalypso + fit Voigt')
	#ax32.plot(pos_fit_V2, '-m', label = 'Kalypso + fit Voigt')
	if opt_slow_trigger == 1:
		if beam == 'X':
			data_bpm_3 = dct_data['BPM13_x']*1.e3 # (microns)
			data_bpm_3 = data_bpm_3 - np.mean(data_bpm_3)
			data_bpm_4 = dct_data['BPM14_x']*1.e3 # (microns)
			data_bpm_4 = data_bpm_4 - np.mean(data_bpm_4)
			x_scale_bpm = np.arange(0, len(data_bpm_3))
			#ax32.plot(x_scale_bpm + 400., data_bpm_3, '-r', label='BPM 3')
			#ax32.plot(x_scale_bpm + 400., data_bpm_4, '-m', label='BPM 4')
		elif beam == 'Z':
			data_bpm_3 = dct_data['BPM13_z']*1.e3 # (microns)
			data_bpm_3 = data_bpm_3 - np.mean(data_bpm_3)
			data_bpm_4 = dct_data['BPM14_z']*1.e3 # (microns)
			data_bpm_4 = data_bpm_4 - np.mean(data_bpm_4)
			x_scale_bpm = np.arange(0, len(data_bpm_3))
			#ax32.plot(x_scale_bpm + 400., data_bpm_3, '-r', label='BPM 3')
			#ax32.plot(x_scale_bpm + 400., data_bpm_4, '-m', label='BPM 4')
	elif opt_slow_trigger == 0:
		if beam == 'X':
			data_bpm_3 = dct_data['BPM13_x']*1.e3 # (microns)
			data_bpm_4 = dct_data['BPM14_x']*1.e3 # (microns)
			x_scale_bpm = [0]
		elif beam == 'Z':
			data_bpm_3 = dct_data['BPM13_z']*1.e3 # (microns)
			data_bpm_4 = dct_data['BPM14_z']*1.e3 # (microns)
			x_scale_bpm = [0]

	ax32.set_xlim([x_scale[0], x_scale[-1]])
	#ax32.set_xlabel('Time ()')
	ax32.set_ylabel(r'$' + beam +'$ ($\mu$m)')
	ax32.grid(True)
	ax32.legend()
	# Size along vertical axis
	ax33.plot(fwhm, '-b', label = 'Kalypso')
	#ax33.plot(fwhm_fit_G, '-r', label = 'Kalypso + fit Gauss')
	ax33.plot(fwhm_fit_V, '-g', label = 'Kalypso + fit Voigt')
	ax33.set_xlim([x_scale[0], x_scale[-1]])
	ax33.set_xlabel('Time ()')
	ax33.set_ylabel(r'$\Sigma_{'+beam+'}$ ($\mu$m-fwhm)')
	ax33.grid(True)
	ax33.legend()

	#########################################
	# Figure for noise analysis
	#########################################
	f4 = plt.figure(4, figsize=[10,8])
	ax41 = f4.add_subplot(111)
	ax41.plot(noise_val_i, max_val, 'ob')
	ax41.plot(noise_val_f, max_val, 'og')

	#########################################
	# Store output data
	#########################################
	data_analysis = {}
	data_analysis['Integration Delay'] = dct_data['Integration Delay']
	data_analysis['Integration Duration'] = dct_data['Integration Duration']
	data_analysis['beam'] = beam	
	data_analysis['int_tot'] = int_tot
	data_analysis['hprof_tot'] = hprof_tot
	data_analysis['vprof_tot'] = vprof_tot
	data_analysis['pos'] = pos
	data_analysis['fwhm'] = fwhm
	data_analysis['pos_fit_V'] = pos_fit_V
	data_analysis['fwhm_fit_V'] = fwhm_fit_V
	data_analysis['opt_slow_trigger'] = opt_slow_trigger	
	if opt_slow_trigger == 1:
		data_analysis['data_bpm_3'] = data_bpm_3
		data_analysis['data_bpm_4'] = data_bpm_4
		data_analysis['x_scale_bpm'] = x_scale_bpm
	elif opt_slow_trigger == 0:
		data_analysis['data_bpm_3'] = data_bpm_3
		data_analysis['data_bpm_4'] = data_bpm_4
		data_analysis['x_scale_bpm'] = x_scale_bpm
	
	#########################################
	# Show figures
	#########################################
	if opt_show==1:
		plt.show()	
	elif opt_show==0:
		plt.close(f1)
		plt.close(f2)
		plt.close(f3)
		plt.close(f4)

	return data_analysis

def get_prof_fwhm(scale, prof):
	"""
	- scale : linear array with profile scale
	- prof: linear array with profile intensity
	- send back the fwhm of the profile
	"""
	max_arg = np.argmax(prof)
	max_loc = scale[max_arg]
	max_val = np.max(prof)
	fwhm1 = scale[np.argmin(np.abs(prof[:max_arg] - max_val/2.))]
	fwhm2 = scale[np.argmin(np.abs(prof[max_arg:] - max_val/2.)) + max_arg]
	fwhm = fwhm2 - fwhm1
	return fwhm


def linear_func(x, a, b):
	"""
	- x: scale along x
	- a: linear coefficient
	- b: offset
	- retun y = linear function along x
	"""
	y = a*x + b
	return y

def gauss_func(x, x0, sigma, c):
	"""
	- x: scale along x
	- x0: center of gaussian
	- sigma: rms width of gaussian
	- retun y = gaussian function along x
	"""
	y = np.exp(-(x - x0)**2 / (2*sigma**2)) +c
	y = y / np.max(y)
	return y

def voigt_func(x, x0, alpha, gamma):
	"""
	Return the Voigt line shape at x with Lorentzian component HWHM gamma
	and Gaussian component HWHM alpha.
	"""
	sigma = alpha / np.sqrt(2 * np.log(2))
	y = np.real(wofz(((x-x0) + 1j*gamma)/sigma/np.sqrt(2))) / sigma\
                                                           /np.sqrt(2*np.pi)
	y = y/np.max(y)
	return y

def sinus_func(x, a, b, T0, phi):
	"""
	- x: scale along x
	- a: linear coefficient
	- b: offset
	- T0: period
	- phi: phase
	- retun y = sinus function along x
	"""
	y = a*np.sin(2*np.pi*x/T0 + phi) + b
	return y

def square_func(x, a, b, c):
	"""
	- x: scale along x
	- a b, c: poly coef
	- retun y 
	"""
	y = a*x**2 + b*x + c
	return y
