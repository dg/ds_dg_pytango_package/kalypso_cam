import os
import numpy as np
from subprocess import Popen,PIPE,STDOUT
import time




class kalypso(object):
    def __init__(self):
        # Load PCILIB environment
        os.environ['PCILIB_MODEL'] = 'ipedma'
        self.opt_nb_pixels=512
        self.cwd_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "shell_512")

    def pci_write(self,register,value):
        cmd=["pci","-w",str(register),str(value)]
        p=Popen(cmd, shell=True, cwd=self.cwd_path,stdin=PIPE, stdout=PIPE, stderr=STDOUT).wait()
        output=p.stdout.read()
        return output

    def pci_read(self,register):
        cmd = ["pci", "-r", str(register)]
        p=Popen(cmd, shell=True, cwd=self.cwd_path,stdin=PIPE, stdout=PIPE, stderr=STDOUT).wait()
        output = p.stdout.read()
        return output

    def reset(self):
        print('##########################################################')
        print('       Start Kalypso system with 512 pixels               ')
        print('##########################################################')

        # Load PCILIB environment
        os.environ['PCILIB_MODEL'] = 'ipedma'
        cwd_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "shell_512")

        # Start sequence
        print('GLOBAL RESET ... ')
        Popen(["./reset_ex_rf.sh"], shell=True, cwd=cwd_path).wait()
        time.sleep(0.5)

        print('PLL INIT/CONF for KARA 62.5 MHz ')
        Popen(["./pll_62_5.sh"], shell=True,cwd=cwd_path).wait()
        time.sleep(0.1)

        print('AXI_MASTER on FPGA INIT ...')
        Popen(["./AXI_conf.sh"], shell=True,cwd=cwd_path).wait()
        time.sleep(0.1)

        print('AXI_Reconfiguration for SubClass 2, frame per Multi-frame = 16')
        Popen(["./AXI_ReConf.sh"], shell=True,cwd=cwd_path).wait()
        time.sleep(0.1)

        print('ADCs INIT/CONF SubClass 2, frame per Multi-frame = 16')
        Popen(["./adc_config.sh"], shell=True,cwd=cwd_path).wait()
        time.sleep(0.1)

        print('SOFT RESET ...')
        Popen(["./soft_reset.sh"], shell=True,cwd=cwd_path).wait()
        time.sleep(0.1)

        print('SEND TX_TRIG both ADCs ...')
        Popen(["./tx_trig_adcs.sh"], shell=True,cwd=cwd_path).wait()
        time.sleep(0.1)

        print('Gotthard ON ... ')
        Popen(["./G_ON.sh"], shell=True,cwd=cwd_path).wait()
        time.sleep(0.1)

        print('Number of acquisitions to acquire (Slow Trig.)= 1')
        Popen(["pci -w 0x9024 0x1"], shell=True,cwd=cwd_path).wait()
        time.sleep(0.1)
        Popen(["pci -w 0x9024 0x0"], shell=True,cwd=cwd_path).wait()  # for internal slow Triger #
        time.sleep(0.1)

        print('Number of acquisitions to skip (Slow Trig.)= 0')
        Popen(["pci -w 0x902C 0x0"], shell=True,cwd=cwd_path).wait()
        time.sleep(0.1)

        print('Number of frame to acquire (Fast Trig.)= 1024')
        Popen(["pci -w 0x9020 0x400"], shell=True,cwd=cwd_path).wait()
        time.sleep(0.1)

        print('Number of frame to skip (Fast Trig.)... = 1')
        Popen(["pci -w 0x9028 0x1"], shell=True,cwd=cwd_path).wait()
        time.sleep(0.1)
        Popen(["pci -w 0x9028 0x0"], shell=True,cwd=cwd_path).wait()
        time.sleep(0.1)

        print('Doing something ????')
        Popen(["pci -w 0x910C 0"], shell=True,cwd=cwd_path).wait()
        time.sleep(0.1)
        Popen(["./adc_offset_at_0_delay.sh"], shell=True,cwd=cwd_path).wait()
        time.sleep(0.1)
        Popen(["./offset_soleil.sh"], shell=True,cwd=cwd_path).wait()
        time.sleep(0.1)

        print('Doing something ????')
        Popen(["pci -w 0x910C 33"], shell=True,cwd=cwd_path).wait()  # with 904c 3009
        time.sleep(0.1)


        print('>>>>>>>>>> System started')

    def set_acquisition_parameters(self,opt_nb_pixels, exp_time, nb_frames, opt_fast_trigger, delay, opt_slow_trigger,
                                       nb_acq_w_slow_trigger, nb_skip_slow_trigger):
            #######################################################
            # Basic loadings...
            #######################################################



            # Get into directory proper 512/1024 pixels directory
            if opt_nb_pixels == 512:
                os.chdir('/home/soleil/kalypso/soleil_GUI/KALYPSO2_5_GUI_512/')
            elif opt_nb_pixels == 1024:
                os.chdir('/home/soleil/kalypso/soleil_GUI/KALYPSO2_5_GUI_1024/')

            # Initialize output file
            output_file_temp = 'temp.bin'
            cmd = 'rm ' + output_file_temp
            try:
                Popen([cmd], shell=True).wait()
                time.sleep(0.1)
            except:
                print('No file to be deleted...')

            #######################################################
            # Acquisition parameters
            #######################################################
            # Exposure time
            print('GOTT: Exposure time (ns) = ', exp_time)
            exp_time_counts = np.int(exp_time / 8.)  # Convert ns to multiple of 8 ns
            exp_time_counts_hex = hex(exp_time_counts)
            cmd = "pci -w 0x9000 " + str(exp_time_counts_hex)
            Popen([cmd], shell=True).wait()
            time.sleep(0.1)

            # Gain
            print('Set gain: TO BE DONE !!!!!.........')

            #######################################################
            # Fast trigger parameters
            #######################################################
            # Fast trigger mode
            print('Setting fast trigger to = ', opt_fast_trigger)
            opt_fast_trigger_hex = hex(opt_fast_trigger)
            print('Setting fast trigger hex to = ', opt_fast_trigger_hex)
            cmd = "pci -w 9044 " + str(opt_fast_trigger_hex)
            print('cmd = ', cmd)
            Popen([cmd], shell=True).wait()

            # Delay w.r. Fast Trigger
            print('GOTT: Delay w.r. Fast trigger (ns) = ', delay)
            cmd = "pci -w 0x9004 " + str(hex(delay))
            Popen([cmd], shell=True).wait()
            time.sleep(0.1)

            # Number of frames to record with Fast Trigger
            print('Number of frames to acquire (Fast Trig.) = ', nb_frames)
            if nb_frames == 100:
                cmd = "pci -w 0x9020 0x64"
            elif nb_frames == 256:
                cmd = "pci -w 0x9020 0x100"
            elif nb_frames == 512:
                cmd = "pci -w 0x9020 0x200"
            elif nb_frames == 1024:
                cmd = "pci -w 0x9020 0x400"
            elif nb_frames == 2048:
                cmd = "pci -w 0x9020 0x800"
            else:
                print('Default setting: 100 frames')
            # print('********************* No such nb of frames encoded !!!************************')
            # sys.exit()
            Popen([cmd], shell=True).wait()
            time.sleep(0.1)

            # Number of shots to skip from Fast Trigger
            print('Number of frame to skip (Fast Trig.)... = ')
            # subprocess.Popen(["pci -w 0x9028 0x1"], shell=True).wait()
            Popen(["pci -w 0x9028 0x0"], shell=True).wait()
            time.sleep(0.1)

            #######################################################
            # Slow trigger parameters
            #######################################################
            # Slow Trigger option
            print('Using Slow Trigger option = ', opt_slow_trigger)

            # Number of acq. using Slow Trigger
            print('Number of acquisitions to acquire (ext. Slow Trig.) = ', nb_acq_w_slow_trigger)
            Popen(["pci -w 0x9024 0x1"], shell=True).wait()  # To initialize the slow trigger (mandatory)
            if opt_slow_trigger == 0:
                cmd = "pci -w 0x9024 0x0"
                Popen([cmd], shell=True).wait()  # Using internal Slow Trigger
            elif opt_slow_trigger == 1:
                cmd = "pci -w 9024 " + str(nb_acq_w_slow_trigger)
                Popen([cmd], shell=True).wait()  # Using external Slow Trigger
            time.sleep(1.0)

            # Number of shots to skip from Slow Trigger
            print('Number of Slow Triggers to skip (ext. Slow Trig.) = ', nb_skip_slow_trigger)
            cmd = "pci -w 0x902C 0x" + str(nb_skip_slow_trigger)
            Popen([cmd], shell=True).wait()
            time.sleep(0.1)

            #######################################################
            # Basic endings....
            #######################################################
            # Get back to directory for scripts
            os.chdir('/home/soleil/kalypso/scripts/')

            print('>>>>>>>>>> Parameters set')

            return
    def power_supply_on(self):
    def power_supply_off(self):
    def status(self):
    def snap(self):


if __name__ == "__main__":
   import kalypso
   dev=kalypso.kalypso()
   dev.reset()
   dev.power_supply_on()
   dev.status()
