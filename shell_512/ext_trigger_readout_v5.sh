 #!/bin/bash
export PCILIB_MODEL=ipedma
usage="
$(basename "$0") [-h] [-f NNNNN] [-d AAAA] [-a n] [-s n]
-- script to acquire with KALYPSO with external trigger
------------------------------------------------
where:
    -h  show this help text

    -f  set the fill number (default: 12345)
    -d  set the detector name (default: Si01)
    -a  set the number of triggers to acquire (default: 8)
    -s  set the skip value (default: 0)

    Note: to stop acquisition, create a 'stop_aq' file in the running directory
------------------------------------------------"

# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Initialize our own variables:
# fill_num="12345"
# det_name="Si01"
extension=".bin"
acquisitions=1000
skip=0
div="_"
fill_prefix="f"

fill_numFlag=false
det_nameFlag=false

bg_delay=20
sig_delay=49

bum="m"

while getopts 'hd:f:a:s' opt; do
    case "$opt" in
    h)  echo "$usage"
        exit
        ;;
    a)  acquisitions=$OPTARG
        ;;
    s)  skip=$OPTARG
        ;;
    f)  fill_num=$OPTARG
        fill_numFlag=true
        ;;
    d)  det_name=$OPTARG
        det_nameFlag=true
        ;;
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift

if ! $fill_numFlag; then
    echo "Error: please specify the fill number with -f option (e.g. -f 12345)" >&2
    echo "Use $(basename "$0") -h for help" >&2
    exit
fi

if ! $det_nameFlag; then
    echo "Error: please specify the detector name with -d option (e.g. -d Si01)" >&2
    echo "Use $(basename "$0") -h for help" >&2
    exit
fi

if [ -e "stop_aq" ]; then
    rm "stop_aq"
fi


echo "Writing Acquisitions ($acquisitions) and Skip ($skip)"
pci -w 9024 $acquisitions
sleep 0.1
pci -w 902C $skip
sleep 0.1

echo "ADD Reset DDR"
pci -w 0x9040 0x10210F50
sleep 0.001

pci -w 0x9040 0x10210F00
sleep 0.001

echo "Start Normal acquisition + ENABLE Readout to DDR + HEADER"
pci -w 0x9040 0x1021FF00
sleep 0.001


for (( i=0; i<=$acquisitions; i++ ))  # change here by Michele #
do
    tmpfname=$output_file$i$extension
    echo "Acquiring dataset $i of $acquisitions in $tmpfname..."

    ## Wait until a new acquisition is acquired...
    ACQ_NUM="$(pci -r 9034 | sed 's/.*[0-9]:  //';)"
    ACQ_NUM="$((16#$ACQ_NUM))"
    ACQ_NUM_TEMP="$(pci -r 9034 | sed 's/.*[0-9]:  //';)"
    ACQ_NUM_TEMP="$((16#$ACQ_NUM_TEMP))"

    while [ "$ACQ_NUM_TEMP" -eq "$ACQ_NUM" ]
    do
        ACQ_NUM_TEMP="$(pci -r 9034 | sed 's/.*[0-9]:  //';)"
        ACQ_NUM_TEMP="$((16#$ACQ_NUM_TEMP))"
        sleep 0.01
        now=$(date +"%T")
        echo -ne "$now : Waiting for new trigger..."\\r
    done
    # dump the status after the acquisition # 
    pci -r 9000 -s 100 
    pci -r dma0 --multipacket -o $tmpfname --timeout $(($(($skip+1))*15000000))

    #echo "Acquired at: $now"
    creationtime="$(date +"%Y-%m-%dT%Hh%Mm%Ss" -r $tmpfname)"

    sleep 0.5 #adjust the sleep time 

    echo "ADD Reset DDR"
    pci -w 0x9040 0x10210F50
    sleep 0.001
    #
    echo "IN ACQUISITION"
    pci -w 0x9040 0x1021FF00  # change here by Michele #
    sleep 0.001

    mv $tmpfname $fill_prefix${fill_num}_${creationtime}_${det_name}_$bum$extension
    echo "Acquired at $now: $fill_prefix${fill_num}_${creationtime}_${det_name}_$bum$extension"
    python write_csv.py -f $fill_prefix${fill_num}_${creationtime}_${det_name}_$bum$extension

 #   sleep 0.1

#    MODUL="$(($i%12))"

    #if ((MODUL == 0)); then
    #    echo "Moving to Background"
    #    pci -w 9004 $(printf %x $bg_delay)
    #    bum="bg"
    #    pci -r 9004
    #else
    #    echo "Moving to Signal"
    #    pci -w 9004 $(printf %x $sig_delay)
    #    bum="m"
    #    pci -r 9004
    #fi

    if [ -e "stop_aq" ]; then
        echo "Found 'stop_aq' file, data taking will now stop."
        break
    fi

done
#
#
#
#
# for (( i=1; i<=$acquisitions; i++ ))
# do
#     echo "Acquiring dataset $i of $acquisitions..."
#     pci -r dma0 --multipacket -o $i$extension --timeout $(($(($skip+1))*10000000))
#     now=$(date +"%T")
#     echo "Acquired at: $now"
#     creationtime="$(date +"%Y-%m-%dT%Hh%Mm%Ss" -r $i$extension)"
#     # creationtimeunix="$(date +"%s" -r $i$extension)"
#     mv $i$extension $fill_prefix${fill_num}_${creationtime}_${det_name}$extension
#     echo "Saved in $fill_prefix${fill_num}_${creationtime}_${det_name}$extension"
#     sleep 0.1
# done

echo "Acquisition done!"
pci -w 9024 0
sleep 0.1
pci -w 902C 0
sleep 0.1
echo "IN IDLE"
pci -w 0x9040 0x10210F00

