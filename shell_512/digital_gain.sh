echo "ADC1: REG ADD 3"
pci -w 0x90A0 0x00039120                 #9 - with digital gain en , 8 -without digital gain en
sleep 0.1

#####################################################################################################################################

echo "ADC1: REG ADD D"
pci -w 0x90A0 0x000D0000                #Offset value
sleep 0.05

echo "ADC1: REG ADD E"
pci -w 0x90A0 0x000E0000               #Offset value
sleep 0.05

echo "ADC1: REG ADD F"
pci -w 0x90A0 0x000F003f                #Offset value
sleep 0.05

echo "ADC1: REG ADD 10"
pci -w 0x90A0 0x0010003f                #Offset value
sleep 0.05

echo "ADC1: REG ADD 11"
pci -w 0x90A0 0x00110000                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 12"
pci -w 0x90A0 0x00120000                #Offset value
sleep 0.05

echo "ADC1: REG ADD 13"
pci -w 0x90A0 0x00130000                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 14"
pci -w 0x90A0 0x00140000                #Offset value
sleep 0.05

echo "ADC1: REG ADD 19"
pci -w 0x90A0 0x00190000              #Offset value
sleep 0.05

echo "ADC1: REG ADD 1A"
pci -w 0x90A0 0x001A0000                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 1B"
pci -w 0x90A0 0x001B0000                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 1C"
pci -w 0x90A0 0x001C0000                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 1D"
pci -w 0x90A0 0x001D0000                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 1E"
pci -w 0x90A0 0x001E0000                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 1F"
pci -w 0x90A0 0x001F0000                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 20"
pci -w 0x90A0 0x00200000                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 25"
pci -w 0x90A0 0x0025003f                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 26"
pci -w 0x90A0 0x0026003f                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 27"
pci -w 0x90A0 0x0027003f                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 28"
pci -w 0x90A0 0x0028003f                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 29"
pci -w 0x90A0 0x00290000

echo "ADC1: REG ADD 2A"
pci -w 0x90A0 0x002A0000                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 2B"
pci -w 0x90A0 0x002B0000                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 2C"
pci -w 0x90A0 0x002C0000                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 31"
pci -w 0x90A0 0x00310000                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 32"
pci -w 0x90A0 0x00320000                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 33"
pci -w 0x90A0 0x00330000                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 34"
pci -w 0x90A0 0x00340000                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 35"
pci -w 0x90A0 0x0035003f                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 36"
pci -w 0x90A0 0x0036003f                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 37"
pci -w 0x90A0 0x00370000               #Offset value
sleep 0.05

echo "ADC1: REG ADD 38"
pci -w 0x90A0 0x00380000                 #Offset value
sleep 0.05
##########################################################################################################

#####################################################################################################################################
pci -w 0x90A4 0x00039120                 #01 is 14bit mode, 000 is 12x, 001 is 14x, 100 is 16x, 011 is 10x
sleep 0.05

echo "ADC2: REG ADD D"
pci -w 0x90A4 0x000D1000                #Offset value
sleep 0.05

echo "ADC2: REG ADD E"
pci -w 0x90A4 0x000E1000                #Offset value
sleep 0.05

echo "ADC2: REG ADD F"
pci -w 0x90A4 0x000F1000               #Offset value
sleep 0.05

echo "ADC2: REG ADD 10"
pci -w 0x90A4 0x00101000                #Offset value
sleep 0.05

echo "ADC2: REG ADD 11"
pci -w 0x90A4 0x00111000                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 12"
pci -w 0x90A4 0x00121000                #Offset value
sleep 0.05

echo "ADC2: REG ADD 13"
pci -w 0x90A4 0x00131000                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 14"
pci -w 0x90A4 0x00141000                #Offset value
sleep 0.05

echo "ADC2: REG ADD 19"
pci -w 0x90A4 0x0019103f              #Offset value
sleep 0.05

echo "ADC2: REG ADD 1A"
pci -w 0x90A4 0x001A103f                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 1B"
pci -w 0x90A4 0x001B1000                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 1C"
pci -w 0x90A4 0x001C1000                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 1D"
pci -w 0x90A4 0x001D1000                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 1E"
pci -w 0x90A4 0x001E1000                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 1F"
pci -w 0x90A4 0x001F1000                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 20"
pci -w 0x90A4 0x00201000                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 25"
pci -w 0x90A4 0x00251000                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 26"
pci -w 0x90A4 0x00261000                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 27"
pci -w 0x90A4 0x0027103f                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 28"
pci -w 0x90A4 0x0028103f                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 29"
pci -w 0x90A4 0x00291000                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 2A"
pci -w 0x90A4 0x002A1000                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 2B"
pci -w 0x90A4 0x002B1000                #Offset value
sleep 0.05

echo "ADC2: REG ADD 2C"
pci -w 0x90A4 0x002C1000                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 31"
pci -w 0x90A4 0x00311000                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 32"
pci -w 0x90A4 0x00321000                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 33"
pci -w 0x90A4 0x00331000                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 34"
pci -w 0x90A4 0x00341000                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 35"
pci -w 0x90A4 0x00351000                #Offset value
sleep 0.05

echo "ADC2: REG ADD 36"
pci -w 0x90A4 0x00361000                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 37"
pci -w 0x90A4 0x00371000                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 38"
pci -w 0x90A4 0x00381000                 #Offset value
sleep 0.05
