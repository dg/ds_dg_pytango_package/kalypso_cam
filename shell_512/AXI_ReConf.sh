#! /bin/bash

echo "*** Re-configure the whole AXI MASTER ***"

pci -w 0x9114 0x08
sleep 0.01
pci -w 0x9118 0x01
sleep 0.01
value=`pci -r 0x911C -s1`
echo "Value = $value "
sleep 0.1

pci -w 0x9114 0x0C
sleep 0.01
pci -w 0x9118 0x00
sleep 0.01
value=`pci -r 0x911C -s1`
echo "Value = $value "
sleep 0.1

pci -w 0x9114 0x10
sleep 0.01
pci -w 0x9118 0x01
sleep 0.01
value=`pci -r 0x911C -s1`
echo "Value = $value "
sleep 0.1

pci -w 0x9114 0x18
sleep 0.01
pci -w 0x9118 0x00
sleep 0.01
value=`pci -r 0x911C -s1`
echo "Value = $value "
sleep 0.1

pci -w 0x9114 0x20
sleep 0.01
pci -w 0x9118 0x07
sleep 0.01
value=`pci -r 0x911C -s1`
echo "Value = $value "
sleep 0.1

pci -w 0x9114 0x24
sleep 0.01
pci -w 0x9118 0x0F
sleep 0.01
value=`pci -r 0x911C -s1`
echo "Frame per mulfi-frame = $value " 
sleep 0.1


pci -w 0x9114 0x2C
sleep 0.01
pci -w 0x9118 0x2
sleep 0.01
value=`pci -r 0x911C -s1`
echo "Sub Class Value = $value "
sleep 0.1

pci -w 0x9114 0x30
sleep 0.01
pci -w 0x9118 0x01 # dummy writing operation # 
pci -w 0x9118 0x00
sleep 0.01
value=`pci -r 0x911C -s1`
echo "Value = $value "
sleep 0.1

pci -w 0x9114 0x34
sleep 0.01
pci -w 0x9118 0x01 # dummy writing operation # y
pci -w 0x9118 0x00
sleep 0.01
value=`pci -r 0x911C -s1`
echo "Value = $value "
sleep 0.1

pci -w 0x9114 0x28
sleep 0.01
pci -w 0x9118 0xF
sleep 0.01
value=`pci -r 0x911C -s1`
echo "Value = $value "
sleep 0.1

pci -w 0x9114 0x04
sleep 0.01
pci -w 0x9118 0x01
sleep 0.01
value=`pci -r 0x911C -s1`
echo "Value = $value "
sleep 0.1



echo "DONE ..."