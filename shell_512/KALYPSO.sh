#!/bin/bash
export PCILIB_MODEL=ipedma

echo " ################################################################## "
echo " Welcome to Kalypso "
echo " Reset and Initialization is started .... "
echo " ################################################################## "


./full_sequence_ex_rf.sh


echo " ################################################################## "
echo " finished .... "
echo " GUI for 512 pixels will be started soon "
echo " ################################################################## "
sleep 0.0001

./status.sh

python ./GUI.py
