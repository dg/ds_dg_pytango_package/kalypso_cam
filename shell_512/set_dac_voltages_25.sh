#!/bin/sh


#echo "ADC & GOT: Turning on power..."
#pci -w 9048 f0
#sleep 0.1

pci -w 9048 ff
sleep 0.1


echo "***Configuring DACs***"

#I2C_DAC_0 = h9068
#I2C_DAC_1 = h906C
#I2C_DAC_2 = h9070
#I2C_DAC_3 = h9074
#I2C_DAC_4 = h9078
#I2C_DAC_5 = h907C


echo "DAC0: CH0 = VB_COLBUFFER 29uA"
pci -w 9068 000820        #original value is 750
sleep 0.1

echo "DAC0: CH2 = VB_COLBUFFER 29uA"
pci -w 9068 020820
sleep 0.1

echo "DAC1: CH0 = VB_COLBUFFER 29uA"
pci -w 906C 000820
sleep 0.1

echo "DAC1: CH2 = VB_COLBUFFER 29uA"
pci -w 906C 020820
sleep 0.1

echo "DAC3: CH0 = VB_COLBUFFER 29uA"
pci -w 9074 000820
sleep 0.1

echo "DAC3: CH2 = VB_COLBUFFER 29uA"
pci -w 9074 020820
sleep 0.1

echo "DAC4: CH0 = VB_COLBUFFER 29uA"
pci -w 9078 000820
sleep 0.1

echo "DAC4: CH2 = VB_COLBUFFER 29uA"
pci -w 9078 020820
sleep 0.1



echo "DAC0: CH1 = IB_DS 28uA"
pci -w 9068 010550
sleep 0.1

echo "DAC0: CH3 = IB_DS 28uA"
pci -w 9068 030550
sleep 0.1

echo "DAC1: CH1 = IB_DS 28uA"
pci -w 906C 010550
sleep 0.1

echo "DAC1: CH3 = IB_DS 28uA"
pci -w 906C 030550
sleep 0.1

echo "DAC3: CH1 = IB_DS 28uA"
pci -w 9074 010550
sleep 0.1

echo "DAC3: CH3 = IB_DS 28uA"
pci -w 9074 030550
sleep 0.1

echo "DAC4: CH1 = IB_DS 28uA"
pci -w 9078 010550
sleep 0.1

echo "DAC4: CH3 = IB_DS 28uA"
pci -w 9078 030550
sleep 0.1


echo "DAC2: CH0 = VOUT_CM "
pci -w 9070 0005DC
sleep 0.1

echo "DAC2: CH2 = VOUT_CM "
pci -w 9070 0205DC
sleep 0.1

echo "DAC5: CH0 = VOUT_CM "
pci -w 907C 0005DC
sleep 0.1

echo "DAC5: CH2 = VOUT_CM "
pci -w 907C 0205DC
sleep 0.1



echo "DAC2: CH1 = VIN_CM 560mV"
pci -w 9070 010460
sleep 0.1

echo "DAC2: CH3 = VIN_CM 560mV "
pci -w 9070 030460
sleep 0.1

echo "DAC5: CH1 = VIN_CM 560mV"
pci -w 907C 010460
sleep 0.1

echo "DAC5: CH3 = VIN_CM 560mV "
pci -w 907C 030460
sleep 0.1

echo "....done!****"
