#!/bin/sh

# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Initialize our own variables:
reg=0x9000

while getopts "r:" opt; do
    case "$opt" in
    r)  reg=$OPTARG
        ;;
    esac
done

pci -r $reg