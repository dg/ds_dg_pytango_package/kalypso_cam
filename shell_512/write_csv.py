"""Usage: write_csv.py -f FILE

Options:
    -h --help Show this screen.
"""
#!/usr/bin/python2
from docopt import docopt

import numpy as np
import csv
import json
import os
import os.path
import subprocess



def write_csv(output_file):

    with open('kalypso_registers.json') as registers_file:
        registers = json.load(registers_file)

    with open('kalypso_log_conf.json') as log_conf_file:
        log_conf = json.load(log_conf_file)

    def read_register(reg_key, hex=False):
        ## Output from 'pci -r' function in terminal is a string with (rubbish)
        ## + (8 chars for values, starting from [11])
        pcitool_string = subprocess.check_output(['pci', '-r', registers[reg_key]["address"]])
        if hex==True:
            return pcitool_string[11:19]
        else:
            value = int(pcitool_string[11:19],16) & int(registers[reg_key]["mask"],16)
            return value

    csvfile = output_file[:-3] + "csv"
    with open(csvfile, "w") as output:
        writer = csv.writer(output, lineterminator='\n')
        entries = []
        for key in log_conf["log_entries"]:
            entries.append([str(registers[key]["name"]),read_register(key,hex=True)])
        writer.writerows(entries)

if __name__ == "__main__":
    arguments = docopt(__doc__, version="lor 0.2")
    output_file = arguments["FILE"]
    write_csv(output_file)
