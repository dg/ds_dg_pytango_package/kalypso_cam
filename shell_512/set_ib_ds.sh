#!/bin/sh


#echo "ADC & GOT: Turning on power..."
#pci -w 9048 f0
#sleep 0.1

echo "***Configuring DACs***"

#I2C_DAC_0 = h9068
#I2C_DAC_1 = h906C
#I2C_DAC_2 = h9070
#I2C_DAC_3 = h9074
#I2C_DAC_4 = h9078
#I2C_DAC_5 = h907C


echo "DAC0: CH1 = IB_DS 28uA"
pci -w 9068 010$1
sleep 0.1

echo "DAC0: CH3 = IB_DS 28uA"
pci -w 9068 030$1
sleep 0.1

echo "DAC1: CH1 = IB_DS 28uA"
pci -w 906C 010$1
sleep 0.1

echo "DAC1: CH3 = IB_DS 28uA"
pci -w 906C 030$1
sleep 0.1

echo "DAC3: CH1 = IB_DS 28uA"
pci -w 9074 010$1
sleep 0.1

echo "DAC3: CH3 = IB_DS 28uA"
pci -w 9074 030$1
sleep 0.1

echo "DAC4: CH1 = IB_DS 28uA"
pci -w 9078 010$1
sleep 0.1

echo "DAC4: CH3 = IB_DS 28uA"
pci -w 9078 030$1
sleep 0.1
