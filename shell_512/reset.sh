#! /bin/bash
export PCILIB_MODEL=ipedma

echo " ###########################################################"
echo "          KALYPSO 512 pix  - INTERNAL CLOCK      "
echo " ###########################################################"

####################################################
echo "RESET of the KALYPSO"
pci -w 9040 0x800ff
sleep 0.1

echo "DE-RESET of the KALYPSO"
pci -w 9040 0x0
sleep 0.1

######################################
pci -w 0x9024 0
sleep 0.1

pci -w 0x9024 1
sleep 0.1
#########################################
echo "*ONBOARD TRIG GEN: ON"
pci -w 0x9044 0x7E
sleep 0.1

echo "GOTT: Integration Period"
pci -w 0x9000 0x2
sleep 0.1

echo "GOTT: Integration Delay"
pci -w 0x9004 0x0
sleep 0.1

echo "GOTT: Gain"
pci -w 0x9010 0x02
sleep 0.1

echo "write the number of samples to be acquired"
pci -w 9108 0x0400
sleep 0.1

################################################
echo "----------> reset of the clock align stage"
pci -w 9088 0
sleep 0.05
#
pci -w 908C 0
sleep 0.05

pci -w 9040 10310f00
sleep 0.05
pci -w 9040 10210f00
sleep 0.05

pci -w 9040 10310f00
sleep 0.05
pci -w 9040 10210f00
sleep 0.05
#
pci -w 9040 10310f00
sleep 0.05
pci -w 9040 10210f00
sleep 0.05
#
pci -w 9040 10310f00
sleep 0.05
pci -w 9040 10210f00
sleep 1 # do not reduce this time, waiting till the PLL is locked #

 ############## For internal RF clock ############
echo "reset and synchronize TOP and BOTTOM"
pci -w 904C 2
sleep 0.1

echo "SETTING FOR 512 PIXELS"
pci -w 904C 8
sleep 0.1
