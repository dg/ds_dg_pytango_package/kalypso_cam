#!/usr/bin/env python

from PyQt4 import QtCore, QtGui
from QLed import QLed
from PyQt4.QtGui import QApplication, QWidget, QPainter, QGridLayout, QSizePolicy, QStyleOption
from PyQt4.QtCore import pyqtSignal, Qt, QSize, QTimer, QByteArray, QRectF, pyqtProperty
from PyQt4.QtSvg import QSvgRenderer


import pyqtgraph as pg
import numpy as np
from pyqtgraph.Qt import QtCore, QtGui
from pyqtgraph import PlotWidget
import time
import datetime
import os
import os.path
import subprocess
import timescan_plot
import strip_data
import csv
import json


##import elog_eosd

### VALUES TO DISPLAY ##################
NUMBER_PIXELS   = 512
OFFSET          = 0x0
#########################################

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(995, 662)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.centralwidget)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))

        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setSizeConstraint(QtGui.QLayout.SetMaximumSize)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))

        # Define TABS
        self.tab_widget = QtGui.QTabWidget()
        self.tab1 = QtGui.QWidget()
        self.tab2 = QtGui.QWidget()

        self.t1_vertical = QtGui.QVBoxLayout(self.tab1)
        self.t2_vertical = QtGui.QVBoxLayout(self.tab2)

        self.tab_widget.addTab(self.tab1, "1D")
        self.tab_widget.addTab(self.tab2, "2D")

        # TAB for live view
        self.graphicsView = PlotWidget(background='k')
        self.graphicsView.setGeometry(QtCore.QRect(10, 60, 800, 500))
        self.graphicsView.setObjectName(_fromUtf8("graphicsView"))
        self.t1_vertical.addWidget(self.graphicsView)


        #
        # pic = QtGui.QLabel(window)
        # pic.setGeometry(10, 10, 400, 100)
        # #use full ABSOLUTE path to the image, not relative
        # pic.setPixmap(QtGui.QPixmap(os.getcwd() + "/logo.png"))

        # TAB for modulation view
        self.imView = pg.GraphicsLayoutWidget()
        self.modplot = self.imView.addPlot()
        self.img = pg.ImageItem()
        self.modplot.addItem(self.img)

        self.imView.nextColumn()
        self.hist = pg.HistogramLUTItem()
        self.hist.setImageItem(self.img)
        self.hist.setHistogramRange(0,2.0)
        self.imView.addItem(self.hist)

        #self.img.setImage(data)
        self.hist.setLevels(0, 2)
        self.modplot.autoRange()

        self.imView.setGeometry(QtCore.QRect(10, 60, 800, 500))
        self.imView.setObjectName(_fromUtf8("imView"))
        self.t2_vertical.addWidget(self.imView)
        self.verticalLayout.addWidget(self.tab_widget)

        ## Lower part
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))

        self.label_disp_settings = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_disp_settings.sizePolicy().hasHeightForWidth())
        self.label_disp_settings.setSizePolicy(sizePolicy)
        self.label_disp_settings.setObjectName(_fromUtf8("label_disp_settings"))
        self.horizontalLayout.addWidget(self.label_disp_settings)


        self.label_pix_min = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_pix_min.sizePolicy().hasHeightForWidth())
        self.label_pix_min.setSizePolicy(sizePolicy)
        self.label_pix_min.setObjectName(_fromUtf8("label_pix_min"))
        self.horizontalLayout.addWidget(self.label_pix_min)

        self.spinbox_pix_min = QtGui.QSpinBox(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.spinbox_pix_min.sizePolicy().hasHeightForWidth())
        self.spinbox_pix_min.setSizePolicy(sizePolicy)
        self.spinbox_pix_min.setMaximumSize(QtCore.QSize(300, 16777215))
        self.spinbox_pix_min.setObjectName(_fromUtf8("pix_min"))
        self.spinbox_pix_min.setMinimum(0)
        self.spinbox_pix_min.setMaximum(510)
        self.horizontalLayout.addWidget(self.spinbox_pix_min)

        self.spinbox_pix_max = QtGui.QSpinBox(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.spinbox_pix_max.sizePolicy().hasHeightForWidth())
        self.spinbox_pix_max.setSizePolicy(sizePolicy)
        self.spinbox_pix_max.setMaximumSize(QtCore.QSize(300, 16777215))
        self.spinbox_pix_max.setObjectName(_fromUtf8("pix_max"))
        self.spinbox_pix_max.setMinimum(1)
        self.spinbox_pix_max.setMaximum(511)
        self.horizontalLayout.addWidget(self.spinbox_pix_max)

        #self.checkBox_remove_back = QtGui.QCheckBox(self.centralwidget)
        #sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        #sizePolicy.setHorizontalStretch(0)
        #sizePolicy.setVerticalStretch(0)
        #sizePolicy.setHeightForWidth(self.checkBox_remove_back.sizePolicy().hasHeightForWidth())
        #self.checkBox_remove_back.setSizePolicy(sizePolicy)
        #self.checkBox_remove_back.setObjectName(_fromUtf8("checkBox_remove_back"))
        #self.horizontalLayout.addWidget(self.checkBox_remove_back)

        #self.checkBox_average = QtGui.QCheckBox(self.centralwidget)
        #sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        #sizePolicy.setHorizontalStretch(0)
        #sizePolicy.setVerticalStretch(0)
        #sizePolicy.setHeightForWidth(self.checkBox_remove_back.sizePolicy().hasHeightForWidth())
        #self.checkBox_average.setSizePolicy(sizePolicy)
        #self.checkBox_average.setObjectName(_fromUtf8("checkBox_average"))
        #self.horizontalLayout.addWidget(self.checkBox_average)

        #self.checkBox_calc_modul = QtGui.QCheckBox(self.centralwidget)
        #sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        #sizePolicy.setHorizontalStretch(0)
        #sizePolicy.setVerticalStretch(0)
        #sizePolicy.setHeightForWidth(self.checkBox_calc_modul.sizePolicy().hasHeightForWidth())
        #self.checkBox_calc_modul.setSizePolicy(sizePolicy)
        #self.checkBox_calc_modul.setObjectName(_fromUtf8("checkBox_calc_modul"))
        #self.horizontalLayout.addWidget(self.checkBox_calc_modul)

        #########################################

        self.verticalLayout.addLayout(self.horizontalLayout)
        self.line = QtGui.QFrame(self.centralwidget)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.verticalLayout.addWidget(self.line)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))

        ## KALYPSO Operation part
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))

        self.label_controls = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_controls.sizePolicy().hasHeightForWidth())
        self.label_controls.setSizePolicy(sizePolicy)
        self.label_controls.setObjectName(_fromUtf8("label_controls"))
        self.verticalLayout_2.addWidget(self.label_controls)

        self.pushButton_init_board = QtGui.QPushButton(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_init_board.sizePolicy().hasHeightForWidth())
        self.pushButton_init_board.setSizePolicy(sizePolicy)
        self.pushButton_init_board.setMaximumSize(QtCore.QSize(800, 16777215))
        self.pushButton_init_board.setObjectName(_fromUtf8("pushButton_init_board"))
        self.verticalLayout_2.addWidget(self.pushButton_init_board)

############################## with external RF ##########################################################
        self.pushButton_init_board_ex_rf = QtGui.QPushButton(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_init_board_ex_rf.sizePolicy().hasHeightForWidth())
        self.pushButton_init_board_ex_rf.setSizePolicy(sizePolicy)
        self.pushButton_init_board_ex_rf.setMaximumSize(QtCore.QSize(800, 16777215))
        self.pushButton_init_board_ex_rf.setObjectName(_fromUtf8("pushButton_init_board_ex_rf"))
        self.verticalLayout_2.addWidget(self.pushButton_init_board_ex_rf)

######################################################################################################

        self.pushButton_timescan = QtGui.QPushButton(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_timescan.sizePolicy().hasHeightForWidth())
        self.pushButton_timescan.setSizePolicy(sizePolicy)
        self.pushButton_timescan.setMaximumSize(QtCore.QSize(800, 16777215))
        self.pushButton_timescan.setObjectName(_fromUtf8("pushButton_timescan"))
        self.verticalLayout_2.addWidget(self.pushButton_timescan)

        self.pushButton_poweroff = QtGui.QPushButton(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        self.pushButton_poweroff.setSizePolicy(sizePolicy)
        self.pushButton_poweroff.setMaximumSize(QtCore.QSize(800, 16777215))
        self.pushButton_poweroff.setObjectName(_fromUtf8("pushButton_poweroff"))
        self.verticalLayout_2.addWidget(self.pushButton_poweroff)

        ### Horizontal Line
        self.line_0 = QtGui.QFrame(self.centralwidget)
        self.line_0.setFrameShape(QtGui.QFrame.HLine)
        self.line_0.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_0.setObjectName(_fromUtf8("line_0"))
        self.verticalLayout_2.addWidget(self.line_0)

        ### Load from file
        self.label_loadfile = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_loadfile.sizePolicy().hasHeightForWidth())
        self.label_loadfile.setSizePolicy(sizePolicy)
        self.label_loadfile.setObjectName(_fromUtf8("label_loadfile"))
        self.verticalLayout_2.addWidget(self.label_loadfile)

        self.pushButton_background_file = QtGui.QPushButton(self.centralwidget)
        self.pushButton_background_file.setSizePolicy(sizePolicy)
        self.pushButton_background_file.setMaximumSize(QtCore.QSize(400, 16777215))
        self.pushButton_background_file.setObjectName(_fromUtf8("pushButton_background_file"))
        self.verticalLayout_2.addWidget(self.pushButton_background_file)

        self.pushButton_unmodulated_file = QtGui.QPushButton(self.centralwidget)
        self.pushButton_unmodulated_file.setSizePolicy(sizePolicy)
        self.pushButton_unmodulated_file.setMaximumSize(QtCore.QSize(400, 16777215))
        self.pushButton_unmodulated_file.setObjectName(_fromUtf8("pushButton_unmodulated_file"))
        self.verticalLayout_2.addWidget(self.pushButton_unmodulated_file)


        self.pushButton_modulated_file = QtGui.QPushButton(self.centralwidget)
        self.pushButton_modulated_file.setSizePolicy(sizePolicy)
        self.pushButton_modulated_file.setMaximumSize(QtCore.QSize(400, 16777215))
        self.pushButton_modulated_file.setObjectName(_fromUtf8("pushButton_modulated_file"))
        self.verticalLayout_2.addWidget(self.pushButton_modulated_file)

        self.horizontalLayout_2.addLayout(self.verticalLayout_2)
        self.line_6 = QtGui.QFrame(self.centralwidget)
        self.line_6.setFrameShape(QtGui.QFrame.VLine)
        self.line_6.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_6.setObjectName(_fromUtf8("line_5"))
        self.horizontalLayout_2.addWidget(self.line_6)


        #### SETTINGS part
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setSizeConstraint(QtGui.QLayout.SetMinimumSize)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))

        ## Integration time
        self.label_settings = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_settings.sizePolicy().hasHeightForWidth())
        self.label_settings.setSizePolicy(sizePolicy)
        self.label_settings.setObjectName(_fromUtf8("label_settings"))
        self.gridLayout.addWidget(self.label_settings, 0, 0, 1, 1)

        self.pushButton_readsettings = QtGui.QPushButton(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_readsettings.sizePolicy().hasHeightForWidth())
        self.pushButton_readsettings.setSizePolicy(sizePolicy)
        self.pushButton_readsettings.setMaximumSize(QtCore.QSize(800, 16777215))
        self.pushButton_readsettings.setObjectName(_fromUtf8("pushButton_readsettings"))
        self.gridLayout.addWidget(self.pushButton_readsettings, 1, 0, 1, 1)

        self.label_int_time = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_int_time.sizePolicy().hasHeightForWidth())
        self.label_int_time.setSizePolicy(sizePolicy)
        self.label_int_time.setObjectName(_fromUtf8("label_int_time"))
        self.gridLayout.addWidget(self.label_int_time, 2, 0, 1, 1)

        self.spinBox_int_time = QtGui.QSpinBox(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.spinBox_int_time.sizePolicy().hasHeightForWidth())
        self.spinBox_int_time.setSizePolicy(sizePolicy)
        self.spinBox_int_time.setMaximumSize(QtCore.QSize(300, 16777215))
        self.spinBox_int_time.setObjectName(_fromUtf8("spinBox_int_time"))
        self.spinBox_int_time.setRange(8,1024)
        self.spinBox_int_time.setSingleStep(8)
        self.gridLayout.addWidget(self.spinBox_int_time, 2, 1, 1, 1)

        ## Int. Delay
        self.label_int_delay = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_int_delay.sizePolicy().hasHeightForWidth())
        self.label_int_delay.setSizePolicy(sizePolicy)
        self.label_int_delay.setObjectName(_fromUtf8("label_int_delay"))
        self.gridLayout.addWidget(self.label_int_delay, 3, 0, 1, 1)
        self.spinBox_int_delay = QtGui.QSpinBox(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.spinBox_int_delay.sizePolicy().hasHeightForWidth())
        self.spinBox_int_delay.setSizePolicy(sizePolicy)
        self.spinBox_int_delay.setMaximumSize(QtCore.QSize(300, 16777215))
        self.spinBox_int_delay.setObjectName(_fromUtf8("spinBox_int_delay"))
        self.spinBox_int_delay.setRange(1,1023)
        self.spinBox_int_delay.setSingleStep(1)
        self.gridLayout.addWidget(self.spinBox_int_delay, 3, 1, 1, 1)

        ## GOTT gain
        self.spinBox_gott_gain = QtGui.QSpinBox(self.centralwidget)
        self.spinBox_gott_gain.setMaximum(2)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.spinBox_gott_gain.sizePolicy().hasHeightForWidth())
        self.spinBox_gott_gain.setSizePolicy(sizePolicy)
        self.spinBox_gott_gain.setMaximumSize(QtCore.QSize(300, 16777215))
        self.spinBox_gott_gain.setObjectName(_fromUtf8("spinBox_gott_gain"))
        self.spinBox_gott_gain.setMaximum(2)
        self.gridLayout.addWidget(self.spinBox_gott_gain, 4, 1, 1, 1)
        self.label_gott_gain = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.spinBox_gott_gain.sizePolicy().hasHeightForWidth())
        self.label_gott_gain.setSizePolicy(sizePolicy)
        self.label_gott_gain.setObjectName(_fromUtf8("label_gott_gain"))
        self.gridLayout.addWidget(self.label_gott_gain, 4, 0, 1, 1)

        ## Total samples
        self.label_samples = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_samples.sizePolicy().hasHeightForWidth())
        self.label_samples.setSizePolicy(sizePolicy)
        self.label_samples.setObjectName(_fromUtf8("label_samples"))
        self.gridLayout.addWidget(self.label_samples, 5, 0, 1, 1)
        self.spinBox_total_samples = QtGui.QSpinBox(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.spinBox_total_samples.sizePolicy().hasHeightForWidth())
        self.spinBox_total_samples.setSizePolicy(sizePolicy)
        self.spinBox_total_samples.setMaximumSize(QtCore.QSize(300, 16777215))
        self.spinBox_total_samples.setObjectName(_fromUtf8("spinBox_total_samples"))
        self.spinBox_total_samples.setMinimum(8)
        self.spinBox_total_samples.setMaximum(25000000)
        self.spinBox_total_samples.setSingleStep(8)
        self.gridLayout.addWidget(self.spinBox_total_samples, 5, 1, 1, 1)

        ## Skip FT
        self.spinBox_skip_samples = QtGui.QSpinBox(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.spinBox_skip_samples.sizePolicy().hasHeightForWidth())
        self.spinBox_skip_samples.setSizePolicy(sizePolicy)
        self.spinBox_skip_samples.setMaximumSize(QtCore.QSize(300, 16777215))
        self.spinBox_skip_samples.setObjectName(_fromUtf8("spinBox_skip_samples"))
        self.spinBox_skip_samples.setMaximum(10000000)
        self.gridLayout.addWidget(self.spinBox_skip_samples, 6, 1, 1, 1)
        self.label_skip_samples = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_skip_samples.sizePolicy().hasHeightForWidth())
        self.label_skip_samples.setSizePolicy(sizePolicy)
        self.label_skip_samples.setObjectName(_fromUtf8("label_skip_samples"))
        self.gridLayout.addWidget(self.label_skip_samples, 6, 0, 1, 1)

        ## Internal Trigger label
        #self.label_int_trig_text = QtGui.QLabel(self.centralwidget)
        #sizePolicy.setHeightForWidth(self.label_int_trig_text.sizePolicy().hasHeightForWidth())
        #self.label_int_trig_text.setSizePolicy(sizePolicy)
        #self.label_int_trig_text.setObjectName(_fromUtf8("label_int_trig_text"))
        #self.gridLayout.addWidget(self.label_int_trig_text, 6, 0, 1, 1)

        ## Internal Trigger
        self.spinBox_int_trig= QtGui.QSpinBox(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.spinBox_int_trig.sizePolicy().hasHeightForWidth())
        self.spinBox_int_trig.setSizePolicy(sizePolicy)
        self.spinBox_int_trig.setMaximumSize(QtCore.QSize(300, 16777215))
        self.spinBox_int_trig.setObjectName(_fromUtf8("spinBox_int_trig"))
        self.spinBox_int_trig.setMaximum(4096)
        self.gridLayout.addWidget(self.spinBox_int_trig, 8, 1, 1, 1)
        self.label_int_trig = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_int_trig.sizePolicy().hasHeightForWidth())
        self.label_int_trig.setSizePolicy(sizePolicy)
        self.label_int_trig.setObjectName(_fromUtf8("label_int_trig"))
        self.gridLayout.addWidget(self.label_int_trig, 8, 0, 1, 1)

#########################------ADC SETTINGS-----------------################################################
        self.line_0 = QtGui.QFrame(self.centralwidget)
        self.line_0.setFrameShape(QtGui.QFrame.HLine)
        self.line_0.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_0.setObjectName(_fromUtf8("line_0"))
        self.gridLayout.addWidget(self.line_0, 9, 0, 1, 1)
        self.line_1 = QtGui.QFrame(self.centralwidget)
        self.line_1.setFrameShape(QtGui.QFrame.HLine)
        self.line_1.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_1.setObjectName(_fromUtf8("line_1"))
        self.gridLayout.addWidget(self.line_1, 9, 1, 1, 1)

        self.adc_label_settings = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.adc_label_settings.sizePolicy().hasHeightForWidth())
        self.adc_label_settings.setSizePolicy(sizePolicy)
        self.adc_label_settings.setObjectName(_fromUtf8("label_settings"))
        self.gridLayout.addWidget(self.adc_label_settings, 10, 0, 1, 1)

        self.spinBox_jesd_delay= QtGui.QSpinBox(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.spinBox_jesd_delay.sizePolicy().hasHeightForWidth())
        self.spinBox_jesd_delay.setSizePolicy(sizePolicy)
        self.spinBox_jesd_delay.setMaximumSize(QtCore.QSize(300, 16777215))
        self.spinBox_jesd_delay.setObjectName(_fromUtf8("spinBox_jesd_delay"))
        self.spinBox_jesd_delay.setMaximum(61)
        self.gridLayout.addWidget(self.spinBox_jesd_delay, 11, 1, 1, 1)
        self.label_jesd_delay = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_jesd_delay.sizePolicy().hasHeightForWidth())
        self.label_jesd_delay.setSizePolicy(sizePolicy)
        self.label_jesd_delay.setObjectName(_fromUtf8("label_jesd_delay"))
        self.gridLayout.addWidget(self.label_jesd_delay, 11, 0, 1, 1)


##############################----------tx_trig-------##########################################################
        # self.pushButton_tx_trig = QtGui.QPushButton(self.centralwidget)
        # sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        # sizePolicy.setHorizontalStretch(0)
        # sizePolicy.setVerticalStretch(0)
        # sizePolicy.setHeightForWidth(self.pushButton_tx_trig.sizePolicy().hasHeightForWidth())
        # self.pushButton_tx_trig.setSizePolicy(sizePolicy)
        # self.pushButton_tx_trig.setMaximumSize(QtCore.QSize(800, 16777215))
        # self.pushButton_tx_trig.setObjectName(_fromUtf8("pushButton_tx_trig"))
        # self.gridLayout.addWidget(self.pushButton_tx_trig, 11, 0, 1, 1)

######################################################################################################
##############################-------phase shifter------##########################################################
        self.pushButton_phase_swap = QtGui.QPushButton(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_phase_swap.sizePolicy().hasHeightForWidth())
        self.pushButton_phase_swap.setSizePolicy(sizePolicy)
        self.pushButton_phase_swap.setMaximumSize(QtCore.QSize(800, 16777215))
        self.pushButton_phase_swap.setObjectName(_fromUtf8("pushButton_phase_swap"))
        self.gridLayout.addWidget(self.pushButton_phase_swap, 12, 0, 1, 1)

######################################################################################################



        ## End of layout
        self.horizontalLayout_2.addLayout(self.gridLayout)
        self.line_5 = QtGui.QFrame(self.centralwidget)
        self.line_5.setFrameShape(QtGui.QFrame.VLine)
        self.line_5.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_5.setObjectName(_fromUtf8("line_5"))
        self.horizontalLayout_2.addWidget(self.line_5)

        ########################################################################
        ### Acquisition part
        self.gridLayout_acq = QtGui.QGridLayout()
        self.gridLayout_acq.setSizeConstraint(QtGui.QLayout.SetMinimumSize)
        self.gridLayout_acq.setObjectName(_fromUtf8("gridLayout_acq"))

        self.label_acq = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_pix_min.sizePolicy().hasHeightForWidth())
        self.label_acq.setSizePolicy(sizePolicy)
        self.label_acq.setObjectName(_fromUtf8("label_acq"))
        self.gridLayout_acq.addWidget(self.label_acq, 1, 0, 1, 1)

        self.pushButton_background = QtGui.QPushButton(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_background.sizePolicy().hasHeightForWidth())
        self.pushButton_background.setSizePolicy(sizePolicy)
        self.pushButton_background.setMaximumSize(QtCore.QSize(800, 16777215))
        self.pushButton_background.setObjectName(_fromUtf8("pushButton_background"))
        self.gridLayout_acq.addWidget(self.pushButton_background, 2, 0, 1, 1)

        self.checkBox_remove_back = QtGui.QCheckBox(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.checkBox_remove_back.sizePolicy().hasHeightForWidth())
        self.checkBox_remove_back.setSizePolicy(sizePolicy)
        self.checkBox_remove_back.setObjectName(_fromUtf8("checkBox_remove_back"))
        self.gridLayout_acq.addWidget(self.checkBox_remove_back, 2, 1, 1, 1)


        self.pushButton_unmodulated = QtGui.QPushButton(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_background.sizePolicy().hasHeightForWidth())
        self.pushButton_unmodulated.setSizePolicy(sizePolicy)
        self.pushButton_unmodulated.setMaximumSize(QtCore.QSize(800, 16777215))
        self.pushButton_unmodulated.setObjectName(_fromUtf8("pushButton_unmodulated"))
        self.gridLayout_acq.addWidget(self.pushButton_unmodulated, 3, 0, 1, 1)

        self.checkBox_average = QtGui.QCheckBox(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.checkBox_average.sizePolicy().hasHeightForWidth())
        self.checkBox_average.setSizePolicy(sizePolicy)
        self.checkBox_average.setObjectName(_fromUtf8("checkBox_average"))
        self.gridLayout_acq.addWidget(self.checkBox_average, 3, 1, 1, 1)


        self.pushButton_modulated = QtGui.QPushButton(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_modulated.sizePolicy().hasHeightForWidth())
        self.pushButton_modulated.setSizePolicy(sizePolicy)
        self.pushButton_modulated.setMaximumSize(QtCore.QSize(800, 16777215))
        self.pushButton_modulated.setObjectName(_fromUtf8("pushButton_modulated"))
        self.gridLayout_acq.addWidget(self.pushButton_modulated, 4, 0, 1, 1)

        self.checkBox_calc_modul = QtGui.QCheckBox(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.checkBox_calc_modul.sizePolicy().hasHeightForWidth())
        self.checkBox_calc_modul.setSizePolicy(sizePolicy)
        self.checkBox_calc_modul.setObjectName(_fromUtf8("checkBox_calc_modul"))
        self.gridLayout_acq.addWidget(self.checkBox_calc_modul, 4, 1, 1, 1)


        self.checkBox_savefile = QtGui.QCheckBox(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.checkBox_savefile.sizePolicy().hasHeightForWidth())
        self.checkBox_savefile.setSizePolicy(sizePolicy)
        self.checkBox_savefile.setObjectName(_fromUtf8("checkBox_savefile"))
        self.gridLayout_acq.addWidget(self.checkBox_savefile, 5, 0, 1, 1)

        # self.checkBox_ELOG = QtGui.QCheckBox(self.centralwidget)
        # sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        # sizePolicy.setHorizontalStretch(0)
        # sizePolicy.setVerticalStretch(0)
        # sizePolicy.setHeightForWidth(self.checkBox_ELOG.sizePolicy().hasHeightForWidth())
        # self.checkBox_ELOG.setSizePolicy(sizePolicy)
        # self.checkBox_ELOG.setObjectName(_fromUtf8("checkBox_ELOG"))
        # self.gridLayout_acq.addWidget(self.checkBox_ELOG, 5, 1, 1, 1)

        self.pushButton_file_path = QtGui.QPushButton(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_file_path.sizePolicy().hasHeightForWidth())
        self.pushButton_file_path.setSizePolicy(sizePolicy)
        self.pushButton_file_path.setMaximumSize(QtCore.QSize(800, 16777215))
        self.pushButton_file_path.setObjectName(_fromUtf8("pushButton_file_path"))
        self.gridLayout_acq.addWidget(self.pushButton_file_path, 6, 0, 1, 1)

        self.lineEdit_file_path = QtGui.QLineEdit(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineEdit_file_path.sizePolicy().hasHeightForWidth())
        self.lineEdit_file_path.setSizePolicy(sizePolicy)
        self.lineEdit_file_path.setMaximumSize(QtCore.QSize(800, 16777215))
        self.lineEdit_file_path.setObjectName(_fromUtf8("lineEdit_file_path"))
        self.gridLayout_acq.addWidget(self.lineEdit_file_path, 6, 1, 1, 1)


        self.fillnum_bar = QtGui.QStatusBar(self.centralwidget)
        self.fillnum_bar.showMessage("Fill number:")
        self.fillnum_bar.setSizeGripEnabled(0)
        self.fillnum_bar.setMaximumSize(QtCore.QSize(900, 100))
        self.fillnum_bar.setObjectName(_fromUtf8("detector_bar"))
        self.gridLayout_acq.addWidget(self.fillnum_bar, 7, 0, 1, 1)

        self.lineEdit_fillnum = QtGui.QLineEdit(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineEdit_fillnum.sizePolicy().hasHeightForWidth())
        self.lineEdit_fillnum.setSizePolicy(sizePolicy)
        self.lineEdit_fillnum.setMaximumSize(QtCore.QSize(800, 16777215))
        self.lineEdit_fillnum.setObjectName(_fromUtf8("lineEdit_fillnum"))
        self.lineEdit_fillnum.setInputMask("99999")
        self.lineEdit_fillnum.setText("12345")

        self.gridLayout_acq.addWidget(self.lineEdit_fillnum, 7, 1, 1, 1)


        self.detector_bar = QtGui.QStatusBar(self.centralwidget)
        self.detector_bar.showMessage("Detector:")
        self.detector_bar.setSizeGripEnabled(0)
        self.detector_bar.setMaximumSize(QtCore.QSize(900, 100))
        self.detector_bar.setObjectName(_fromUtf8("detector_bar"))
        self.gridLayout_acq.addWidget(self.detector_bar, 8, 0, 1, 1)

        self.lineEdit_detector = QtGui.QLineEdit(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineEdit_detector.sizePolicy().hasHeightForWidth())
        self.lineEdit_detector.setSizePolicy(sizePolicy)
        self.lineEdit_detector.setMaximumSize(QtCore.QSize(800, 16777215))
        self.lineEdit_detector.setObjectName(_fromUtf8("lineEdit_detector"))
        self.lineEdit_detector.setInputMask("NNNN")
        self.lineEdit_detector.setText("Si01")
        self.gridLayout_acq.addWidget(self.lineEdit_detector, 8, 1, 1, 1)

        self.comments_bar = QtGui.QStatusBar(self.centralwidget)
        self.comments_bar.showMessage("Comments:")
        self.comments_bar.setSizeGripEnabled(0)
        self.comments_bar.setMaximumSize(QtCore.QSize(900, 100))
        self.comments_bar.setObjectName(_fromUtf8("comments_bar"))
        self.gridLayout_acq.addWidget(self.comments_bar, 9, 0, 1, 1)

        self.lineEdit_comments = QtGui.QLineEdit(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineEdit_file_path.sizePolicy().hasHeightForWidth())
        self.lineEdit_comments.setSizePolicy(sizePolicy)
        self.lineEdit_comments.setMaximumSize(QtCore.QSize(800, 16777215))
        self.lineEdit_comments.setObjectName(_fromUtf8("lineEdit_comments"))
        self.gridLayout_acq.addWidget(self.lineEdit_comments, 9, 1, 1, 1)

        self.horizontalLayout_2.addLayout(self.gridLayout_acq)

        self.line_7 = QtGui.QFrame(self.centralwidget)
        self.line_7.setFrameShape(QtGui.QFrame.VLine)
        self.line_7.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_7.setObjectName(_fromUtf8("line_7"))
        self.horizontalLayout_2.addWidget(self.line_7)


        ## Log
        self.verticalLayout_9 = QtGui.QVBoxLayout()
        self.verticalLayout_9.setSizeConstraint(QtGui.QLayout.SetMaximumSize)
        self.verticalLayout_9.setObjectName(_fromUtf8("verticalLayout_9"))
        self.horizontalLayout_2.addLayout(self.verticalLayout_9)

        self.label_log = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_log.sizePolicy().hasHeightForWidth())
        self.label_log.setSizePolicy(sizePolicy)
        self.label_log.setObjectName(_fromUtf8("label_log"))
        self.verticalLayout_9.addWidget(self.label_log)

        # self.checkBox_cont_acq = QtGui.QCheckBox(self.centralwidget)
        # sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        # sizePolicy.setHorizontalStretch(0)
        # sizePolicy.setVerticalStretch(0)
        # sizePolicy.setHeightForWidth(self.checkBox_cont_acq.sizePolicy().hasHeightForWidth())
        # self.checkBox_cont_acq.setSizePolicy(sizePolicy)
        # self.checkBox_cont_acq.setObjectName(_fromUtf8("checkBox_cont_acq"))
        # self.verticalLayout_9.addWidget(self.checkBox_cont_acq)

        self.pushButton_run = QtGui.QPushButton(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButton_run.sizePolicy().hasHeightForWidth())
        self.pushButton_run.setCheckable(True)
        self.pushButton_run.setSizePolicy(sizePolicy)
        self.pushButton_run.setMaximumSize(QtCore.QSize(800, 16777215))
        self.pushButton_run.setObjectName(_fromUtf8("pushButton_background"))
        self.verticalLayout_9.addWidget(self.pushButton_run)


        self.status_bar = QtGui.QStatusBar(self.centralwidget)
        self.status_bar.showMessage("System Status: GUI started")
        self.status_bar.setSizeGripEnabled(0)
        self.status_bar.setMaximumSize(QtCore.QSize(900, 100))
        self.status_bar.setObjectName(_fromUtf8("status_bar"))
        self.verticalLayout_9.addWidget(self.status_bar)

        self.status_bar_fr = QtGui.QStatusBar(self.centralwidget)
        self.status_bar_fr.showMessage("Framerate display:")
        self.status_bar_fr.setSizeGripEnabled(0)
        self.status_bar_fr.setMaximumSize(QtCore.QSize(900, 100))
        self.status_bar_fr.setObjectName(_fromUtf8("status_bar_fr"))
        self.verticalLayout_9.addWidget(self.status_bar_fr)


        self.textEdit = QtGui.QTextEdit(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        self.textEdit.setMaximumSize(QtCore.QSize(1024, 900))
        self.textEdit.setSizePolicy(sizePolicy)
        self.textEdit.setReadOnly(True)
        self.textEdit.setObjectName(_fromUtf8("textEdit"))
        # self.textEdit.setStyleSheet("background-image: url(kit.png); background-attachment: fixed; background-position: bottom right; background-repeat: no-repeat")
        self.verticalLayout_9.addWidget(self.textEdit)


        #self.horizontalLayout_2.addLayout(self.gridLayout)
        self.line_5 = QtGui.QFrame(self.centralwidget)
        self.line_5.setFrameShape(QtGui.QFrame.VLine)
        self.line_5.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_5.setObjectName(_fromUtf8("line_5"))
        self.horizontalLayout_2.addWidget(self.line_5)


###############################------------------------------------LED------------------------------------------#########################################################
        self.gridLayout_led = QtGui.QGridLayout()
        self.gridLayout_led.setSizeConstraint(QtGui.QLayout.SetMaximumSize)
        self.gridLayout_led.setObjectName(_fromUtf8("gridLayout_led"))
        self.horizontalLayout_2.addLayout(self.gridLayout_led)
        self.verticalLayout_10 = QtGui.QVBoxLayout()
        self.verticalLayout_10.setSizeConstraint(QtGui.QLayout.SetMaximumSize)
        self.verticalLayout_10.setObjectName(_fromUtf8("verticalLayout_10"))
        self.horizontalLayout_2.addLayout(self.verticalLayout_10)


        self.led1=QLed(self.centralwidget, onColour=QLed.Red,offColour=QLed.Green, shape=QLed.Circle)
        self.led1.setMaximumSize(QtCore.QSize(10, 100))
        sizePolicy.setHeightForWidth(self.led1.sizePolicy().hasHeightForWidth())
        self.led1.setSizePolicy(sizePolicy)
        #self.led3.value=True
        self.label_led1 = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_led1.sizePolicy().hasHeightForWidth())
        self.label_led1.setSizePolicy(sizePolicy)
        self.label_led1.setObjectName(_fromUtf8("led 1"))
        self.label_led1.setAlignment(Qt.AlignCenter)
        self.gridLayout_led.addWidget(self.label_led1,1,1,1,1)
        self.led1.setObjectName(_fromUtf8("Led 1"))
        self.gridLayout_led.addWidget(self.led1,1,0,1,1)


        self.led2=QLed(self.centralwidget, onColour=QLed.Red,offColour=QLed.Green, shape=QLed.Circle)
        self.led2.setMaximumSize(QtCore.QSize(10, 100))
        sizePolicy.setHeightForWidth(self.led2.sizePolicy().hasHeightForWidth())
        self.led2.setSizePolicy(sizePolicy)
        #self.led2.value=True
        self.label_led2 = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_led2.sizePolicy().hasHeightForWidth())
        self.label_led2.setSizePolicy(sizePolicy)
        self.label_led2.setObjectName(_fromUtf8("led 2"))
        self.label_led2.setAlignment(Qt.AlignCenter)
        self.gridLayout_led.addWidget(self.label_led2,2,1,1,1)
        self.led2.setObjectName(_fromUtf8("Led 2"))
        self.gridLayout_led.addWidget(self.led2,2,0,1,1)


        self.led3=QLed(self.centralwidget, onColour=QLed.Red,offColour=QLed.Green, shape=QLed.Circle)
        self.led3.setMaximumSize(QtCore.QSize(10, 100))
        sizePolicy.setHeightForWidth(self.led3.sizePolicy().hasHeightForWidth())
        self.led3.setSizePolicy(sizePolicy)
        #self.led3.value=True
        self.label_led3 = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_led3.sizePolicy().hasHeightForWidth())
        self.label_led3.setSizePolicy(sizePolicy)
        self.label_led3.setObjectName(_fromUtf8("led 3"))
        self.label_led3.setAlignment(Qt.AlignCenter)
        self.gridLayout_led.addWidget(self.label_led3,3,1,1,1)
        self.led3.setObjectName(_fromUtf8("Led 3"))
        self.gridLayout_led.addWidget(self.led3,3,0,1,1)


        self.led4=QLed(self.centralwidget, onColour=QLed.Red,offColour=QLed.Green, shape=QLed.Circle)
        self.led4.setMaximumSize(QtCore.QSize(10, 100))
        sizePolicy.setHeightForWidth(self.led4.sizePolicy().hasHeightForWidth())
        self.led4.setSizePolicy(sizePolicy)
        #self.led4.value=True
        self.label_led4 = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_led4.sizePolicy().hasHeightForWidth())
        self.label_led4.setSizePolicy(sizePolicy)
        self.label_led4.setAlignment(Qt.AlignCenter)
        self.label_led4.setObjectName(_fromUtf8("led 4"))
        self.gridLayout_led.addWidget(self.label_led4,4,1,1,1)
        self.led4.setObjectName(_fromUtf8("Led 4"))
        self.gridLayout_led.addWidget(self.led4,4,0,1,1)


        self.led5=QLed(self.centralwidget, onColour=QLed.Red,offColour=QLed.Green, shape=QLed.Circle)
        self.led5.setMaximumSize(QtCore.QSize(10, 100))
        sizePolicy.setHeightForWidth(self.led5.sizePolicy().hasHeightForWidth())
        self.led5.setSizePolicy(sizePolicy)
        #self.led5.value=True
        self.label_led5 = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_led5.sizePolicy().hasHeightForWidth())
        self.label_led5.setSizePolicy(sizePolicy)
        self.label_led5.setAlignment(Qt.AlignCenter)
        self.label_led5.setObjectName(_fromUtf8("led 5"))
        self.gridLayout_led.addWidget(self.label_led5,5,1,1,1)
        self.led5.setObjectName(_fromUtf8("Led 5"))
        self.gridLayout_led.addWidget(self.led5,5,0,1,1)


        self.led6=QLed(self.centralwidget, onColour=QLed.Red,offColour=QLed.Green, shape=QLed.Circle)
        self.led6.setMaximumSize(QtCore.QSize(10, 100))
        sizePolicy.setHeightForWidth(self.led6.sizePolicy().hasHeightForWidth())
        self.led6.setSizePolicy(sizePolicy)
        #self.led6.value=True
        self.label_led6 = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_led6.sizePolicy().hasHeightForWidth())
        self.label_led6.setSizePolicy(sizePolicy)
        self.label_led6.setAlignment(Qt.AlignCenter)
        self.label_led6.setObjectName(_fromUtf8("led 6"))
        self.gridLayout_led.addWidget(self.label_led6,6,1,1,1)
        self.led6.setObjectName(_fromUtf8("Led 6"))
        self.gridLayout_led.addWidget(self.led6,6,0,1,1)


        self.led7=QLed(self.centralwidget, onColour=QLed.Red,offColour=QLed.Green, shape=QLed.Circle)
        self.led7.setMaximumSize(QtCore.QSize(10, 100))
        sizePolicy.setHeightForWidth(self.led7.sizePolicy().hasHeightForWidth())
        self.led7.setSizePolicy(sizePolicy)
        #self.led7.value=True
        self.label_led7 = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_led7.sizePolicy().hasHeightForWidth())
        self.label_led7.setSizePolicy(sizePolicy)
        self.label_led7.setAlignment(Qt.AlignCenter)
        self.label_led7.setObjectName(_fromUtf8("led 7"))
        self.gridLayout_led.addWidget(self.label_led7,7,1,1,1)
        self.led7.setObjectName(_fromUtf8("Led 7"))
        self.gridLayout_led.addWidget(self.led7,7,0,1,1)


        self.led8=QLed(self.centralwidget, onColour=QLed.Red,offColour=QLed.Green, shape=QLed.Circle)
        self.led8.setMaximumSize(QtCore.QSize(10, 100))
        sizePolicy.setHeightForWidth(self.led8.sizePolicy().hasHeightForWidth())
        self.led8.setSizePolicy(sizePolicy)
        #self.led8.value=True
        self.label_led8 = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_led8.sizePolicy().hasHeightForWidth())
        self.label_led8.setSizePolicy(sizePolicy)
        self.label_led8.setAlignment(Qt.AlignCenter)
        self.label_led8.setObjectName(_fromUtf8("led 8"))
        self.gridLayout_led.addWidget(self.label_led8,8,1,1,1)
        self.led8.setObjectName(_fromUtf8("Led 8"))
        self.gridLayout_led.addWidget(self.led8,8,0,1,1)


        self.led9=QLed(self.centralwidget, onColour=QLed.Red,offColour=QLed.Green, shape=QLed.Circle)
        self.led9.setMaximumSize(QtCore.QSize(10, 100))
        sizePolicy.setHeightForWidth(self.led9.sizePolicy().hasHeightForWidth())
        self.led9.setSizePolicy(sizePolicy)
        #self.led9.value=True
        self.label_led9 = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_led9.sizePolicy().hasHeightForWidth())
        self.label_led9.setSizePolicy(sizePolicy)
        self.label_led9.setAlignment(Qt.AlignCenter)
        self.label_led9.setObjectName(_fromUtf8("led 5"))
        self.gridLayout_led.addWidget(self.label_led9,9,1,1,1)
        self.led9.setObjectName(_fromUtf8("Led 5"))
        self.gridLayout_led.addWidget(self.led9,9,0,1,1)


        self.led10=QLed(self.centralwidget, onColour=QLed.Red,offColour=QLed.Green, shape=QLed.Circle)
        self.led10.setMaximumSize(QtCore.QSize(10, 100))
        sizePolicy.setHeightForWidth(self.led10.sizePolicy().hasHeightForWidth())
        self.led10.setSizePolicy(sizePolicy)
        #self.led10.value=True
        self.label_led10 = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_led10.sizePolicy().hasHeightForWidth())
        self.label_led10.setSizePolicy(sizePolicy)
        self.label_led10.setAlignment(Qt.AlignCenter)
        self.label_led10.setObjectName(_fromUtf8("led 6"))
        self.gridLayout_led.addWidget(self.label_led10,10,1,1,1)
        self.led10.setObjectName(_fromUtf8("Led 6"))
        self.gridLayout_led.addWidget(self.led10,10,0,1,1)


        self.led11=QLed(self.centralwidget, onColour=QLed.Red,offColour=QLed.Green, shape=QLed.Circle)
        self.led11.setMaximumSize(QtCore.QSize(10, 100))
        sizePolicy.setHeightForWidth(self.led11.sizePolicy().hasHeightForWidth())
        self.led11.setSizePolicy(sizePolicy)
        #self.led11.value=True
        self.label_led11 = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_led11.sizePolicy().hasHeightForWidth())
        self.label_led11.setSizePolicy(sizePolicy)
        self.label_led11.setAlignment(Qt.AlignCenter)
        self.label_led11.setObjectName(_fromUtf8("led 7"))
        self.gridLayout_led.addWidget(self.label_led11,11,1,1,1)
        self.led11.setObjectName(_fromUtf8("Led 7"))
        self.gridLayout_led.addWidget(self.led11,11,0,1,1)


        self.led12=QLed(self.centralwidget, onColour=QLed.Red,offColour=QLed.Green, shape=QLed.Circle)
        self.led12.setMaximumSize(QtCore.QSize(10, 100))
        sizePolicy.setHeightForWidth(self.led12.sizePolicy().hasHeightForWidth())
        self.led12.setSizePolicy(sizePolicy)
        #self.led12.value=True
        self.label_led12 = QtGui.QLabel(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_led12.sizePolicy().hasHeightForWidth())
        self.label_led12.setSizePolicy(sizePolicy)
        self.label_led12.setAlignment(Qt.AlignCenter)
        self.label_led12.setObjectName(_fromUtf8("led 8"))
        self.gridLayout_led.addWidget(self.label_led12,12,1,1,1)
        self.led12.setObjectName(_fromUtf8("Led 8"))
        self.gridLayout_led.addWidget(self.led12,12,0,1,1)



        #self.pushButton_custom_function_2 = QtGui.QPushButton(self.centralwidget)
        #sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        #sizePolicy.setHorizontalStretch(0)
        #sizePolicy.setVerticalStretch(0)
        #sizePolicy.setHeightForWidth(self.pushButton_custom_function_2.sizePolicy().hasHeightForWidth())
        #self.pushButton_custom_function_2.setSizePolicy(sizePolicy)
        #self.pushButton_custom_function_2.setMaximumSize(QtCore.QSize(400, 10000))
        #self.pushButton_custom_function_2.setObjectName(_fromUtf8("pushButton_custom_function_2"))
        #self.verticalLayout_2.addWidget(self.pushButton_custom_function_2)

        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.verticalLayout_3.addLayout(self.verticalLayout)
        MainWindow.setCentralWidget(self.centralwidget)

        ######### PLOT  ######### ######### ######### ######### ######### #########
        pw=self.graphicsView

        pw.setWindowTitle('KALYPSO data viewer')
        pw.setLabel('bottom', 'Pixel')
        pw.setLabel('left', 'Signal', units='ADC Counts')
        self.modplot.setLabel('bottom', 'Sample number')
        self.modplot.setLabel('left', 'Pixel number')
        pw.setXRange(0, NUMBER_PIXELS)
        pw.setYRange(0, 2**14)
        pw.showGrid(1,1)
        pw.addLegend()
        curve = pw.plot(pen='g', name='raw')
        curve_bg = pw.plot(pen='r', alpha = '0.8', name='w/o bg')
        curve_std_p = pw.plot(pen=pg.mkPen(200, 0, 50, 160),name='+/- std.')
        curve_std_l = pw.plot(pen=pg.mkPen(200, 0, 50, 160))
        curve_snr   = pw.plot(pen=pg.mkPen(0, 0, 250, 160), name='SNR')






        def update_plots(data):
            if (self.tab_widget.currentIndex()==0):
                plot_1D(data)
            elif (self.tab_widget.currentIndex()==1):
                plot_2D(data)

        # Plot Waveform
        def plot_1D(data):
            global background_data
            global unmodulated_data

            pix_std=np.std(data,axis = 0)

            if self.checkBox_average.isChecked():
                data = np.average(data, axis = 0)
            else:
                data = data[OFFSET,:]

            ## Remove BG
            if self.checkBox_calc_modul.isChecked():
                modulation = (data - background_data) / (unmodulated_data - background_data)
                curve_bg.setData(modulation)
                curve_snr.clear()
                curve_std_l.clear()
                curve_std_p.clear()
                curve.clear()
            elif self.checkBox_remove_back.isChecked():
                curve_bg.setData(data - background_data)
                curve.clear()
                curve_std_p.setData(data - background_data+pix_std)
                curve_std_l.setData(data - background_data-pix_std)
                curve_snr.setData((data - background_data)/pix_std)
                # curve_snr.setData(pix_std)
            else:
                curve.setData(data.flatten())
                curve_std_p.setData(data+pix_std)
                curve_std_l.setData(data-pix_std)
                curve_snr.clear()
                curve_bg.clear()

            pw.setXRange(int(self.spinbox_pix_min.text()), int(self.spinbox_pix_max.text()))
            app.processEvents()


        # Input data must be array, not filename!
        def plot_2D(data):
            global background_data
            global unmodulated_data

            samples , pix = np.shape(data)
            if self.checkBox_calc_modul.isChecked():
                data = (data - background_data) / (unmodulated_data - background_data)
            else:
                if self.checkBox_remove_back.isChecked():
                    data = data - background_data
                else:
                    data = data

            self.img.setImage(data, autoLevels=0, autoDownsample="True")
            self.modplot.setXRange(0,samples)
            self.modplot.setYRange(int(self.spinbox_pix_min.text()), int(self.spinbox_pix_max.text()))

            #self.hist.setHistogramRange(0.5,1.5)
            #self.hist.setLevels(0.5,1.5)

        #def terminal_cmd_checkbox():
        #    if self.checkBox_cont_acq.isChecked():
        #        if(int(self.spinBox_total_samples.text()) < 1000000):
        #            subprocess.call(['./normal_acq.sh'])
        #        else:
        #            self.textEdit.setText('Too many samples selected for "Live preview"')
        #            self.checkBox_cont_acq.toggle()
        #        output_file = 'temp.bin'
        #        if os.path.isfile(output_file):
        #            data , dc, orb  = strip_data.strip_data(output_file, NUMBER_PIXELS, 0)
        #            if dc:
        #                update_plots(data)
        #            else:
        #                self.textEdit.append(u'\u26A0' + ' Data consistency check failed!')
        #        else:
        #            self.textEdit.append(u'\u26A0' + ' Data file does not exist!')
        #        check_status()



        def terminal_cmd_checkbox():
            if self.checkBox_cont_acq.isChecked():
                if(int(self.spinBox_total_samples.text()) < 1000000):
                    subprocess.call(['./normal_acq.sh'])
                else:
                    self.textEdit.setText('Too many samples selected for "Live preview"')
                    self.checkBox_cont_acq.toggle()
                output_file = 'temp.bin'
                if os.path.isfile(output_file):
                    data, orb  = strip_data.strip_data(output_file, NUMBER_PIXELS, 0)
                    update_plots(data)
                else:
                    self.textEdit.append(u'\u26A0' + ' Data file does not exist!')
                check_status()
                led_update()
                framerate_calc()



        def terminal_cmd_run():
            if self.pushButton_run.isChecked():
               self.pushButton_run.setStyleSheet("background-color : red")
               self.pushButton_run.setText(_translate("MainWindow", "STOP", None))
               self.pushButton_run.setEnabled(True)
               if(int(self.spinBox_total_samples.text()) < 1000000):
                   subprocess.call(['./normal_acq.sh'])
               else:
                   self.textEdit.setText('Too many samples selected for "Live preview"')
                   self.checkBox_cont_acq.toggle()
               output_file = 'temp.bin'
               if os.path.isfile(output_file):
                   data, orb  = strip_data.strip_data(output_file, NUMBER_PIXELS, 0)
                   update_plots(data)
               else:
                   self.textEdit.append(u'\u26A0' + ' Data file does not exist!')
               check_status()
               led_update()
               framerate_calc()
            else:
                self.pushButton_run.setStyleSheet("background-color : green")
                self.pushButton_run.setText(_translate("MainWindow", "RUN", None))
            check_status()
            led_update()








        def rst_slow_trigger_samples():
            subprocess.check_output(['pci', '-w', self.registers["ST_TO_ACQ"]["address"],'0'])

        def init_board():
            self.textEdit.clear()
            if (read_register("PCIE_RESET",hex=True) != "14021700"):
                self.status_bar.showMessage('Error!', 1000)
                self.textEdit.insertPlainText(u'\u26A0' + ' Communication failed with FPGA board... Check FPGA, the PCI-Express connection and reboot')
                check_status()
                led_update()
            else:
                self.status_bar.showMessage('Initializing board...', 5000)
                subprocess.call(['./full_sequence.sh'])
                self.spinBox_int_time.setValue(8)
                self.spinBox_int_delay.setValue(0)
                self.spinBox_gott_gain.setValue(2)
                self.spinBox_total_samples.setValue(100)
                self.spinBox_skip_samples.setValue(0)
                self.spinBox_int_trig.setValue(126)
                self.spinBox_jesd_delay.setValue(50)
                rst_slow_trigger_samples()
                self.status_bar.showMessage('Board initialized', 1000)
                self.textEdit.insertPlainText(subprocess.check_output(['./status.sh']))
                check_status()
                led_update()
                #framerate_calc()

        def init_board_ex_rf():
            self.textEdit.clear()
            if (read_register("PCIE_RESET",hex=True) != "14021700"):
                self.status_bar.showMessage('Error!', 1000)
                self.textEdit.insertPlainText(u'\u26A0' + ' Communication failed with FPGA board... Check FPGA, the PCI-Express connection and reboot')
                check_status()
                led_update()
            else:
                self.status_bar.showMessage('Initializing board...', 5000)
                subprocess.call(['./full_sequence_ex_rf.sh'])
                self.spinBox_int_time.setValue(8)
                self.spinBox_int_delay.setValue(0)
                self.spinBox_gott_gain.setValue(2)
                self.spinBox_total_samples.setValue(100)
                self.spinBox_skip_samples.setValue(0)
                self.spinBox_int_trig.setValue(126)
                self.spinBox_jesd_delay.setValue(50)
                rst_slow_trigger_samples()
                self.status_bar.showMessage('Board initialized with external RF parameters', 1000)
                self.textEdit.insertPlainText(subprocess.check_output(['./status.sh']))
                check_status()
                led_update()
                #framerate_calc()

#######################################################################################################################

        def framerate_calc():
            acq_time = read_register("FRAME_RATE", hex = False)
            acq_time = float(acq_time*0.2)
            total_samples = read_register("FT_ACQ", hex = False)
            framerate = float(total_samples / acq_time)
            #self.status_bar_fr.showMessage('Framerate (Hz): '+framerate + ' : ' + value, 1000)
            #self.status_bar_fr.showMessage('Framerate (Mfps):' +str(framerate), 1000)
            self.textEdit.append('Framerate (Mfps):' +str(framerate))
            return framerate


        def read_int_time_value():
            integration_time_rd = read_register("INTEG_DURATION", hex = False)
            integration_time_r = integration_time_rd * 8
            return integration_time_r

        def write_int_time_value():
            integration_time_wr = self.spinBox_int_time.text()
            integration_time_w = int(float(integration_time_wr) )// 8
            integration_time_w = str(integration_time_w)
            write_reg("INTEG_DURATION",integration_time_w)
            return integration_time_w


        def read_int_delay_value():
            integration_delay_rd = read_register("INTEG_DELAY", hex = False)
            integration_delay_r = integration_delay_rd * 8
            return integration_delay_r

        def write_int_delay_value():
            integration_delay_wr = self.spinBox_int_delay.text()
            integration_delay_w = int(float(integration_delay_wr) )// 8
            integration_delay_w = str(integration_delay_w)
            write_reg("INTEG_DELAY",integration_delay_w)
            return integration_delay_w


        def tx_trig():
            self.pushButton_run.setFlat(0)
            subprocess.call(['./tx_trig_adcs.sh'])

        def phase_swap():
            self.pushButton_run.setFlat(0)
            phase0 = 9
            phase180 = 12297 #3009
            phase_set = read_register("PHASE_SWAP", hex = False)
            if (str(phase_set) == str(phase0)):
                write_reg("PHASE_SWAP", str(phase180)) #3008
            else:
                write_reg("PHASE_SWAP", str(phase0))  #008


########################################################################################################################
        def timescan():
            self.textEdit.setText(timestamp())
            if os.path.isfile('temp_bg.bin'):
                os.system('pci -w 0x9020 0x80')
                self.textEdit.insertPlainText(subprocess.check_output(['./timescan.sh']))
                self.textEdit.append('Timescan complete')
                timescan_plot.timescan_plot('timescan_data.out', 8 ,512)
                #timescan_plot.timescan_plot('timescan_data.out', 128 ,1023)
                self.status_bar.showMessage('Timescan', 1000)
                #write_total_samples()
            else:
                self.textEdit.append(u'\u26A0' + ' Take background measurement first!')

        def get_filename():
            return str(self.lineEdit_file_path.text() + 'f' + self.lineEdit_fillnum.text() + '_' + filename_timestamp() + '_' + self.lineEdit_detector.text())

        def readout_modulated():
            #self.checkBox_cont_acq.setCheckState(0)
            #self.pushButton_run.setCheckState(0)
            if self.checkBox_savefile.isChecked():
                output_file = (get_filename() + '_m.bin')
            else:
                output_file = 'temp.bin'
            time.sleep(0.1)
            self.textEdit.setText(timestamp())
            subprocess.call(['./normal_acq.sh', '-f', output_file])
            if os.path.isfile(output_file):
                # if self.checkBox_ELOG.isChecked():
                #     elog_eosd.elog_eosd(output_file, 'm')
                #     print ('Creating ELOG entry...')
                if self.checkBox_savefile.isChecked():
                    write_csv(output_file)
                if (int(self.spinBox_total_samples.text()) < 1000000):
                    data, orb  = strip_data.strip_data(output_file, NUMBER_PIXELS, 0)
                    self.textEdit.setText('Acquired ' + str(orb) + ' samples in ' + output_file)
                    update_plots(data)

                else:
                    self.textEdit.append('*** File too large to process in GUI...')
            else:
                self.textEdit.append(u'\u26A0' + ' Data file does not exist!')
            check_status()
            led_update()
            framerate_calc()

        def readout_background():
            global background_data
            #self.checkBox_cont_acq.setCheckState(0)
            #self.pushButton_run.setCheckState(0)
            if self.checkBox_savefile.isChecked():
                output_file = (get_filename() + '_b.bin')
            else:
                output_file = 'temp_bg.bin'
            time.sleep(0.1)
            self.textEdit.setText(timestamp())
            subprocess.call(['./normal_acq.sh', '-f', output_file])
            if os.path.isfile(output_file):
                # if self.checkBox_ELOG.isChecked():
                    ##elog_eosd.elog_eosd(output_file, 'b')
                    # print ('Creating ELOG entry...')
                if self.checkBox_savefile.isChecked():
                    write_csv(output_file)
                if (int(self.spinBox_total_samples.text()) < 1000000):
                    data, orb  = strip_data.strip_data(output_file, NUMBER_PIXELS, 0)
                    self.textEdit.setText('Acquired ' + str(orb) + ' background in ' + output_file)
                    background_data = np.average(data,axis = 0)

                else:
                    self.textEdit.append('*** File too large to process in GUI...')
            else:
                self.textEdit.append(u'\u26A0' + ' Data file does not exist!')
            check_status()
            led_update()
            framerate_calc()


        def readout_unmodulated():
            global unmodulated_data
            #self.checkBox_cont_acq.setCheckState(0)
            #self.pushButton_run.setCheckState(0)
            if self.checkBox_savefile.isChecked():
                output_file = (get_filename() + '_u.bin')
            else:
                output_file = 'temp_un.bin'
            time.sleep(0.1)
            self.textEdit.setText(timestamp())
            subprocess.call(['./normal_acq.sh', '-f', output_file])
            if os.path.isfile(output_file):
                # if self.checkBox_ELOG.isChecked():
                    # print ('Creating ELOG entry...')
                    ##elog_eosd.elog_eosd(output_file, 'u')
                if self.checkBox_savefile.isChecked():
                    write_csv(output_file)
                if (int(self.spinBox_total_samples.text()) < 1000000):
                    data, orb  = strip_data.strip_data(output_file, NUMBER_PIXELS, 0)
                    self.textEdit.setText('Acquired ' + str(orb) + ' unmodulated in ' + output_file)
                    unmodulated_data = np.average(data,axis = 0)

                else:
                    self.textEdit.append('*** File too large to process in GUI...')
            else:
                self.textEdit.append(u'\u26A0' + ' Data file does not exist!')
            check_status()
            led_update()
            framerate_calc()

        def poweroff():
            self.textEdit.setText(timestamp())
            self.textEdit.insertPlainText(subprocess.check_output(['./poweroff.sh']))

        def timestamp():
            return datetime.datetime.now().strftime("[%H:%M:%S] ")

        def filename_timestamp():
            return (time.strftime("%Y-%m-%dT%Hh%Mm%Ss"))

        # Dialog to set path to save readout file
        def getDir():
            dialog = QtGui.QFileDialog()
            dialog.setFileMode(QtGui.QFileDialog.Directory)
            dialog.setOption(QtGui.QFileDialog.ShowDirsOnly)
            self.lineEdit_file_path.setText(_translate("MainWindow", dialog.getExistingDirectory(None, 'Choose Directory') + '/', None))

        # Dialog to get unmodulated data, load result into global var unmodulated_data
        def get_unmodulated():
            global unmodulated_data
            self.textEdit.setText(timestamp())
            dialog = QtGui.QFileDialog()
            dialog.setFileMode(QtGui.QFileDialog.ExistingFile)
            unmodulated_filename = (_translate("MainWindow", dialog.getOpenFileName(None, 'Choose unmodulated file...'), None))
            unmodulated_data = calc_average_from_file(unmodulated_filename)
            self.textEdit.append('Opening unmodulated file: ' + unmodulated_filename)

        # Dialog to get background file, load result into global var background_data
        def get_background():
            global background_data
            self.textEdit.setText(timestamp())
            dialog = QtGui.QFileDialog()
            dialog.setFileMode(QtGui.QFileDialog.ExistingFile)
            background_filename = (_translate("MainWindow", dialog.getOpenFileName(None, 'Choose background file...'), None))
            background_data = calc_average_from_file(background_filename)
            self.textEdit.append('Opening background file: ' + background_filename)


        # Dialog to get modulated file, plots are updated
        def get_modulated():
            dialog = QtGui.QFileDialog()
            self.textEdit.setText(timestamp())
            dialog.setFileMode(QtGui.QFileDialog.ExistingFile)
            modulated_filename = (_translate("MainWindow", dialog.getOpenFileName(None, 'Choose modulated file...'), None))
            data ,orb  = strip_data.strip_data(modulated_filename, NUMBER_PIXELS, 0)
            self.textEdit.append('Opening ' + str(orb) + ' samples from modulated file: ' + modulated_filename)
            update_plots(data)

        def read_pcie_register(reg_number):
            ## Output is a string with (rubbish) + (8 chars for values, starting from [11])
            try:
                pcitool_string = subprocess.check_output(['./read_reg.sh', '-r', str(reg_number)])
            except subprocess.CalledProcessError as e:
                pcitool_string = '0'
            #pcitool_string = subprocess.check_output(['./read_reg.sh', '-r', str(reg_number)])
            value = pcitool_string[11:19]
            ##value = '00011110'
            return value

        def read_register(reg_key, hex=False):
            ## Output from 'pci -r' function in terminal is a string with (rubbish)
            ## + (8 chars for values, starting from [11])
            pcitool_string = subprocess.check_output(['pci', '-r', self.registers[reg_key]["address"]])
            if hex==True:
                return pcitool_string[11:19]
            else:
                value = int(pcitool_string[11:19],16) & int(self.registers[reg_key]["mask"],16)
                return value

        def write_reg(reg_key,value):
            subprocess.check_output(['pci', '-w', self.registers[reg_key]["address"],hex(int(value))])
            name_of_reg_written = self.registers[reg_key]["name"]
            self.status_bar.showMessage('Writing '+name_of_reg_written + ' : ' + value, 1000)

        # Calculate average from filename, if DC is not correct returns 0
        def calc_average_from_file(filename):
            data, orb  = strip_data.strip_data(filename, NUMBER_PIXELS, 0)
            average = np.average(data,axis = 0)
            return average


        # Function used to create .CSV with all the settings together with data file
        def write_csv(output_file):
            csvfile = output_file[:-3] + "csv"
            with open(csvfile, "w") as output:
                writer = csv.writer(output, lineterminator='\n')
                entries = []
                for key in self.log_conf["log_entries"]:
                    entries.append([str(self.registers[key]["name"]),read_register(key)])
                entries.append(["Timestamp",filename_timestamp()])
                entries.append(["Fill number",self.lineEdit_fillnum.text()])
                entries.append(["Detector",self.lineEdit_detector.text()])
                entries.append(["Comments",self.lineEdit_comments.text()])
                writer.writerows(entries)

        # Read-back the settings from FPGA and saves them in the GUI
        def readback_settings():
            try:
                if (read_register("PCIE_RESET",hex=True) != "14021700"):
                    self.status_bar.showMessage('Error!', 1000)
                    self.textEdit.insertPlainText(u'\u26A0' + ' Communication failed with FPGA board... Check FPGA, the PCI-Express connection and reboot')
                else:
                    self.spinBox_int_time.setValue(read_int_time_value())
                    self.spinBox_int_delay.setValue(read_register("INTEG_DELAY"))
                    #self.spinBox_int_delay.setValue(read_int_delay_value())
                    self.spinBox_gott_gain.setValue(read_register("GOTT_GAIN"))
                    self.spinBox_total_samples.setValue(read_register("FT_TO_ACQ"))
                    self.spinBox_skip_samples.setValue(read_register("FT_SKIP"))
                    self.spinBox_int_trig.setValue(read_register("CLK_DIV"))
                    self.spinBox_jesd_delay.setValue(read_register("JESD_DELAY"))

            except:
                self.status_bar.showMessage('Error!', 1000)
                self.textEdit.insertPlainText(u'\u26A0' + ' Communication failed with FPGA board... Check FPGA, the PCI-Express connection and reboot')


        # Check status and decode errors
        def check_status():
            status_1 = read_register("STATUS_1", hex=True)
            status_1 = read_register("STATUS_1", hex=True)
            status_1 = read_register("STATUS_1", hex=True)
            status_1 = int(status_1,16)
            status_1 = '{:032b}'.format(status_1)
            #if (status_1 == '0000000000000000000000000000000000000000'):
            #    self.status_bar.showMessage(u'Status: \u2713', 5000)
            #else:
            #    self.status_bar.showMessage(u'Status: \u26A0 error detected!', 5000)
            #    self.textEdit.setTextColor(QtGui.QColor(255, 50, 0))
                #if (status_1[0] != '0'):
                #    self.textEdit.append(u'\u26A0' + ' Fast trigger is too fast or integration time is too high!')
                #if (status_1[6] != '0'):
                #    self.textEdit.append(u'\u26A0' + ' ADC fifos full! Problem with onboard DDR memory, turn on/off FPGA board and reboot the system.')
                #if (status_1[8] != '0'):
                #    self.textEdit.append(u'\u26A0' + ' DDR fifo is full during data readout! Re-init, check PCIe connection')
                #if (status_1[16] != '0'):
                #    self.textEdit.append(u'\u26A0' + ' Synchronization lost during operation! Check RF-CLK and re-init the system')

            self.textEdit.setTextColor(QtGui.QColor(0, 0, 0))
            return




        def led_update():
                status_led = read_register("STATUS_1", hex=True)
                status_led = read_register("STATUS_1", hex=True)
                status_led = int(status_led,16)
                status_led = '{:032b}'.format(status_led)
                #print(status_led)
                if (status_led == '0000000000000000000000000000000000000000'):
                    self.led1.value=False
                    self.led2.value=False
                    self.led3.value=False
                    self.led4.value=False
                    self.led5.value=False
                    self.led6.value=False
                    self.led7.value=False
                    self.led8.value=False
                    self.led9.value=False
                    self.led10.value=False
                    self.led11.value=False
                    self.led12.value=False
                else:
                    if (status_led[4] != '0'):
                        self.led12.value=True
                        self.textEdit.append(u'\u26A0' + 'Jesd link error, Re-initialize')
                    else:
                        self.led12.value=False
                    if (status_led[5] != '0'):
                        self.led11.value=True
                        self.textEdit.append(u'\u26A0' + 'Jesd link error, Re-initialize')
                    else:
                        self.led11.value=False
                    if (status_led[6] != '1'):
                        self.led10.value=True
                        self.textEdit.append(u'\u26A0' + 'Jesd readout error, Re-initialize')
                    else:
                        self.led10.value=False
                    if (status_led[8] != '1'):
                        self.led9.value=True
                        self.textEdit.append(u'\u26A0' + 'Jesd readout error, Re-initialize')
                    else:
                        self.led9.value=False
                    if (status_led[9] != '1'):
                        self.led8.value=True
                        self.textEdit.append(u'\u26A0' + 'Jesd not in sync, Re-initialize')
                    else:
                        self.led8.value=False
                    if (status_led[10] != '1'):
                        self.led7.value=True
                        self.textEdit.append(u'\u26A0' + 'Jesd not in sync, Re-initialize')
                    else:
                        self.led7.value=False
                    if (status_led[11] != '0'):
                        self.led6.value=True
                        self.textEdit.append(u'\u26A0' + 'Pll error')
                    else:
                        self.led6.value=False
                    if (status_led[12] != '0'):
                        self.led5.value=True
                        self.textEdit.append(u'\u26A0' + 'Pll error')
                    else:
                        self.led5.value=False
                    if (status_led[13] != '0'):
                        self.led4.value=True
                        self.textEdit.append(u'\u26A0' + 'Pll error')
                    else:
                        self.led4.value=False
                    if (status_led[14] != '0'):
                        self.led3.value=True
                        self.textEdit.append(u'\u26A0' + 'Pll error')
                    else:
                        self.led3.value=False
                    if (status_led[26] != '1'):
                        self.led2.value=True
                        self.textEdit.append(u'\u26A0' + 'PCIe express link lost or could not be established')
                    else:
                        self.led2.value=False
                    if (status_led[27] != '1'):
                        self.led1.value=True
                        self.textEdit.append(u'\u26A0' + 'DDR not ready')
                    else:
                        self.led1.value=False
                return




        with open('kalypso_registers.json') as registers_file:
            self.registers = json.load(registers_file)

        with open('kalypso_log_conf.json') as log_conf_file:
            self.log_conf = json.load(log_conf_file)

        # BUTTONS ACTIONS
        self.spinBox_int_time.valueChanged.connect(lambda: write_int_time_value())
        #self.spinBox_int_delay.valueChanged.connect(lambda: write_int_delay_value())
        #self.spinBox_gott_gain.valueChanged.connect(lambda: write_reg("INTEG_DELAY",self.spinBox_int_delay.text())) # removed 10/06/2021
	self.spinBox_int_delay.valueChanged.connect(lambda: write_reg("INTEG_DELAY",self.spinBox_int_delay.text())) # added 10/06/2021
        self.spinBox_gott_gain.valueChanged.connect(lambda: write_reg("GOTT_GAIN",self.spinBox_gott_gain.text()))
        self.spinBox_total_samples.valueChanged.connect(lambda: write_reg("FT_TO_ACQ",self.spinBox_total_samples.text()))
        self.spinBox_skip_samples.valueChanged.connect(lambda: write_reg("FT_SKIP",self.spinBox_skip_samples.text()))
        self.spinBox_int_trig.valueChanged.connect(lambda: write_reg("CLK_DIV",self.spinBox_int_trig.text()))
        self.spinBox_jesd_delay.valueChanged.connect(lambda: write_reg("JESD_DELAY",self.spinBox_jesd_delay.text()))


        self.pushButton_phase_swap.clicked.connect(phase_swap)

        self.pushButton_file_path.clicked.connect(getDir)
        self.pushButton_background_file.clicked.connect(get_background)
        self.pushButton_modulated_file.clicked.connect(get_modulated)
        self.pushButton_unmodulated_file.clicked.connect(get_unmodulated)
        self.pushButton_readsettings.clicked.connect(readback_settings)
        self.pushButton_init_board.clicked.connect(init_board)
        self.pushButton_init_board_ex_rf.clicked.connect(init_board_ex_rf)
        self.pushButton_timescan.clicked.connect(timescan)
        self.pushButton_poweroff.clicked.connect(poweroff)
        self.pushButton_modulated.clicked.connect(readout_modulated)
        self.pushButton_background.clicked.connect(readout_background)
        self.pushButton_unmodulated.clicked.connect(readout_unmodulated)

        #self.pushButton_custom_function_2.clicked.connect(custom_function_2)

        #timer
        self.timer = pg.QtCore.QTimer()
        #self.timer.timeout.connect(plot_waveform)
        self.timer.timeout.connect(terminal_cmd_run)
        #self.timer.timeout.connect(terminal_cmd_checkbox)
        self.timer.start(10)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        ## Readback settings from FPGA at beginning

        try:
            if (read_register("PCIE_RESET",hex=True) != "14021700"):
                self.status_bar.showMessage('Error!', 1000)
                self.textEdit.insertPlainText(u'\u26A0' + ' Communication failed with FPGA board... Check FPGA, the PCI-Express connection and reboot')
            else:
                readback_settings()
        except:
            self.status_bar.showMessage('Error!', 1000)
            self.textEdit.insertPlainText(u'\u26A0' + ' Communication failed with FPGA board... Check FPGA, the PCI-Express connection and reboot')




    def retranslateUi(self, MainWindow):
        boldFont=QtGui.QFont()
        boldFont.setBold(True)
        MainWindow.setWindowTitle(_translate("MainWindow", "KALYPSO-v2 GUI", None))
        #self.pushButton_custom_function_2.setText(_translate("MainWindow", "custom_function_2", None))
        self.label_settings.setText(_translate("MainWindow", "KALYPSO settings", None))
        self.pushButton_readsettings.setText(_translate("MainWindow", "Read-back", None))
        self.label_settings.setFont(boldFont)
        self.adc_label_settings.setText(_translate("MainWindow", "ADC settings", None))
        self.adc_label_settings.setFont(boldFont)

        self.label_int_time.setText(_translate("MainWindow", "Integration Time (ns)", None))
        self.label_int_delay.setText(_translate("MainWindow", "Integration Delay", None))
        self.label_gott_gain.setText(_translate("MainWindow", "GOTTHARD Gain", None))
        self.label_disp_settings.setText(_translate("MainWindow", "Display settings", None))
        self.label_disp_settings.setFont(boldFont)
        self.label_pix_min.setText(_translate("MainWindow", "Pixel Range", None))
        self.label_samples.setText(_translate("MainWindow", "Total samples", None))
        self.spinbox_pix_min.setValue(0)
        self.spinbox_pix_max.setValue(511)
        self.label_skip_samples.setText(_translate("MainWindow", "Fast-trigger skip", None))
        # self.label_int_trig_text.setText(_translate("MainWindow", "Internal trigger (0 = external trigger)", None))
        # self.label_int_trig_text.setFont(boldFont)

        self.label_jesd_delay.setText(_translate("MainWindow", "JESD delay", None))
        self.pushButton_phase_swap.setText(_translate("MainWindow", "Phase swap", None))
        self.label_int_trig.setText(_translate("MainWindow", "Internal trig. DIV (0 = ext)", None))
        self.label_controls.setText(_translate("MainWindow", "KALYPSO operation", None))
        self.label_controls.setFont(boldFont)
        self.pushButton_init_board.setText(_translate("MainWindow", "Internal CLK", None))
        self.pushButton_init_board_ex_rf.setText(_translate("MainWindow", "External CLK", None))


        self.pushButton_timescan.setText(_translate("MainWindow", "Timescan", None))
        self.pushButton_poweroff.setText(_translate("MainWindow", "Poweroff", None))
        self.label_loadfile.setText(_translate("MainWindow", "Load from file", None))
        self.label_loadfile.setFont(boldFont)
        self.pushButton_background_file.setText(_translate("MainWindow", "Background", None))
        self.pushButton_modulated_file.setText(_translate("MainWindow", "Modulated", None))
        self.pushButton_unmodulated_file.setText(_translate("MainWindow", "Unmodulated", None))

        self.label_acq.setText(_translate("MainWindow", "Acquire data", None))
        self.label_acq.setFont(boldFont)
        ##self.checkBox_cont_acq.setText(_translate("MainWindow", "Live preview", None))
        # self.checkBox_ELOG.setText(_translate("MainWindow", "Create ELOG entry", None))
        self.checkBox_savefile.setText(_translate("MainWindow", "Save File", None))
        self.pushButton_modulated.setText(_translate("MainWindow", "Modulated", None))
        self.pushButton_background.setText(_translate("MainWindow", "Background", None))
        self.pushButton_unmodulated.setText(_translate("MainWindow", "Unmodulated", None))

        self.checkBox_calc_modul.setText(_translate("MainWindow", "Calculate Mod.", None))
        self.checkBox_remove_back.setText(_translate("MainWindow", "Remove background", None))
        self.checkBox_average.setText(_translate("MainWindow", "Show average", None))
        self.pushButton_file_path.setText(_translate("MainWindow", "File Path", None))
        self.lineEdit_file_path.setText(_translate("MainWindow", "./temp/", None))



        self.label_led1.setText(_translate("MainWindow", "DDR ready", None))
        self.label_led1.setFont(boldFont)
        self.label_led2.setText(_translate("MainWindow", "PCIe link", None))
        self.label_led2.setFont(boldFont)
        self.label_led3.setText(_translate("MainWindow", "PLL Gotthard TOP", None))
        self.label_led3.setFont(boldFont)
        self.label_led4.setText(_translate("MainWindow", "PLL Gotthard BOTTOM", None))
        self.label_led4.setFont(boldFont)
        self.label_led5.setText(_translate("MainWindow", "PLL Infrastructure FPGA", None))
        self.label_led5.setFont(boldFont)
        self.label_led6.setText(_translate("MainWindow", "PLL on-board Kalypso", None))
        self.label_led6.setFont(boldFont)
        self.label_led7.setText(_translate("MainWindow", "ADC TOP", None))
        self.label_led7.setFont(boldFont)
        self.label_led8.setText(_translate("MainWindow", "ADC BOTTOM", None))
        self.label_led8.setFont(boldFont)
        self.label_led9.setText(_translate("MainWindow", "JESD TOP", None))
        self.label_led9.setFont(boldFont)
        self.label_led10.setText(_translate("MainWindow", "JESD BOTTOM", None))
        self.label_led10.setFont(boldFont)
        self.label_led11.setText(_translate("MainWindow", "JESD TOP ERROR", None))
        self.label_led11.setFont(boldFont)
        self.label_led12.setText(_translate("MainWindow", "JESD BOTTOM ERROR", None))
        self.label_led12.setFont(boldFont)

        self.label_log.setText(_translate("MainWindow", "KALYPSO status", None))
        self.label_log.setFont(boldFont)

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    app.setStyle('cleanlooks')
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)

    MainWindow.show()
    sys.exit(app.exec_())
