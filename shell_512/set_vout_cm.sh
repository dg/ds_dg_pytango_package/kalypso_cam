#!/bin/sh


#echo "ADC & GOT: Turning on power..."
#pci -w 9048 f0
#sleep 0.1

echo "***Configuring DACs***"

#I2C_DAC_0 = h9068
#I2C_DAC_1 = h906C
#I2C_DAC_2 = h9070
#I2C_DAC_3 = h9074
#I2C_DAC_4 = h9078
#I2C_DAC_5 = h907C



echo "DAC2: CH0 = VOUT_CM "
pci -w 9070 000$1
sleep 0.5

echo "DAC2: CH2 = VOUT_CM "
pci -w 9070 020$1
sleep 0.5

echo "DAC5: CH0 = VOUT_CM "
pci -w 907C 000$1
sleep 0.5

echo "DAC5: CH2 = VOUT_CM "
pci -w 907C 020$1
sleep 0.5
