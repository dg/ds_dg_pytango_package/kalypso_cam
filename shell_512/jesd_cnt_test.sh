#! /bin/bash
export PCILIB_MODEL=ipedma

echo "write the number of samples to be acquired"
#pci -w 9108 0x1000

pci -w 0x9040 0x10210f00
sleep 1

echo "Set the HEADER + ENABLE Readout to DDR"
pci -w 0x9040 0x18310000
sleep 0.01


echo "ENABLE Test pattern and start the acquisition"
pci -w 0x9040 0x18210F00
echo "DISABLE Test pattern"
pci -w 0x9040 0x10210F00
sleep 0.01

echo "*DMA: Reading data..."
./read.sh
#pci -r dma0 --multipacket -o test_data.out --timeout=1000000

#./status.sh

#./dumpFile.sh bench.out
