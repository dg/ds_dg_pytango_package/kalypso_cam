#! /bin/bash

# bit number 5 is enable the PRBS_EN
echo "ADC1: REG ADD 3"
pci -w 0x90A0 0x00038120                 #01 is 14bit mode, 000 is 12x, 001 is 14x, 100 is 16x, 011 is 10x
sleep 0.05

#####################################################################################################################################

echo "ADC1: REG ADD D"
pci -w 0x90A0 0x000D00ff                #Offset value
sleep 0.05

echo "ADC1: REG ADD E"
pci -w 0x90A0 0x000E00ff               #Offset value
sleep 0.05

echo "ADC1: REG ADD F"
pci -w 0x90A0 0x000F00ff                #Offset value
sleep 0.05

echo "ADC1: REG ADD 10"
pci -w 0x90A0 0x001000ff                #Offset value
sleep 0.05

echo "ADC1: REG ADD 11"
pci -w 0x90A0 0x001100ff                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 12"
pci -w 0x90A0 0x001200ff                #Offset value
sleep 0.05

echo "ADC1: REG ADD 13"
pci -w 0x90A0 0x001300ff                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 14"
pci -w 0x90A0 0x001400ff                #Offset value
sleep 0.05

echo "ADC1: REG ADD 19"
pci -w 0x90A0 0x001900ff              #Offset value
sleep 0.05

echo "ADC1: REG ADD 1A"
pci -w 0x90A0 0x001A00ff                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 1B"
pci -w 0x90A0 0x001B00ff                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 1C"
pci -w 0x90A0 0x001C00ff                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 1D"
pci -w 0x90A0 0x001D00ff                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 1E"
pci -w 0x90A0 0x001E00ff                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 1F"
pci -w 0x90A0 0x001F00ff                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 20"
pci -w 0x90A0 0x002000ff                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 25"
pci -w 0x90A0 0x002500ff                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 26"
pci -w 0x90A0 0x002600ff                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 27"
pci -w 0x90A0 0x0027010f                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 28"
pci -w 0x90A0 0x0028010f                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 29"
pci -w 0x90A0 0x002900ff                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 2A"
pci -w 0x90A0 0x002A00ff                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 2B"
pci -w 0x90A0 0x002B00ff                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 2C"
pci -w 0x90A0 0x002C00ff                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 31"
pci -w 0x90A0 0x003100ff                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 32"
pci -w 0x90A0 0x003200ff                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 33"
pci -w 0x90A0 0x003300ff                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 34"
pci -w 0x90A0 0x003400ff                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 35"
pci -w 0x90A0 0x003500ff                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 36"
pci -w 0x90A0 0x003600ff                 #Offset value
sleep 0.05

echo "ADC1: REG ADD 37"
pci -w 0x90A0 0x003700ff               #Offset value
sleep 0.05

echo "ADC1: REG ADD 38"
pci -w 0x90A0 0x003800ff                 #Offset value
sleep 0.05
##########################################################################################################

#####################################################################################################################################
pci -w 0x90A4 0x00038120                 #01 is 14bit mode, 000 is 12x, 001 is 14x, 100 is 16x, 011 is 10x
sleep 0.05

echo "ADC2: REG ADD D"
pci -w 0x90A4 0x000D00df                #Offset value
sleep 0.05

echo "ADC2: REG ADD E"
pci -w 0x90A4 0x000E00df                #Offset value
sleep 0.05

echo "ADC2: REG ADD F"
pci -w 0x90A4 0x000F007f               #Offset value
sleep 0.05

echo "ADC2: REG ADD 10"
pci -w 0x90A4 0x0010007f                #Offset value
sleep 0.05

echo "ADC2: REG ADD 11"
pci -w 0x90A4 0x0011007f                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 12"
pci -w 0x90A4 0x0012007f                #Offset value
sleep 0.05

echo "ADC2: REG ADD 13"
pci -w 0x90A4 0x0013007f                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 14"
pci -w 0x90A4 0x0014007f                #Offset value
sleep 0.05

echo "ADC2: REG ADD 19"
pci -w 0x90A4 0x0019007f              #Offset value
sleep 0.05

echo "ADC2: REG ADD 1A"
pci -w 0x90A4 0x001A007f                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 1B"
pci -w 0x90A4 0x001B007f                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 1C"
pci -w 0x90A4 0x001C007f                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 1D"
pci -w 0x90A4 0x001D007f                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 1E"
pci -w 0x90A4 0x001E007f                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 1F"
pci -w 0x90A4 0x001F007f                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 20"
pci -w 0x90A4 0x0020007f                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 25"
pci -w 0x90A4 0x002500df                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 26"
pci -w 0x90A4 0x002600df                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 27"
pci -w 0x90A4 0x002700df                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 28"
pci -w 0x90A4 0x002800df                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 29"
pci -w 0x90A4 0x002900df                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 2A"
pci -w 0x90A4 0x002A00df                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 2B"
pci -w 0x90A4 0x002B007f                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 2C"
pci -w 0x90A4 0x002C007f                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 31"
pci -w 0x90A4 0x0031007f                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 32"
pci -w 0x90A4 0x0032007f                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 33"
pci -w 0x90A4 0x0033007f                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 34"
pci -w 0x90A4 0x0034007f                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 35"
pci -w 0x90A4 0x0035007f                #Offset value
sleep 0.05

echo "ADC2: REG ADD 36"
pci -w 0x90A4 0x0036007f                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 37"
pci -w 0x90A4 0x0037007f                 #Offset value
sleep 0.05

echo "ADC2: REG ADD 38"
pci -w 0x90A4 0x0038007f                 #Offset value
sleep 0.05
