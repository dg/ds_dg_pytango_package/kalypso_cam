###### Read in data from KALYPSO
#from docopt import docopt

import numpy as np
#import matplotlib.pyplot as plt
#from mpl_toolkits.axes_grid1 import make_axes_locatable
#import matplotlib as mpl
#import strip_data
import os

NUMBER_PIXELS = 256
DDR_FILLING   = [0xba98,0xfedc]
HEADER_0      = 0xABBA
short_pattern = [0,1,2,3,3,2,1,0]
long_pattern = np.repeat(short_pattern, 32)



#####################################################################################
## strip_data (filename, number of pixels, orbits[opt.])
## return: data_array_stripped,   data_consistency_is_ok,   orbits
#####################################################################################

#####################################################################################
def strip_data(filename, number_pixels, orbits, offset = None):

#    np.set_printoptions(threshold='nan')
     # data = np.fromfile(str(filename), dtype=np.uint16)
     if     offset is None:
         data = np.memmap( str(filename), dtype=np.dtype('<u2'), mode='r')
     else:
         data = np.memmap( str(filename), dtype=np.dtype('<u2'), offset = (2*NUMBER_PIXELS*offset),mode='r')
	
     print(data.shape)


     if (orbits):
         data = data[0:orbits*number_pixels]
          
     print(data.shape)

#####################################################################################
     ### Remove Header
#####################################################################################
     if (data[0] == 43962):
         print("Removing header...")
         data = data[16:]
         data = np.append(data,[DDR_FILLING,DDR_FILLING,DDR_FILLING,DDR_FILLING,DDR_FILLING,DDR_FILLING,DDR_FILLING,DDR_FILLING])

#####################################################################################
     ## Reshape list as an 256*whatever array
#####################################################################################
     data = np.reshape(data, (-1,NUMBER_PIXELS))
     print(data.shape)
     # data = data[0:-8,:]

#####################################################################################
     ### Remove Filling at the end
#####################################################################################

     lines_with_filling = 0

     print("\n***********************")
     print("Removing DDR filling...")
     while (np.all(data[-1:]==DDR_FILLING) or np.all(data[-1:]==0x00000000)):
         data = data[0:-1,:]
         lines_with_filling += 1

     print("Removed %d bytes with DDR filling" % (lines_with_filling*512))
     print(data.shape)

#####################################################################################
     ### Data consistency check
#####################################################################################
     orbits, pixels = np.shape(data)

     high = (data & 0xC000) >> 14
     data = (data & 0x3fff)
     #print (NUMBER_PIXELS)
     #print (data)
     print(data.shape)
     print("\n***********************")
     print("Checking data consistency on %d orbits: " % orbits)

     if (not np.any(high - long_pattern)):
         print("everything is ok!")
         return (data, 1, orbits)
     else:
         print("error in data consistency!")
         #sys.exit("Error in data Consistency!!")
         #print (high - long_pattern)
         #print (np.where((high - long_pattern) != 0)[0])
         return (data, 1, orbits)





strip_data('f12345_2020-02-12T11h45m01s_Si01_m.bin',256,1000)

