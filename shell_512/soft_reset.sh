#! /bin/bash
export PCILIB_MODEL=ipedma

echo "RESET of the KALYPSO"
pci -w 9040 0x200010
sleep 0.1

echo "DE-RESET of the KALYPSO"
pci -w 9040 0x200000
sleep 0.1
#./status.sh
