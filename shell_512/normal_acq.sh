#! /bin/bash
export PCILIB_MODEL=ipedma
#!/bin/sh
# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Initialize our own variables:
output_file="temp.bin"

while getopts "f:" opt; do
    case "$opt" in
    f)  output_file=$OPTARG
        ;;
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift

echo "output_file='$output_file'"

rm $output_file



echo "Number of acquisitions to acquire (Slow Trig.)= 1"
pci -w 0x9024 0x1
sleep 0.0001

echo "Number of acquisitions to skip (Slow Trig.)= 0"
pci -w 0x902C 0x0
sleep 0.0001

#echo "Number of frame to acquire (Fast Trig.)= 1024"
#pci -w 0x9020 0x800
#sleep 0.0001

pci -w 0x9024 0x0
sleep 0.0001

echo "ADD Reset DDR"
pci -w 0x9040 0x10210F50
sleep 0.001

pci -w 0x9040 0x10210F00
sleep 0.001

echo "Start Normal acquisition + ENABLE Readout to DDR + HEADER"
pci -w 0x9040 0x1021FF00
sleep 0.001


echo "*DMA: Reading data..."
pci -r dma0 --multipacket -o $output_file --timeout=1000000

#./status.sh
echo "IDLE"
pci -w 0x9040 0x10210F00
sleep 0.001
#./status.sh


#pci -r dma0 --multipacket -o test_data.out --timeout=1000000

#./status.sh

#./dumpFile.sh bench.out
