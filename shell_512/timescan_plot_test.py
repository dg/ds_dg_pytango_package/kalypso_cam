import numpy as np
import matplotlib.pyplot as plt
import strip_data
import os
import os.path

NUMBER_PIXELS = 512

def timescan_plot(filename, orbits, settings):

    data, orb  = strip_data.strip_data(filename, NUMBER_PIXELS, 0)

    if os.path.isfile('temp_bg.bin'):
        bg_data , orb_bg  = strip_data.strip_data('temp_bg.bin', NUMBER_PIXELS, 0)
        if (orb_bg):
            bg_avg = np.average(bg_data,axis = 0)
            data = bg_avg - data
        else:
            data = data



    data = np.reshape(data, (-1,orbits,NUMBER_PIXELS))

    data = np.average(data, axis=1)


    data = np.reshape(data, (-1,NUMBER_PIXELS))

    data = data[1::4,:]
    

    fig, ax = plt.subplots()
    im = ax.imshow(data, cmap=plt.cm.hot, aspect=0.5, interpolation='none', extent=[0,NUMBER_PIXELS,settings,0])

    fig.colorbar(im, ax=ax)
    plt.ylabel('Delay setting')
    plt.xlabel('Pixels')

    plt.tight_layout()

    plt.show(block=True)


timescan_plot('timescan_data.out', 128 ,1023)
