#!/bin/sh


#echo "ADC & GOT: Turning on power..."
#pci -w 9048 f0
#sleep 0.1

pci -w 9048 ff
sleep 0.1


echo "***Configuring DACs***"

#I2C_DAC_0 = h9068
#I2C_DAC_1 = h906C
#I2C_DAC_2 = h9070
#I2C_DAC_3 = h9074
#I2C_DAC_4 = h9078
#I2C_DAC_5 = h907C



echo "DAC3: CH0 = VB_COLBUFFER 29uA"
pci -w 9074 000800
sleep 0.1

echo "DAC3: CH1 = IB_DS 28uA"
pci -w 9074 010690
sleep 0.1

################# common for gotthard 4 and 5 #############################


echo "DAC5: CH0 = VOUT_CM "
pci -w 907C 0005DC
sleep 0.1

echo "DAC5: CH1 = VIN_CM 560mV"
pci -w 907C 010460
sleep 0.1


echo "....done!****"
