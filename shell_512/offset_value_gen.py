import numpy as np
import sys
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy import signal
import scipy.integrate as spi
from scipy import asarray as ar, exp, sqrt
from scipy.optimize import curve_fit
#from astropy import modeling


pixels = 512*2
TAIL = [0xba98,0xfedc]*16
DEAD = [0xdea1,0xdead,0xdea2,0xdead,0xdea3,0xdead,0xdea4,0xdead,0xdea5,0xdead,0xdea6,0xdead,0xdea7,0xdead,0xdea8,0xdead]

def strip_data(filename, number_pixels, orbits, offset = None):
        global data

        ###############----------Read data from memory----------############################################
        np.set_printoptions(threshold='nan')
        data = np.fromfile(str(filename), dtype=np.uint16)
        #if offset is None:
        # data = np.memmap( str(filename), dtype=np.dtype('<u2'), mode='r')
        #else:
        #    data = np.memmap( str(filename), dtype=np.dtype('<u2'), offset = (2*pixels*offset),mode='r')


        ###############----------Remove Header----------####################################################

        if (data[0] == 0x1111 and data[15] == 0xF888):
            print ('Removing header .....')
            data = data[16:]
        else:
            print ('No data header....')
            data = data

        ##############--------Search and remove for TAIL--------############################################
        offset = 0
        tailoffset = offset
        while np.any((data[tailoffset : tailoffset + len(TAIL)] != TAIL)):
            tailoffset += 8
            if tailoffset + len(TAIL) > len(data):
                print('Could not find tail')
                sys.exit(-1)
        print('Found tail at offset ' + str(tailoffset))

        ##############--------Search and remove for DEAD filling--------##################################
        deadcnt = 0
        deadoffset = tailoffset
        while np.any(data[deadoffset - len(DEAD) : deadoffset] == DEAD):
            deadcnt +=1
            deadoffset -= len(DEAD)

        print('Removed %d bytes of DEAD' %(deadcnt))

        ##############--------Set the pure data --------###################################################

        data = np.frombuffer((data[offset:deadoffset]), dtype = np.uint16)

        #dead = np.where(data == 0xdea1)
        #loc = np.amin(dead)
        #data = data[0:loc]

        ############-------Swap the data bytes and change from 16 bit to 14 bit---------####################

        data = data.byteswap()
        data = data >> 2

        #############---------Index decoder for the jesd data for 16 input mode----------###################
        odd_inner_idx_1 = [23,22,7,6,21,20,5,4,19,18,3,2,17,16,1,0] # working one
        odd_inner_idx_2 = [8,9,24,25,10,11,26,27,12,13,28,29,14,15,30,31]    # non interleaved

        odd_index_1 = np.zeros([256], dtype=np.int64)
        odd_index_2 = np.zeros([256], dtype=np.int64)
        for i in range(16):
            for j in range(16):
                odd_index_1[i * 16 + j] = odd_inner_idx_1[i] +  j * 64

        for i in range(16):
            for j in range(16):
                odd_index_2[i * 16 + j] = odd_inner_idx_2[i] +  j * 64


        even_inner_idx_1 = [55,54,39,38,53,52,37,36,51,50,35,34,49,48,33,32] # working one
        even_inner_idx_2 = [40,41,56,57,42,43,58,59,44,45,60,61,46,47,62,63]    # non interleaved

        even_index_1 = np.zeros([256], dtype=np.int64)
        even_index_2 = np.zeros([256], dtype=np.int64)
        for i in range(16):
            for j in range(16):
                even_index_1[i * 16 + j] = even_inner_idx_1[i] +  j * 64

        for i in range(16):
            for j in range(16):
                even_index_2[i * 16 + j] = even_inner_idx_2[i] +  j * 64


        ################------working part---------###########################################################
        #index_interleave = np.ravel(np.column_stack((index_1,index_2)))  #interleaving the pixels, NOTE: interchange index_1 & index_2 if the pixels are swapped
        #k = 32
        #pixel_map = []
        #a = index_interleave

        #for i in range(0, len(a), k):
        #    pixel_map.extend((a[i:i + k])[::-1])
        ######################################################################################################
        l = 16
        odd_adc_2 = []
        odd_b = odd_index_2
        for i in range(0, len(odd_b), l):
            odd_adc_2.extend((odd_b[i:i + l])[::-1])

        even_adc_2 = []
        even_b = even_index_2
        for i in range(0, len(even_b), l):
            even_adc_2.extend((even_b[i:i + l])[::-1])

        #index_interleave = np.concatenate((odd_index_1,odd_adc_2),axis = 0)    # for concatenated pixels
        #odd_index_interleave = np.concatenate((odd_index_1,odd_adc_2),axis = 0)    # for concatenated pixels
        #even_index_interleave = np.concatenate((even_index_1,even_adc_2),axis = 0)    # for concatenated pixels

        #odd_index_interleave = np.ravel(np.column_stack((odd_index_1,odd_adc_2)))    # for interleaved pixels
        #even_index_interleave = np.ravel(np.column_stack((even_index_1,even_adc_2)))    # for interleaved pixels

        #splitting the even part of the ADC since on the sensor they belong to the outer 256 x 2
        #even_index_interleave_left = even_index_interleave[0:256]
        #even_index_interleave_right = even_index_interleave[256:512]

        #splitting the even part of the ADC since on the sensor they belong to the outer 256 x 2
        odd_left_1 = odd_index_1[0:128]
        odd_right_1 = odd_index_1[128:256]

        odd_left_2 = odd_adc_2[0:128]
        odd_right_2 = odd_adc_2[128:256]

        #final_1024_index = np.concatenate((even_index_interleave_left,odd_index_interleave,even_index_interleave_right),axis = 0)
        #######################
        final_1024_index = np.concatenate((odd_left_1,even_index_1,odd_right_1,odd_left_2,even_adc_2,odd_right_2),axis = 0)  # for concatenated pixels

        ##############-------------Reshape the data as pixel x orbits-------------------###################
        if (orbits):
            data = data[0:orbits*512*2]

        data = np.reshape(data,(-1,pixels))

        data = data[:,final_1024_index]

        orb, pix = np.shape(data)

        return (data, orb)





strip_data('soleil_card_offset_1024.bin',1024,1)
np.set_printoptions(formatter={'int':hex})

pixel_num = np.arange(1,513,1)
modulated_data = data.astype(np.uint16)
orbits, pixels = np.shape(modulated_data)
ad = np.transpose(modulated_data)
offset_to = 470
offset_value = ad - offset_to




offset_value = int(offset_value)

['{:04x}'.format(i) for i in offset_value]

#map('{:04x}'.format(offset_value))

#'{:04x}'.format(offset_value)



#'{:032b}'.format(status_led)

max_ad = np.max(ad)
print (max_ad)
min_ad = np.min(ad)
print (min_ad)


x = 16



print('ADC1 0x90A0')
###########
#
#print('pci -w 0x90A0  0x0038'+'0'+'{:04x}'.format(hex(offset_value[4,:]))[3:-1])


print('pci -w 0x90A0  0x0038'+'0'+str(offset_value[4,:])[3:-1])  #D/E
print('pci -w 0x90A0  0x0036'+'0'+str(offset_value[4+x,:])[3:-1])  #F/10
print('pci -w 0x90A0  0x0034'+'0'+str(offset_value[4+2*x,:])[3:-1])  #11/12
print('pci -w 0x90A0  0x0032'+'0'+str(offset_value[4+3*x,:])[3:-1])  #13/14

print('pci -w 0x90A0  0x002C'+'0'+str(offset_value[4+4*x,:])[3:-1])  #19/1A
print('pci -w 0x90A0  0x002A'+'0'+str(offset_value[4+5*x,:])[3:-1])  #1B/1C
print('pci -w 0x90A0  0x0028'+'0'+str(offset_value[4+6*x,:])[3:-1]) #1D/1E
print('pci -w 0x90A0  0x0026'+'0'+str(offset_value[4+7*x,:])[3:-1]) #1F/20
###########
print('pci -w 0x90A0  0x0037'+'0'+str(offset_value[4+8*x,:])[3:-1])  #D/E
print('pci -w 0x90A0  0x0035'+'0'+str(offset_value[4+9*x,:])[3:-1])  #F/10
print('pci -w 0x90A0  0x0033'+'0'+str(offset_value[4+10*x,:])[3:-1])  #11/12
print('pci -w 0x90A0  0x0031'+'0'+str(offset_value[4+11*x,:])[3:-1])  #13/14

print('pci -w 0x90A0  0x002B'+'0'+str(offset_value[4+12*x,:])[3:-1])  #19/1A
print('pci -w 0x90A0  0x0029'+'0'+str(offset_value[4+13*x,:])[3:-1])  #1B/1C
print('pci -w 0x90A0  0x0027'+'0'+str(offset_value[4+14*x,:])[3:-1]) #1D/1E
print('pci -w 0x90A0  0x0025'+'0'+str(offset_value[4+15*x,:])[3:-1]) #1F/20

print('pci -w 0x90A0  0x001F'+'0'+str(offset_value[4+16*x,:])[3:-1]) #25/26
print('pci -w 0x90A0  0x001D'+'0'+str(offset_value[4+17*x,:])[3:-1]) #27/28
print('pci -w 0x90A0  0x001B'+'0'+str(offset_value[4+18*x,:])[3:-1]) #29/2A
print('pci -w 0x90A0  0x0019'+'0'+str(offset_value[4+19*x,:])[3:-1]) #2B/2C

print('pci -w 0x90A0  0x0013'+'0'+str(offset_value[4+20*x,:])[3:-1]) #31/32
print('pci -w 0x90A0  0x0011'+'0'+str(offset_value[4+21*x,:])[3:-1]) #33/34
print('pci -w 0x90A0  0x000F'+'0'+str(offset_value[4+22*x,:])[3:-1]) #35/36
print('pci -w 0x90A0  0x000D'+'0'+str(offset_value[4+23*x,:])[3:-1]) #37/38
###########
print('pci -w 0x90A0  0x0020'+'0'+str(offset_value[4+24*x,:])[3:-1]) #25/26
print('pci -w 0x90A0  0x001E'+'0'+str(offset_value[4+25*x,:])[3:-1]) #27/28
print('pci -w 0x90A0  0x001C'+'0'+str(offset_value[4+26*x,:])[3:-1]) #29/2A
print('pci -w 0x90A0  0x001A'+'0'+str(offset_value[4+27*x,:])[3:-1]) #2B/2C

print('pci -w 0x90A0  0x0014'+'0'+str(offset_value[4+28*x,:])[3:-1]) #31/32
print('pci -w 0x90A0  0x0012'+'0'+str(offset_value[4+29*x,:])[3:-1]) #33/34
print('pci -w 0x90A0  0x0010'+'0'+str(offset_value[4+30*x,:])[3:-1]) #35/36
print('pci -w 0x90A0  0x000E'+'0'+str(offset_value[4+31*x,:])[3:-1]) #37/38
###################################################################
print('ADC2 0x90A4')

print('pci -w 0x90A4  0x000E'+'0'+str(offset_value[4+32*x,:])[3:-1]) #D/E
print('pci -w 0x90A4  0x0010'+'0'+str(offset_value[4+33*x,:])[3:-1]) #F/10
print('pci -w 0x90A4  0x0012'+'0'+str(offset_value[4+34*x,:])[3:-1]) #11/12
print('pci -w 0x90A4  0x0014'+'0'+str(offset_value[4+35*x,:])[3:-1]) #13/14

print('pci -w 0x90A4  0x001A'+'0'+str(offset_value[4+36*x,:])[3:-1]) #19/1A
print('pci -w 0x90A4  0x001C'+'0'+str(offset_value[4+37*x,:])[3:-1]) #1B/1C
print('pci -w 0x90A4  0x001E'+'0'+str(offset_value[4+38*x,:])[3:-1]) #1D/1E
print('pci -w 0x90A4  0x0020'+'0'+str(offset_value[4+39*x,:])[3:-1]) #1F/20

print('pci -w 0x90A4  0x000D'+'0'+str(offset_value[4+40*x,:])[3:-1]) #D/E
print('pci -w 0x90A4  0x000F'+'0'+str(offset_value[4+41*x,:])[3:-1]) #F/10
print('pci -w 0x90A4  0x0011'+'0'+str(offset_value[4+42*x,:])[3:-1]) #11/12
print('pci -w 0x90A4  0x0013'+'0'+str(offset_value[4+43*x,:])[3:-1]) #13/14

print('pci -w 0x90A4  0x0019'+'0'+str(offset_value[4+44*x,:])[3:-1]) #19/1A
print('pci -w 0x90A4  0x001B'+'0'+str(offset_value[4+45*x,:])[3:-1]) #1B/1C
print('pci -w 0x90A4  0x001D'+'0'+str(offset_value[4+46*x,:])[3:-1]) #1D/1E
print('pci -w 0x90A4  0x001F'+'0'+str(offset_value[4+47*x,:])[3:-1]) #1F/20

print('pci -w 0x90A4  0x0025'+'0'+str(offset_value[4+48*x,:])[3:-1]) #25/26
print('pci -w 0x90A4  0x0027'+'0'+str(offset_value[4+49*x,:])[3:-1]) #27/28
print('pci -w 0x90A4  0x0029'+'0'+str(offset_value[4+50*x,:])[3:-1]) #29/2A
print('pci -w 0x90A4  0x002B'+'0'+str(offset_value[4+51*x,:])[3:-1]) #2B/2C

print('pci -w 0x90A4  0x0031'+'0'+str(offset_value[4+52*x,:])[3:-1]) #31/32
print('pci -w 0x90A4  0x0033'+'0'+str(offset_value[4+53*x,:])[3:-1]) #33/34
print('pci -w 0x90A4  0x0035'+'0'+str(offset_value[4+54*x,:])[3:-1]) #35/36
print('pci -w 0x90A4  0x0037'+'0'+str(offset_value[4+55*x,:])[3:-1]) #37/38

print('pci -w 0x90A4  0x0026'+'0'+str(offset_value[4+56*x,:])[3:-1]) #25/26
print('pci -w 0x90A4  0x0028'+'0'+str(offset_value[4+57*x,:])[3:-1]) #27/28
print('pci -w 0x90A4  0x002A'+'0'+str(offset_value[4+58*x,:])[3:-1]) #29/2A
print('pci -w 0x90A4  0x002C'+'0'+str(offset_value[4+59*x,:])[3:-1]) #2B/2C

print('pci -w 0x90A4  0x0032'+'0'+str(offset_value[4+60*x,:])[3:-1]) #31/32
print('pci -w 0x90A4  0x0034'+'0'+str(offset_value[4+61*x,:])[3:-1]) #33/34
print('pci -w 0x90A4  0x0036'+'0'+str(offset_value[4+62*x,:])[3:-1]) #35/36
print('pci -w 0x90A4  0x0038'+'0'+str(offset_value[4+63*x,:])[3:-1]) #37/38
