#!/bin/bash
for add in {1..9};
  do
    echo "CLK SHIFT $add"
    pci -w 9088 FF
    pci -r 9088 -s 1
    sleep 0.1
    pci -w 9088 FE
    pci -r 9088 -s 1
  done
