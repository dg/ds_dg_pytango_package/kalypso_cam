#!/bin/bash

# R0 (INIT)	0x80000100
# R0	0x00070600
# R1	0x00061F01
# R2	0x00065F02
# R3	0x00070603
# R4	0x00070604
# R5	0x00070305
# R6	0x00070306
# R7	0x00065F07
# R8	0x10000908
# R9	0xA0022A09
# R11	0x0082800B
# R13	0x028F800D
# R14	0x0830020E
# R15	0xC800180F




#echo "Remove Board Reset ... "
#pci -w 0x9040 0x00
#sleep 0.5

#while :
#do

echo "*PLL: configuration + calibration start ... "
echo "*PLL: R0 Reset ... "

pci -w 0x9064 0x80000000
sleep 0.2

# R0 (INIT)
pci -w 0x9064 0x80000100
sleep 0.2

#echo "*PLL: R0 "
pci -w 0x9064 0x00070600   # clock enable
#pci -w 0x9064 0x00060600   # clock disable   # OUTPUT 62.5 MHZ to FPGA
sleep 0.2

#echo "*PLL: R3 "
pci -w 0x9064 0x000706f3          # clk_ADC_2    working:0x000704b3
sleep 0.2

#echo "*PLL: R4 "
pci -w 0x9064 0x000706f4          # clk_ADC_1    working:0x000704b3
sleep 0.2

#echo "*PLL: R5 "
pci -w 0x9064 0x00070305          # FPGA_GTHX_CLOCK_0
sleep 0.2

#echo "*PLL: R6 "
pci -w 0x9064 0x00070306          # FPGA_GTHX_CLOCK_1
sleep 0.2

#echo "*PLL: R7 "
pci -w 0x9064 0x00000107            # FPGA_SYS_REF before 0x00035C07
sleep 0.2

echo "*PLL: R8 "
pci -w 0x9064 0x10000908
sleep 0.2

echo "*PLL: R9 No Vboost"
#pci -w 0x9064 0xA0022A09
pci -w 0x9064 0xA0032A09
sleep 0.2

echo "*PLL: RB"
pci -w 0x9064 0x0082800B
#pci -w 0x9064 0x0082000B
sleep 0.2

echo "*PLL: RD (R13)"
pci -w 0x9064 0x028F800D
#pci -w 0x9064 0x028F80AD
sleep 0.2

echo "*PLL: RE"
pci -w 0x9064 0x0830020E
#pci -w 0x9064 0x08000A0E
sleep 0.2

echo "*PLL: RF"
pci -w 0x9064 0xC800180F
#pci -w 0x9064 0xC800780F
sleep 0.2


./pll_sync.sh
sleep 0.1
