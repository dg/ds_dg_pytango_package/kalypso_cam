#! /bin/bash
echo "Removing ADC offset at 0 delay setting"
# bit number 5 is enable the PRBS_EN
#echo "ADC1: REG ADD 3"
pci -w 0x90A0 0x00038120                 #01 is 14bit mode, 000 is 12x, 001 is 14x, 100 is 16x, 011 is 10x
sleep 0.05

#####################################################################################################################################

#echo "ADC1: REG ADD D"
pci -w 0x90A0 0x000D0101                #Offset value
sleep 0.05

#echo "ADC1: REG ADD E"
pci -w 0x90A0 0x000E0101               #Offset value
sleep 0.05

#echo "ADC1: REG ADD F"
pci -w 0x90A0 0x000F00e0                #Offset value
sleep 0.05

#echo "ADC1: REG ADD 10"
pci -w 0x90A0 0x001000e0                #Offset value
sleep 0.05

#echo "ADC1: REG ADD 11"
pci -w 0x90A0 0x001100bc                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 12"
pci -w 0x90A0 0x001200bc                #Offset value
sleep 0.05

#echo "ADC1: REG ADD 13"
pci -w 0x90A0 0x00130091                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 14"
pci -w 0x90A0 0x00140091                #Offset value
sleep 0.05

#echo "ADC1: REG ADD 19"
pci -w 0x90A0 0x001900c8              #Offset value
sleep 0.05

#echo "ADC1: REG ADD 1A"
pci -w 0x90A0 0x001A00c8                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 1B"
pci -w 0x90A0 0x001B00bf                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 1C"
pci -w 0x90A0 0x001C00bf                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 1D"
pci -w 0x90A0 0x001D0085                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 1E"
pci -w 0x90A0 0x001E0085                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 1F"
pci -w 0x90A0 0x001F00f2                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 20"
pci -w 0x90A0 0x002000f2                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 25"
pci -w 0x90A0 0x0025010d                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 26"
pci -w 0x90A0 0x0026010d                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 27"
pci -w 0x90A0 0x002700e8                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 28"
pci -w 0x90A0 0x002800e8                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 29"
pci -w 0x90A0 0x002900b7

#echo "ADC1: REG ADD 2A"
pci -w 0x90A0 0x002A00b7                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 2B"
pci -w 0x90A0 0x002B009b                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 2C"
pci -w 0x90A0 0x002C009b                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 31"
pci -w 0x90A0 0x003100b2                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 32"
pci -w 0x90A0 0x003200b2                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 33"
pci -w 0x90A0 0x003300b6                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 34"
pci -w 0x90A0 0x003400b6                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 35"
pci -w 0x90A0 0x003500da                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 36"
pci -w 0x90A0 0x003600da                #Offset value
sleep 0.05

#echo "ADC1: REG ADD 37"
pci -w 0x90A0 0x00370100               #Offset value
sleep 0.05

#echo "ADC1: REG ADD 38"
pci -w 0x90A0 0x00380100                 #Offset value
sleep 0.05
##########################################################################################################

#####################################################################################################################################
pci -w 0x90A4 0x00038120                 #01 is 14bit mode, 000 is 12x, 001 is 14x, 100 is 16x, 011 is 10x
sleep 0.05

#echo "ADC2: REG ADD D"
pci -w 0x90A4 0x000D012a                #Offset value
sleep 0.05

#echo "ADC2: REG ADD E"
pci -w 0x90A4 0x000E012a                #Offset value
sleep 0.05

#echo "ADC2: REG ADD F"
pci -w 0x90A4 0x000F00a7               #Offset value
sleep 0.05

#echo "ADC2: REG ADD 10"
pci -w 0x90A4 0x001000a7                #Offset value
sleep 0.05

#echo "ADC2: REG ADD 11"
pci -w 0x90A4 0x001100af                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 12"
pci -w 0x90A4 0x001200af                #Offset value
sleep 0.05

#echo "ADC2: REG ADD 13"
pci -w 0x90A4 0x001300a1                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 14"
pci -w 0x90A4 0x001400a1                #Offset value
sleep 0.05

#echo "ADC2: REG ADD 19"
pci -w 0x90A4 0x0019009c              #Offset value
sleep 0.05

#echo "ADC2: REG ADD 1A"
pci -w 0x90A4 0x001A009c                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 1B"
pci -w 0x90A4 0x001B00ae                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 1C"
pci -w 0x90A4 0x001C00ae                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 1D"
pci -w 0x90A4 0x001D00b9                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 1E"
pci -w 0x90A4 0x001E00b9                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 1F"
pci -w 0x90A4 0x001F00bb                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 20"
pci -w 0x90A4 0x002000bb                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 25"
pci -w 0x90A4 0x0025010d                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 26"
pci -w 0x90A4 0x0026010d                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 27"
pci -w 0x90A4 0x002700e3                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 28"
pci -w 0x90A4 0x002800e3                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 29"
pci -w 0x90A4 0x002900bd                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 2A"
pci -w 0x90A4 0x002A00bd                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 2B"
pci -w 0x90A4 0x002B007f                #Offset value
sleep 0.05

#echo "ADC2: REG ADD 2C"
pci -w 0x90A4 0x002C007f                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 31"
pci -w 0x90A4 0x00310079                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 32"
pci -w 0x90A4 0x00320079                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 33"
pci -w 0x90A4 0x00330071                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 34"
pci -w 0x90A4 0x00340071                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 35"
pci -w 0x90A4 0x00350085                #Offset value
sleep 0.05

#echo "ADC2: REG ADD 36"
pci -w 0x90A4 0x00360085                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 37"
pci -w 0x90A4 0x003700bf                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 38"
pci -w 0x90A4 0x003800bf                 #Offset value
sleep 0.05
