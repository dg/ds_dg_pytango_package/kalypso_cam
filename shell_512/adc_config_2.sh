#! /bin/bash

echo "*** ADC  ADS52j90 ***"
echo "*** Start Configuration of ADC # 1 ***"
echo "       Sub Class 2      "
echo "*** Start INIT after Power-ON ***"
pci -w 0x90A0 0x000A3000
sleep 1

#echo "ADC1: Soft Reset"
pci -w 0x90A0 0x00000001
sleep 1

#echo "ADC1: enable write"
pci -w 0x90A0 0x00000000
sleep 1
## sequences start here #

#echo "ADC1: REG ADD 1"
pci -w 0x90A0 0x00010074
sleep 1

#echo "TEST PATTER IS 1234"
#pci -w 0x90A0 0x00051234
#pci -w 0x90A0 0x00051234   # all 1s test mode is 2, all 0s is 3
#sleep 1

#echo "ADC1: REG ADD 2 --> Normal MODE' "
pci -w 0x90A0 0x00020000  # all 1s test mode is 2, all 0s is 3
sleep 1

# bit number 5 is enable the PRBS_EN
#echo "ADC1: REG ADD 3"
pci -w 0x90A0 0x00038020                 #01 is 14bit mode, 000 is 12x, 001 is 14x, 100 is 16x, 011 is 10x
sleep 1

#echo "ADC1: REG ADD 4"
pci -w 0x90A0 0x00040001                 #01 is 14bit mode, 00 is 12 bit mode and 11 is 10bit
sleep 1

echo "Start JESD settings ADC # 1"

 #####-------changing K value-----#####
 # enable force K
 # with frame alignment option #
#echo "ADC: REG ADD 49 --> to force K value"
#pci -w 0x90A0 0x00491804
#sleep 1

#echo "ADC1: REG ADD 49 --> without FRAME ALIG. to force K value"
pci -w 0x90A0 0x00490004
sleep 1

#echo "ADC1: REG ADD 4B"
pci -w 0x90A0 0x004B0100
sleep 1

#  force K to 5+1 = 6
#echo "ADC1: REG ADD 53 --> K value force to F"
pci -w 0x90A0 0x0053000F
sleep 1

#echo "ADC1: REG ADD 55 Sub Class 2 - here"
pci -w 0x90A0 0x00554020
sleep 1

#echo "ADC1: REG ADD 73"
pci -w 0x90A0 0x00730010
sleep 1

echo "READ ALL CONFIG ... "

#echo "ADC1: Read"
#pci -w 0x90A0 0x00000002
#sleep 1

#for add in {1..5};
#  do
#    pci -w 0x90B0 0x$add
#    pci -r 0x90B0
#    value=`pci -r 0x90A8 -s1`
#    echo "address $add, value = $value "
#  done
#
#for add in {49..73};
#  do
#    pci -w 0x90B0 0x$add
#    pci -r 0x90B0
#    value=`pci -r 0x90A8 -s1`
#    echo "address $add, value = $value "
#  done

#0000
echo "ADC1: CONFIG ...DONE..."


#! /bin/bash

echo "*** ADC  ADS52j90 ***"
echo "*** Start Configuration of ADC # 2 ***"
echo "*** Start INIT after Power-ON ***"
 pci -w 0x90A4 0x000A3000
 sleep 1

#echo "ADC2: Soft Reset"
pci -w 0x90A4 0x00000001
sleep 1

#echo "ADC2: enable write"
pci -w 0x90A4 0x00000000
sleep 1
## sequences start here #

#echo "ADC2: REG ADD 1"
pci -w 0x90A4 0x00010074
sleep 1

#echo "TEST PATTER IS 1234"
#pci -w 0x90A4 0x00051234
#pci -w 0x90A4 0x00050000   # all 1s test mode is 2, all 0s is 3
#sleep 1

#echo "ADC2: REG ADD 2 --> Normal MODE ' "
pci -w 0x90A4 0x00021000  # all 1s test mode is 2, all 0s is 3
sleep 1

# bit number 5 is enable the PRBS_EN
#echo "ADC2: REG ADD 3"
pci -w 0x90A4 0x00038020                 #01 is 14bit mode, 000 is 12x, 001 is 14x, 100 is 16x, 011 is 10x
sleep 1

#echo "ADC2: REG ADD 4"
pci -w 0x90A4 0x00040001                 #01 is 14bit mode, 00 is 12 bit mode and 11 is 10bit
sleep 1

echo "Start JESD settings ADC # 2"

 #####-------changing K value-----#####
 # enable force K
 # with frame alignment option #
#echo "ADC: REG ADD 49 --> to force K value"
#pci -w 0x90A4 0x00491804
#sleep 1

#echo "ADC2: REG ADD 49 --> without FRAME ALIG. to force K value"
pci -w 0x90A4 0x00490004
sleep 1

#echo "ADC2: REG ADD 4B"
pci -w 0x90A4 0x004B0100
sleep 1

#  force K to 5+1 = 6
#echo "ADC2: REG ADD 53 --> K value force to F"
pci -w 0x90A4 0x0053000F
sleep 1

#echo "ADC2: REG ADD 55 Sub Class 2 - here"
pci -w 0x90A4 0x00554020
sleep 1

#echo "ADC2: REG ADD 73"
pci -w 0x90A4 0x00730010
sleep 1

#echo "READ ALL CONFIG ... "

#echo "ADC2: enable read"
#pci -w 0x90A4 0x00000002
#sleep 1

#for add in {1..5};
#  do
#    pci -w 0x90B4 0x$add
#    pci -r 0x90B4
#    value=`pci -r 0x90AC -s1`
#    echo "address $add, value = $value "
#  done

#for add in {49..73};
#  do
#    pci -w 0x90B4 0x$add
#    pci -r 0x90B4
#   value=`pci -r 0x90AC -s1`
#   echo "address $add, value = $value "
#  done

echo "ADC2: CONFIG ...DONE..."
