#! /bin/bash

# bit number 5 is enable the PRBS_EN
#echo "ADC1: REG ADD 3"
pci -w 0x90A0 0x00038120                 #01 is 14bit mode, 000 is 12x, 001 is 14x, 100 is 16x, 011 is 10x
sleep 0.05

#####################################################################################################################################

#echo "ADC1: REG ADD D"
pci -w 0x90A0 0x000D0064                #Offset value
sleep 0.05

#echo "ADC1: REG ADD E"
pci -w 0x90A0 0x000E0064               #Offset value
sleep 0.05

#echo "ADC1: REG ADD F"
pci -w 0x90A0 0x000F0000                #Offset value
sleep 0.05

#echo "ADC1: REG ADD 10"
pci -w 0x90A0 0x00100000                #Offset value
sleep 0.05

#echo "ADC1: REG ADD 11"
pci -w 0x90A0 0x00110000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 12"
pci -w 0x90A0 0x00120000                #Offset value
sleep 0.05

#echo "ADC1: REG ADD 13"
pci -w 0x90A0 0x00130000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 14"
pci -w 0x90A0 0x00140000                #Offset value
sleep 0.05

#echo "ADC1: REG ADD 19"
pci -w 0x90A0 0x00190000              #Offset value
sleep 0.05

#echo "ADC1: REG ADD 1A"
pci -w 0x90A0 0x001A0000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 1B"
pci -w 0x90A0 0x001B0000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 1C"
pci -w 0x90A0 0x001C0000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 1D"
pci -w 0x90A0 0x001D0000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 1E"
pci -w 0x90A0 0x001E0000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 1F"
pci -w 0x90A0 0x001F0000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 20"
pci -w 0x90A0 0x00200000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 25"
pci -w 0x90A0 0x00250000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 26"
pci -w 0x90A0 0x00260000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 27"
pci -w 0x90A0 0x00270000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 28"
pci -w 0x90A0 0x00280000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 29"
pci -w 0x90A0 0x00290000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 2A"
pci -w 0x90A0 0x002A0000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 2B"
pci -w 0x90A0 0x002B0000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 2C"
pci -w 0x90A0 0x002C0000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 31"
pci -w 0x90A0 0x00310000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 32"
pci -w 0x90A0 0x00320000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 33"
pci -w 0x90A0 0x00330000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 34"
pci -w 0x90A0 0x00340000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 35"
pci -w 0x90A0 0x00350000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 36"
pci -w 0x90A0 0x00360000                 #Offset value
sleep 0.05

#echo "ADC1: REG ADD 37"
pci -w 0x90A0 0x00370083               #Offset value
sleep 0.05

#echo "ADC1: REG ADD 38"
pci -w 0x90A0 0x00380083                 #Offset value
sleep 0.05
##########################################################################################################

#####################################################################################################################################
pci -w 0x90A4 0x00038120                 #01 is 14bit mode, 000 is 12x, 001 is 14x, 100 is 16x, 011 is 10x
sleep 0.05

#echo "ADC2: REG ADD D"
pci -w 0x90A4 0x000D0083                #Offset value
sleep 0.05

#echo "ADC2: REG ADD E"
pci -w 0x90A4 0x000E0083                #Offset value
sleep 0.05

#echo "ADC2: REG ADD F"
pci -w 0x90A4 0x000F0000              #Offset value
sleep 0.05

#echo "ADC2: REG ADD 10"
pci -w 0x90A4 0x00100000                #Offset value
sleep 0.05

#echo "ADC2: REG ADD 11"
pci -w 0x90A4 0x00110000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 12"
pci -w 0x90A4 0x00120000                #Offset value
sleep 0.05

#echo "ADC2: REG ADD 13"
pci -w 0x90A4 0x00130000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 14"
pci -w 0x90A4 0x00140000                #Offset value
sleep 0.05

#echo "ADC2: REG ADD 19"
pci -w 0x90A4 0x00190000              #Offset value
sleep 0.05

#echo "ADC2: REG ADD 1A"
pci -w 0x90A4 0x001A0000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 1B"
pci -w 0x90A4 0x001B0000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 1C"
pci -w 0x90A4 0x001C0000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 1D"
pci -w 0x90A4 0x001D0000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 1E"
pci -w 0x90A4 0x001E0000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 1F"
pci -w 0x90A4 0x001F0000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 20"
pci -w 0x90A4 0x00200000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 25"
pci -w 0x90A4 0x00250000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 26"
pci -w 0x90A4 0x00260000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 27"
pci -w 0x90A4 0x00270000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 28"
pci -w 0x90A4 0x00280000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 29"
pci -w 0x90A4 0x00290000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 2A"
pci -w 0x90A4 0x002A0000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 2B"
pci -w 0x90A4 0x002B0000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 2C"
pci -w 0x90A4 0x002C0000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 31"
pci -w 0x90A4 0x00310000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 32"
pci -w 0x90A4 0x00320000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 33"
pci -w 0x90A4 0x00330000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 34"
pci -w 0x90A4 0x00340000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 35"
pci -w 0x90A4 0x00350000                #Offset value
sleep 0.05

#echo "ADC2: REG ADD 36"
pci -w 0x90A4 0x00360000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 37"
pci -w 0x90A4 0x00370000                 #Offset value
sleep 0.05

#echo "ADC2: REG ADD 38"
pci -w 0x90A4 0x00380000                 #Offset value
sleep 0.05
