 #!/bin/bash
usage="
$(basename "$0") [-h] [-f NNNNN] [-d AAAA] [-a n] [-s n]
-- script to acquire with KALYPSO with external trigger
------------------------------------------------
where:
    -h  show this help text

    -f  set the fill number (default: 12345)
    -d  set the detector name (default: Si01)
    -a  set the number of triggers to acquire (default: 8)
    -s  set the skip value (default: 0)

    Note: to stop acquisition, create a 'stop_aq' file in the running directory
------------------------------------------------"

# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Initialize our own variables:
# fill_num="12345"
# det_name="Si01"
extension=".bin"
acquisitions=3
skip=0
div="_"
fill_prefix="f"

fill_numFlag=false
det_nameFlag=false

bg_delay=20
sig_delay=49

bum="m"

while getopts 'hd:f:as' opt; do
    case "$opt" in
    h)  echo "$usage"
        exit
        ;;
    a)  acquisitions=$OPTARG
        ;;
    s)  skip=$OPTARG
        ;;
    f)  fill_num=$OPTARG
        fill_numFlag=true
        ;;
    d)  det_name=$OPTARG
        det_nameFlag=true
        ;;
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift

if ! $fill_numFlag; then
    echo "Error: please specify the fill number with -f option (e.g. -f 12345)" >&2
    echo "Use $(basename "$0") -h for help" >&2
    exit
fi

if ! $det_nameFlag; then
    echo "Error: please specify the detector name with -d option (e.g. -d Si01)" >&2
    echo "Use $(basename "$0") -h for help" >&2
    exit
fi

if [ -e "stop_aq" ]; then
    rm "stop_aq"
fi

echo "Data Reset ... "
pci -w 0x9040 0x000fff01

echo "Writing Acquisitions ($acquisitions) and Skip ($skip)"
pci -w 9024 $acquisitions
sleep 0.1
pci -w 902C $skip
sleep 0.1

echo "System is ON, now waiting for External trigger..."
pci -w 0x9040 000fff00
sleep 1



for (( i=1; i<=$acquisitions; i++ ))
do
    tmpfname=$output_file$i$extension
    echo "Acquiring dataset $i of $acquisitions in $tmpfname..."

    ## Wait until a new acquisition is acquired...
    ACQ_NUM="$(pci -r 9034 | sed 's/.*[0-9]:  //';)"
    echo "acq num hex $ACQ_NUM"
    ACQ_NUM="$((16#$ACQ_NUM))"
    echo "acq num dec $ACQ_NUM"
    ACQ_NUM_TEMP="$(pci -r 9034 | sed 's/.*[0-9]:  //';)"
    echo "acq num temp hex $ACQ_NUM_TEMP"
    ACQ_NUM_TEMP="$((16#$ACQ_NUM_TEMP))"
    echo "acq num temp dec $ACQ_NUM_TEMP"

    while [ "$ACQ_NUM_TEMP" -eq "$ACQ_NUM" ]
    do
        ACQ_NUM_TEMP="$(pci -r 9034 | sed 's/.*[0-9]:  //';)"
        ACQ_NUM_TEMP="$((16#$ACQ_NUM_TEMP))"
        sleep 0.01
        now=$(date +"%T")
        echo -ne "$now : Waiting for new trigger..."\\r
    echo "acq num temp hex $ACQ_NUM_TEMP"
    echo "acq num temp dec $ACQ_NUM_TEMP"
    done

    pci -r dma0 --multipacket -o $tmpfname --timeout $(($(($skip+1))*15000000))

    #echo "Acquired at: $now"
    creationtime="$(date +"%Y-%m-%dT%Hh%Mm%Ss" -r $tmpfname)"

    pci -w 0x9040 0x000fff01
    sleep 0.01
    pci -w 0x9040 0x000fff00

    mv $tmpfname $fill_prefix${fill_num}_${creationtime}_${det_name}_$bum$extension
    echo "Acquired at $now: $fill_prefix${fill_num}_${creationtime}_${det_name}_$bum$extension"
    python write_csv.py -f $fill_prefix${fill_num}_${creationtime}_${det_name}_$bum$extension

    sleep 0.1

    MODUL="$(($i%12))"

    if ((MODUL == 0)); then
        echo "Moving to Background"
        pci -w 9004 $(printf %x $bg_delay)
        bum="bg"
        pci -r 9004
    else
        echo "Moving to Signal"
        pci -w 9004 $(printf %x $sig_delay)
        bum="m"
        pci -r 9004
    fi

    if [ -e "stop_aq" ]; then
        echo "Found 'stop_aq' file, data taking will now stop."
        break
    fi

done
#
#
#
#
# for (( i=1; i<=$acquisitions; i++ ))
# do
#     echo "Acquiring dataset $i of $acquisitions..."
#     pci -r dma0 --multipacket -o $i$extension --timeout $(($(($skip+1))*10000000))
#     now=$(date +"%T")
#     echo "Acquired at: $now"
#     creationtime="$(date +"%Y-%m-%dT%Hh%Mm%Ss" -r $i$extension)"
#     # creationtimeunix="$(date +"%s" -r $i$extension)"
#     mv $i$extension $fill_prefix${fill_num}_${creationtime}_${det_name}$extension
#     echo "Saved in $fill_prefix${fill_num}_${creationtime}_${det_name}$extension"
#     sleep 0.1
# done

echo "Acquisition done!"
pci -w 0x9040 400000ff
sleep 0.1

pci -w 9024 0
sleep 0.1
pci -w 902C 0
sleep 0.1
