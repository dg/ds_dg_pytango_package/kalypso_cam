#!/bin/sh

# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Initialize our own variables:
output_file="temp.bin"

while getopts "f:" opt; do
    case "$opt" in
    f)  output_file=$OPTARG
        ;;
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift

echo "output_file='$output_file'"

rm $output_file

echo "*KALYPSO: Enable Readout + DDR"
pci -w 0x9040 0x000fff00

echo "*DMA: Reading data..."
pci -r dma0 --multipacket -o $output_file --timeout=1000000

echo "*KALYPSO: RST GOTTHARD stage"
pci -w 0x9040 0x000fff01
sleep 0.01
