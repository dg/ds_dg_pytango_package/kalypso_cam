#! /bin/bash
export PCILIB_MODEL=ipedma

echo "Gotthard ON ... "
./G_ON.sh    
sleep 0.1

echo "Number of acquisitions to acquire (Slow Trig.)= 1"
pci -w 0x9024 0x1
sleep 0.1

echo "Number of acquisitions to skip (Slow Trig.)= 0"
pci -w 0x902C 0x0
sleep 0.1

echo "Number of frame to acquire (Fast Trig.)= 1024"
pci -w 0x9020 0x400
sleep 0.1

echo "Number of frame to skip (Fast Trig.)... = 1"
pci -w 0x9024 0x0       
sleep 0.1

echo "Start Normal acquisition + ENABLE Readout to DDR + HEADER"
pci -w 0x9040 0x1021FF00



for add in {1..10};
  do
    echo "Start Normal acquisition + ENABLE Readout to DDR + HEADER $add"
    pci -w 0x9040 0x1021FF00
    sleep 0.5
    echo "IDLE"
    pci -w 0x9040 0x10210F00
  done

#./status.sh 
#sleep 0.1

#echo "*DMA: Reading data..."
#./read.sh
#pci -r dma0 --multipacket -o test_data.out --timeout=1000000

#./status.sh

#./dumpFile.sh bench.out 
