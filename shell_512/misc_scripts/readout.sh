#! /bin/bash

echo "***** KALYPSO 2.0 ******"
echo "***   READ sequence ****"

rm test_data.out

echo "*ADC & GOT: RST"
pci -w 0x9040 0x000fff01
sleep 0.1

echo "*KALYPSO: Total Orbits"
pci -w 0x9020 0x20
sleep 0.1

# echo "*ONBOARD TRIG GEN: ON"
# pci -w 0x9044 0x0017
# sleep 0.01

echo "GOTT: Integration Period"
pci -w 0x9000 0x10
sleep 0.01

echo "GOTT: Integration Delay"
pci -w 0x9004 0x10
sleep 0.01

echo "GOTT: Gain"
pci -w 0x9010 0x000002
sleep 0.01

echo "*KALYPSO: Enable Readout + DDR"
pci -w 0x9040 0x000fff00

echo "*DMA: Reading data..."
pci -r dma0 --multipacket -o test_data.out --timeout=1000000

./status.sh

echo "KALYPSO: Enable Readout + DDR"
pci -w 0x9040 0x000fff01

#python plot_single.py -f test_data.out --orbits=0
