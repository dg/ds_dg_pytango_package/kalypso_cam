#! /bin/bash
export PCILIB_MODEL=ipedma
echo "CONFIGURING BARE PCB TEST ... "

echo "GLOBAL RESET ... "
./reset.sh
sleep 0.5

echo "PLL INIT/CONF for KARA 62.5 MHz ... "
./pll_62_5_KARA.sh
sleep 0.1

./pll_sync.sh
sleep 0.1

./set_dac_voltages_25.sh
sleep 0.1

echo "AXI_MASTER on FPGA INIT ..."
./AXI_conf.sh
sleep 0.1

echo "AXI_Reconfiguration for SubClass 2, frame per Multi-frame = 16"
./AXI_ReConf.sh
sleep 0.1

echo "ADCs INIT/CONF SubClass 2, frame per Multi-frame = 16"
./adc_config.sh
sleep 0.1

echo "SOFT RESET ..."
./soft_reset.sh
sleep 0.1

echo "SEND TX_TRIG both ADCs ..."
./tx_trig_adcs.sh
sleep 0.1
./tx_trig_adcs_left.sh
sleep 0.1
./tx_trig_adcs_right.sh
sleep 0.1

echo "Gotthard ON ... "
./G_ON.sh
sleep 0.1

echo "Number of acquisitions to acquire (Slow Trig.)= 1"
pci -w 0x9024 0x1
sleep 0.1
pci -w 0x9024 0x0 # for internal slow Triger #
sleep 0.1

echo "Number of acquisitions to skip (Slow Trig.)= 0"
pci -w 0x902C 0x0
sleep 0.1

echo "Number of frame to acquire (Fast Trig.)= 1024"
pci -w 0x9020 0x400
sleep 0.1

echo "Number of frame to skip (Fast Trig.)... = 1"
pci -w 0x9028 0x1
sleep 0.1
pci -w 0x9028 0x0
sleep 0.1

#echo "Gotthard ON ... "
#./align_clock.sh
#./align_clock_b.sh
sleep 0.1


pci -w 0x910C 0
sleep 0.1
./adc_offset_at_0_delay.sh
./adc_config_2_offset.sh  # previous board
#pci -w 0x910C e
#sleep 0.1

pci -w 0x910C 2f
sleep 0.1
