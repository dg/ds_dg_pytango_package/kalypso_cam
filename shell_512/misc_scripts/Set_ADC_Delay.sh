#!/bin/bash

echo "Set clock ADC delay... "

upfix=3
divfix=00C 
fixed=1

    hex_val=$(printf "%03x\n" $(($1<<2)))
    
    echo "Set $hex_val --> Time value picosecond = `expr $1 "*" 156`."
    echo "Write pci -w 9060 $upfix$hex_val$divfix$fixed"
    pci -w 0x9060 $upfix$hex_val$divfix$fixed
