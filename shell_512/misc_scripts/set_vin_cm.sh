#!/bin/sh


#echo "ADC & GOT: Turning on power..."
#pci -w 9048 f0
#sleep 0.1

echo "***Configuring DACs***"

#I2C_DAC_0 = h9068
#I2C_DAC_1 = h906C
#I2C_DAC_2 = h9070
#I2C_DAC_3 = h9074
#I2C_DAC_4 = h9078
#I2C_DAC_5 = h907C



echo "DAC2: CH1 = VIN_CM 560mV"
pci -w 9070 010$1
sleep 0.5

echo "DAC2: CH3 = VIN_CM 560mV "
pci -w 9070 030$1
sleep 0.5

echo "DAC5: CH1 = VIN_CM 560mV"
pci -w 907C 010$1
sleep 0.5

echo "DAC5: CH3 = VIN_CM 560mV "
pci -w 907C 030$1
sleep 0.5
