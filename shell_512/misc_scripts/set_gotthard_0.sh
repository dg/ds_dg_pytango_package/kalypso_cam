#!/bin/sh


#echo "ADC & GOT: Turning on power..."
#pci -w 9048 f0
#sleep 0.1

pci -w 9048 ff
sleep 0.1


echo "***Configuring DACs***"

#I2C_DAC_0 = h9068
#I2C_DAC_1 = h906C
#I2C_DAC_2 = h9070
#I2C_DAC_3 = h9074
#I2C_DAC_4 = h9078
#I2C_DAC_5 = h907C



echo "DAC0: CH0 = VB_COLBUFFER 29uA"
pci -w 9068 000800               #750 original
sleep 0.1


echo "DAC0: CH1 = IB_DS 28uA"
pci -w 9068 010550               #550
sleep 0.1

#################### Common for gotthard 0 and 1 ######################



echo "DAC2: CH0 = VOUT_CM "
pci -w 9070 0005DC          #5DC
sleep 0.1


echo "DAC2: CH1 = VIN_CM 560mV"
pci -w 9070 010460            #460
sleep 0.1


echo "....done!****"
