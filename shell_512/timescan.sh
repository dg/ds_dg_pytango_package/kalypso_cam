#!/bin/sh
export PCILIB_MODEL=ipedma

pci -w 9024 0x1
sleep 0.001
pci -w 9024 0x0
# A POSIX variable
OPTIND=1   # Reset in case getopts has been used previously in the shell.

# Initialize our own variables:
output_file="timescan_data.out"
orbits=0x1

while getopts "o:f:" opt; do
    case "$opt" in
    o)  orbits=$OPTARG
        ;;
    f)  output_file=$OPTARG
        ;;
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift

#echo "orbits=$orbits, output_file='$output_file'"

rm $output_file

pci -w 9014 1
sleep 0.1

echo "ADD Reset DDR"
#pci -w 0x9040 0x10210F50  # was 0x10210F40
pci -w 0x9040 0x10210F50
sleep 0.01

pci -w 0x9040 0x10210F00
sleep 0.01

echo "Start Normal acquisition + ENABLE Readout to DDR + HEADER"
pci -w 0x9040 0x1021FF00
sleep 0.01

echo $output_file
echo "*DMA: Reading data..."
pci -r dma0 --multipacket -o $output_file --timeout=1000000

#./status.sh
echo "IDLE"
pci -w 0x9040 0x10210F00
sleep 0.01
#./status.sh

echo "TS: Disable time scan"
pci -w 0x9014 0x0
sleep 0.1
