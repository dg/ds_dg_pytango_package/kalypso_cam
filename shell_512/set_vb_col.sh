#!/bin/sh


#echo "ADC & GOT: Turning on power..."
#pci -w 9048 f0
#sleep 0.1

echo "***Configuring DACs***"

#I2C_DAC_0 = h9068
#I2C_DAC_1 = h906C
#I2C_DAC_2 = h9070
#I2C_DAC_3 = h9074
#I2C_DAC_4 = h9078
#I2C_DAC_5 = h907C

echo "DAC0: CH0 = VB_COLBUFFER 29uA"
pci -w 9068 000$1
sleep 0.1

echo "DAC0: CH2 = VB_COLBUFFER 29uA"
pci -w 9068 020$1
sleep 0.1

echo "DAC1: CH0 = VB_COLBUFFER 29uA"
pci -w 906C 000$1
sleep 0.1

echo "DAC1: CH2 = VB_COLBUFFER 29uA"
pci -w 906C 020$1
sleep 0.1

echo "DAC3: CH0 = VB_COLBUFFER 29uA"
pci -w 9074 000$1
sleep 0.1

echo "DAC3: CH2 = VB_COLBUFFER 29uA"
pci -w 9074 020$1
sleep 0.1

echo "DAC4: CH0 = VB_COLBUFFER 29uA"
pci -w 9078 000$1
sleep 0.1

echo "DAC4: CH2 = VB_COLBUFFER 29uA"
pci -w 9078 020$1
sleep 0.1