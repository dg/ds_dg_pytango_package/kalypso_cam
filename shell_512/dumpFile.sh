#!/bin/bash

hexdump -v -e ' "0x%08.8_ax: " 8/2 " 0x%04x " "\n" ' $1 | less
