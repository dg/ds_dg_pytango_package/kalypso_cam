#! /bin/bash
export PCILIB_MODEL=ipedma

#echo "ADC & GOT: Turning on power..."
#pci -w 9048 f0
#sleep 0.1

#pci -w 9048 ff
#sleep 0.1

#echo "KALYPSO: reset internal PLL"
#pci -w 904C f
#sleep 0.1

#echo "KALYPSO: enable internal clock (WARNING: ONLY FOR DEBUGGING!)"
#pci -w 904C 0
#sleep 0.1

#echo "KALYPSO: enable external RF-CLK"
#pci -w 904C 1



#echo "*DMA: START"
#pci --stop-dma dma0r
#sleep 0.2

#pci --start-dma dma0r
#sleep 0.2

echo "RESET of the KALYPSO"
pci -w 9040 0x800f0  


echo "DE-RESET of the KALYPSO"
pci -w 9040 0x0  

echo "KALYPSO: Total Orbits"
pci -w 0x9020 0x400
sleep 0.1

pci -w 0x9024 0
sleep 0.1

echo "*ONBOARD TRIG GEN: ON"
pci -w 0x9044 0x2E
sleep 0.01

echo "GOTT: Integration Period"
pci -w 0x9000 0x1
sleep 0.01

echo "GOTT: Integration Delay"
pci -w 0x9004 0x0
sleep 0.01

echo "GOTT: Gain"
pci -w 0x9010 0x02
sleep 0.01


#echo "KALYPSO: AXI Master de- reset "
# pci -w 0x9040 0x200000
#sleep 0.1
#pci -w 0x9040 0x000021 -- original for reset Gotthard control to be consider later 
#sleep 0.1

# only control reg [0] and reg [5] for reset

#pci -w 9040 0x000000
#sleep 0.1




# internal receiver reset
#pci -w 0x9040 0x00000000
#sleep 0.01
#pci -w 0x9040 0x00000010
#sleep 0.01
#pci -w 0x9040 0x00000000
#sleep 0.01



#echo "*KALYPSO: Reset errors"
#pci -w 0x9050 0x01
#sleep 0.01
#pci -w 0x9050 0x00
#sleep 0.01



#./PLL_jesd.sh
#sleep 0.1





#echo "*KALYPSO: Reset ADC ADS52J90"
# pci -w 9040 0x00001000
# sleep 0.1

# pci -w 9040 0x00000000
# sleep 0.1

# reset ADC SPI (hardware reset)
#echo "*KALYPSO: ADC Hardware reset"
# control_reg [13]
#pci -w 0x9040 0x00022000
#sleep 0.01
#pci -w 0x9040 0x00000000
#sleep 0.01




#echo "*KALYPSO: Configure ADC ADS52J90"
#./spi_configure.sh


#echo "*DMA: START"
#pci --stop-dma dma0r
#sleep 0.2

#pci --start-dma dma0r
#sleep 0.2




#echo "*KALYPSO: DDR3 Enable Read"
# control_reg [10]
#pci -w 0x9040 0x00000400
#sleep 0.01
#pci -w 0x9040 0x00000000
#sleep 0.01


#echo "*DMA: Reading data..."
#pci -r dma0 --multipacket -o test_data.out --timeout=1000000

echo "write the number of samples to be acquired"
pci -w 9108 0x1000  

./status.sh
