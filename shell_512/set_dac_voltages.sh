echo "*** Configuring DACs ...."
# NOTE: 1(dec) = 0.5 mV
# Calibrated at 1.4 AVDD

echo "DAC0: CH0 = VB_COLBUFFER 29uA"
pci -w 9068 000750
sleep 0.01
echo "DAC1: CH0 = VB_COLBUFFER 29uA"
pci -w 906C 000750
sleep 0.01

echo "DAC0: CH1 = IB_DS 28uA"
pci -w 9068 010550
sleep 0.01
echo "DAC1: CH1 = IB_DS 28uA"
pci -w 906C 010550
sleep 0.01

echo "DAC0: CH2 = VOUT_CM "
pci -w 9068 0205DC
sleep 0.01
echo "DAC1: CH2 = VOUT_CM "
pci -w 906C 0205DC
sleep 0.01

echo "DAC0: CH3 = VIN_CM 560mV"
pci -w 9068 030460
sleep 0.01

echo "DAC1: CH3 = VIN_CM 560mV "
pci -w 906C 030460
sleep 0.01

echo "... done! ****"
