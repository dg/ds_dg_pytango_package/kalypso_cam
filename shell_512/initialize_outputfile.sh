#! /bin/bash
export PCILIB_MODEL=ipedma
#!/bin/sh
# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Initialize our own variables:
output_file="temp.bin"

while getopts "f:" opt; do
    case "$opt" in
    f)  output_file=$OPTARG
        ;;
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift

echo "output_file='$output_file'"

rm $output_file


