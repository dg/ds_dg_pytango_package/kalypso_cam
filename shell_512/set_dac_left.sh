#!/bin/sh


#echo "ADC & GOT: Turning on power..."
#pci -w 9048 f0
#sleep 0.5

#pci -w 9048 ff
#sleep 0.5


echo "***Configuring DACs***"

#VB_COLBUFFER 750
#IB_DS 550
#VIN_CM 5DC
#VOUT_CM 460


#I2C_DAC_0 = h9068
#I2C_DAC_1 = h906C
#I2C_DAC_2 = h9070
#I2C_DAC_3 = h9074
#I2C_DAC_4 = h9078
#I2C_DAC_5 = h907C



echo "DAC0: CH0 = VB_COLBUFFER 29uA"
pci -w 9068 000900
sleep 0.5

echo "DAC0: CH2 = VB_COLBUFFER 29uA"
pci -w 9068 020900
sleep 0.5

echo "DAC1: CH0 = VB_COLBUFFER 29uA"
pci -w 906C 000900
sleep 0.5

echo "DAC1: CH2 = VB_COLBUFFER 29uA"
pci -w 906C 020900
sleep 0.5


########################################################

echo "DAC0: CH1 = IB_DS 28uA"
pci -w 9068 0106d0
sleep 0.5

echo "DAC0: CH3 = IB_DS 28uA"
pci -w 9068 0306d0    #699 now programmed
sleep 0.5

echo "DAC1: CH1 = IB_DS 28uA"
pci -w 906C 0106d0
sleep 0.5

echo "DAC1: CH3 = IB_DS 28uA"
pci -w 906C 0306d0
sleep 0.5

#########################################################

echo "DAC2: CH0 = VOUT_CM 750mV=5DC "
pci -w 9070 0005DC
sleep 0.5

echo "DAC2: CH2 = VOUT_CM "
pci -w 9070 0205DC
sleep 0.5

######################################################

echo "DAC2: CH1 = VIN_CM 560mV"
pci -w 9070 010460
sleep 0.5

echo "DAC2: CH3 = VIN_CM 560mV "
pci -w 9070 030460
sleep 0.5

echo "....done!****"
