#! /bin/bash
export PCILIB_MODEL=ipedma
#!/bin/sh

dt=$(date +%Y_%m_%d_%Hh_%Mm_%Ss)


echo "Number of acquisitions to acquire (Slow Trig.)= 1"
pci -w 0x9024 0x1
sleep 0.0001

echo "Number of acquisitions to skip (Slow Trig.)= 0"
pci -w 0x902C 0x0
sleep 0.0001



#pci -r 0x9020 $num_frames 
echo "Number of frame to acquire (Fast Trig.)= 1024"
pci -w 0x9020 0xF4240   #1 million frames
#pci -w 0x9020 0x400   #small frames
#pci -w 0x9020 0x1E8480  #2 million frames

#pci -w 0x9020 0x400
sleep 0.0001

pci -w 0x9024 0x0
sleep 0.0001

echo "ADD Reset DDR"
pci -w 0x9040 0x10210F50
sleep 0.1

pci -w 0x9040 0x10210F00
sleep 0.1

echo "Start Normal acquisition + ENABLE Readout to DDR + HEADER"
pci -w 0x9040 0x1021FF00
#sleep 0.1
sleep 2
#sleep 4
#./status.sh
echo "IDLE"
pci -w 0x9040 0x10210F00
#sleep 0.1
./status.sh



echo "*DMA: Reading data..."
#pci -r dma0 --multipacket -o /home/soleil/kalypso/data/bench.out --timeout=10000000

pci -r dma0 --multipacket -o /home/soleil/kalypso/data/kalypso_${dt}.bin --timeout=10000000
#pci -r dma0 --multipacket -o /dev/null --timeout=100000

./status.sh

#./dumpFile.sh bench.out
