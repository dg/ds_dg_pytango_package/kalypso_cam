#!/bin/bash

#echo "Remove Board Reset ... "
#pci -w 0x9040 0x00
#sleep 0.5

#while :
#do

echo "*PLL: calibration start ... "
echo "*PLL: R0 Reset ... "

pci -w 0x9064 0x80000000
sleep 0.2


#pci -w 0x9064 0x80000100
#sleep 0.2


#pci -w 0x9064 0x00030100
#sleep 0.2


# R0 (INIT)

pci -w 0x9064 0x80000100 # OUTPUT 500MHZ FPGA
sleep 0.2

#echo "*PLL: R0 "
pci -w 0x9064 0x00070400 # OUTPUT 500 MHZ FPGA
sleep 0.2


#echo "*PLL: R1 "
#pci -w 0x9064 0x00005C01 # SYS_REF_2 before 0x00035C01
#sleep 0.2

#echo "*PLL: R2 "
#pci -w 0x9064 0x00005C02 # SYS_REF_1
#sleep 0.2

#echo "*PLL: R3 "
#pci -w 0x9064 0x00030403   #######original
#sleep 0.2

echo "*PLL: R3 "
pci -w 0x9064 0x00070483     #####remove later # clk_ADC_2    working:0x000704b3
sleep 0.2

#echo "*PLL: R4 "
#pci -w 0x9064 0x00030404
#sleep 0.2

echo "*PLL: R4 "
pci -w 0x9064 0x00070484          # d0,d4,d8,db # clk_ADC_1  working:0x000704b3
sleep 0.2

echo "*PLL: R5 "
pci -w 0x9064 0x00070205           # FPGA_GTHX_CLOCK_0
sleep 0.2


echo "*PLL: R6 "
pci -w 0x9064 0x00070206            # FPGA_GTHX_CLOCK_1
sleep 0.2


#echo "*PLL: R7 "
#pci -w 0x9064 0x00005C07            # FPGA_SYS_REF before 0x00035C07
#sleep 0.2


echo "*PLL: R8 "
pci -w 0x9064 0x10000908
sleep 0.2

echo "*PLL: R9 "
pci -w 0x9064 0xA0022A09
sleep 0.2

echo "*PLL: RB (R11)"
pci -w 0x9064 0x0082000B
sleep 0.2

echo "*PLL: RD (R13)"
pci -w 0x9064 0x028300AD
sleep 0.2

echo "*PLL: RE (R14)"
pci -w 0x9064 0x08007D0E
sleep 0.2


echo "*PLL: RF (R15)"
pci -w 0x9064 0xCC03E80F
sleep 0.2
