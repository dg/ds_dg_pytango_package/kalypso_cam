#!/bin/bash

#echo "Remove Board Reset ... "
#pci -w 0x9040 0x00
#sleep 0.5

echo "*PLL: calibration start ... "
echo "*PLL: R0 Reset ... "
pci -w 0x9064 0x80000000
sleep 0.1

echo "*PLL: R0 clock to FPGA ... "
## note: divider is x2
## ex: 6 -> divider 12
pci -w 0x9064 0x00030600
sleep 0.1

echo "*PLL: R8 ... "
pci -w 0x9064 0x10000908
sleep 0.1

echo "*PLL: R11 ..."
pci -w 0x9064 0x0082800B
sleep 0.1

echo "*PLL: R13 ..."
pci -w 0x9064 0x028F800D
# pci -w 0x9064 0x028F800D
sleep 0.1

echo "*PLL: R14 ..."
pci -w 0x9064 0x0830020E
sleep 0.1

echo "*PLL: R15 ..."
# pci -w 0x9064 0xD000180F
pci -w 0x9064 0xC800180F
sleep 0.1

echo "*PLL: send SYNC signal"
pci -w 0x9040 0x010000ff
sleep 0.1
pci -w 0x9040 0x000000ff
sleep 0.1
