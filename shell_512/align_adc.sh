#!/bin/sh

  echo "*** Aligning ADC... ***"

  echo "ADC: RST input stage"
  pci -w 0x9040 0x0000ff
  sleep 0.05
  pci -w 0x9040 0x000001
  sleep 0.05

  echo "Send align Pattern: 00 0000 0111 1111 = 7F"
  pci -w 9084 0x7F

  echo "ADC: Test mode +FS short"
  # 8 for user pattern
  pci -w 0x9060 0x00d0a
  sleep 0.05

  echo "ADC: Transfer Register"
  pci -w 0x9060 0x0ff01
  sleep 0.05

  echo "ADC: Transfer Register"
  pci -w 0x9060 0x0ff00
  sleep 0.05

  echo "ADC: Enable ADC alignment"
  pci -w 0x9040 0x0010000
  sleep 0.5

  echo "ADC: Set back normal mode"
  pci -w 0x9060 0x0d00
  sleep 0.05

  pci -w 0x9060 0x0ff01
  sleep 0.05
  pci -w 0x9060 0x0ff00
  sleep 0.05
