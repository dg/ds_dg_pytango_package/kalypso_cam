#! /bin/bash

# bit number 5 is enable the PRBS_EN
echo "ADC1: REG ADD 3"
pci -w 0x90A0 0x00038120                 #01 is 14bit mode, 000 is 12x, 001 is 14x, 100 is 16x, 011 is 10x
sleep 0.3

pci -w 0x90B0 0x0003
pci -r 0x90B0
pci -r 0x90A8


#####################################################################################################################################
echo "ADC1: REG ADD D"
pci -w 0x90A0 0x000D03FF                #Offset value
sleep 0.3
pci -w 0x90B0 0x000D
pci -r 0x90B0
pci -r 0x90A8

echo "ADC1: REG ADD E"
pci -w 0x90A0 0x000E03FF                #Offset value
sleep 0.3

echo "ADC1: REG ADD F"
pci -w 0x90A0 0x000F03FF                 #Offset value
sleep 0.3

echo "ADC1: REG ADD 10"
pci -w 0x90A0 0x001003FF                #Offset value
sleep 0.3

echo "ADC1: REG ADD 11"
pci -w 0x90A0 0x001103FF                 #Offset value
sleep 0.3

echo "ADC1: REG ADD 12"
pci -w 0x90A0 0x001203FF                #Offset value
sleep 0.3

echo "ADC1: REG ADD 13"
pci -w 0x90A0 0x001303FF                 #Offset value
sleep 0.3

echo "ADC1: REG ADD 14"
pci -w 0x90A0 0x001403FF                #Offset value
sleep 0.3

echo "ADC1: REG ADD 19"
pci -w 0x90A0 0x001903FF              #Offset value
sleep 0.3

echo "ADC1: REG ADD 1A"
pci -w 0x90A0 0x001A03FF                 #Offset value
sleep 0.3

echo "ADC1: REG ADD 1B"
pci -w 0x90A0 0x001B03FF                 #Offset value
sleep 0.3

echo "ADC1: REG ADD 1C"
pci -w 0x90A0 0x001C03FF                 #Offset value
sleep 0.3

echo "ADC1: REG ADD 1D"
pci -w 0x90A0 0x001D03FF                 #Offset value
sleep 0.3

echo "ADC1: REG ADD 1E"
pci -w 0x90A0 0x001E03FF                 #Offset value
sleep 0.3

echo "ADC1: REG ADD 1F"
pci -w 0x90A0 0x001F03FF                 #Offset value
sleep 0.3

echo "ADC1: REG ADD 20"
pci -w 0x90A0 0x002003FF                 #Offset value
sleep 0.3
##########################################################################################################
